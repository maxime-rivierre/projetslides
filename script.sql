-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 10 mai 2019 à 08:05
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entreprise` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id`, `nom`, `entreprise`, `site`) VALUES
(1, 'Centre Services', 'Centre Services', 'https://centreservices.fr/'),
(2, 'Maison Savouré', 'Maison Savouré', 'https://maisonsavoure.com/'),
(3, 'Plein Jour Habitat', 'Plein Jour Habitat', 'https://www.pleinjourhabitat.com/accueil'),
(4, 'Maxime Rivierre', '1984 Agency', 'https://1984.agency/'),
(55, 'Rémi Grosset', 'Centre services', 'https://fr.wikipedia.org/wiki/Erreur_HTTP_404.abcd'),
(56, 'Benjamin Assous', 'Centre services', ''),
(140, 'François Petit', '1984 Agency', ''),
(146, 'Tiphaine Savouré', 'Maison Savouré', ''),
(216, 'Mickaël HUILLERY', 'Pompes Funèbres Orbecquoises', '');

-- --------------------------------------------------------

--
-- Structure de la table `forum_category`
--

DROP TABLE IF EXISTS `forum_category`;
CREATE TABLE IF NOT EXISTS `forum_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `forum_category`
--

INSERT INTO `forum_category` (`id`, `name`, `creation_date`, `user_id`) VALUES
(1, 'Aide', '2019-02-21 11:16:36', 1),
(2, 'Support', '2019-02-21 11:29:11', 1),
(3, 'Général', '2019-02-21 11:33:46', 1),
(4, 'Archive', '2019-02-21 11:50:32', 1),
(5, 'Administratif', '2019-02-26 14:34:11', 1),
(6, 'Test', '2019-02-28 09:17:03', 1);

-- --------------------------------------------------------

--
-- Structure de la table `forum_notification`
--

DROP TABLE IF EXISTS `forum_notification`;
CREATE TABLE IF NOT EXISTS `forum_notification` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `get_notif_new_topic` tinyint(1) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `forum_notification`
--

INSERT INTO `forum_notification` (`id`, `user_id`, `get_notif_new_topic`, `status`) VALUES
(1, 1, 1, 'active'),
(2, 2, 1, 'inactive');

-- --------------------------------------------------------

--
-- Structure de la table `forum_post`
--

DROP TABLE IF EXISTS `forum_post`;
CREATE TABLE IF NOT EXISTS `forum_post` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `modification_date` datetime DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `image_link` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(10) DEFAULT NULL,
  `topic_id` int(10) DEFAULT NULL,
  `get_notif_after_post` tinyint(1) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `topic_id` (`topic_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `forum_post`
--

INSERT INTO `forum_post` (`id`, `modification_date`, `creation_date`, `image_link`, `content`, `user_id`, `topic_id`, `get_notif_after_post`, `status`) VALUES
(1, NULL, '2019-02-26 08:34:37', NULL, 'Topic dédié à l\'entraide.\r\n\r\nN\'hésitez pas à poster.', 1, 1, 1, 1),
(2, NULL, '2019-02-22 08:35:43', NULL, 'A l\'aide, mon site ne marche plus', 2, 2, 1, 1),
(3, '2019-03-01 09:38:32', '2019-02-23 12:31:34', NULL, 'Bonjour,\n\nPouvez vous fournir plus de détails ?\n\nCordialement,\nMaxime', 1, 2, 0, 1),
(4, NULL, '2019-02-26 10:33:36', NULL, 'FAQ \r\n\r\nMarche à suivre : test.com\r\n\r\nContact : 01 23 45 67 89', 1, 3, 0, 1),
(5, '2019-02-28 08:47:43', '2019-02-26 10:21:11', '', 'Bonjour,\n\nTexte\n\nCordialement', 1, 4, 0, 1),
(6, NULL, '2019-02-26 10:26:09', '', 'Blablabla', 1, 5, 0, 1),
(7, '2019-02-27 12:15:35', '2019-02-26 10:29:01', '', 'Test\n\nPremière ligne du contenu du message\nDeuxième ligne du contenu du message\n\nTroisième ligne du contenu du message\n\nMessage de fin,\nNom\n\n', 1, 6, 0, 1),
(8, NULL, '2019-02-26 10:30:58', '', 'rgegeg', 1, 7, 0, 1),
(9, NULL, '2019-02-26 10:33:16', '', 'Contenu', 1, 8, 0, 1),
(10, '2019-02-28 11:00:51', '2019-02-26 10:39:28', '', 'Pour nous contacter : https://1984.agency/contact\n\nrezgzgfgeegege\neregeg\n', 1, 9, 0, 1),
(11, NULL, '2019-02-26 14:24:40', '', 'egegeg', 1, 14, 0, 1),
(14, '2019-02-28 14:00:02', '2019-02-26 16:24:09', '', 'rthrhrhrhr', 1, 3, 1, 1),
(15, NULL, '2019-02-27 08:20:01', '', 'New post topic 3 cat 2', 1, 3, 0, 1),
(16, NULL, '2019-02-27 08:24:29', '', 'Ce message a pour but de remonter le topic', 1, 6, 0, 1),
(17, '2019-02-27 12:09:10', '2019-02-27 08:26:48', '', 'Test new post update last\n\nTest update', 1, 6, 0, 1),
(18, NULL, '2019-02-27 08:29:13', '', 'Test avec new id', 1, 6, 0, 1),
(19, NULL, '2019-02-27 09:15:29', '', 'Test up topic', 1, 5, 0, 1),
(20, '2019-02-27 14:30:50', '2019-02-27 14:30:21', '', 'Topic des modérateurs\n\nSignaler les messages innoportuns', 1, 15, 1, 1),
(21, NULL, '2019-02-27 14:30:59', '', 'regqgqerg', 1, 15, 0, 1),
(22, '2019-02-28 08:46:19', '2019-02-27 15:04:44', '', 'Pas mal l\'image\n\nrtyhyrthr', 1, 3, 0, 1),
(23, NULL, '2019-02-27 15:11:30', '', 'Test ajout AJAX', 1, 3, 0, 1),
(24, NULL, '2019-02-27 15:12:35', '', 'Deuxieme test AJAX', 1, 3, 0, 1),
(25, '2019-02-28 11:05:16', '2019-02-27 15:12:49', '', 'Ca marche c\'est beau\n\net en ajax', 1, 3, 0, 1),
(26, NULL, '2019-02-27 15:15:09', '', 'Test insert encore', 1, 3, 0, 1),
(27, NULL, '2019-02-27 15:31:09', 'modules/suiviClient/images/27-02-2019d6065b8e967b179dcd90702e2462f72110d10da80a8494b83c.png', 'Image éventuelle\r\n\r\nVoir si convient', 1, 3, 0, 1),
(28, NULL, '2019-02-27 15:35:42', 'modules/suiviClient/images/27-02-2019fff1c6d0a89f6f18e55e94ca8cc0fc1b1e98d6c3c2f0cb3b91.png', '', 1, 3, 0, 1),
(29, '2019-02-27 15:43:15', '2019-02-27 15:37:00', 'modules/suiviClient/images/27-02-20191942b4a18d8a527d6cf1d933ae0c02e0f9d4c45dcb58f4db81.png', 'Test upload image ajax\n\nA voir si l\'image est bien ajoutée par AJAX et non refresh f5\nVoir si taille pas trop mal\n\nNom prenom,\nposte', 1, 3, 0, 1),
(30, NULL, '2019-02-27 15:37:51', 'modules/suiviClient/images/27-02-2019f83e6b317d3c50ae445b734501b4b9421153f003b588f75529.png', 'Testeuh', 1, 3, 0, 1),
(31, '2019-02-28 14:02:25', '2019-02-27 15:40:56', '', '', 1, 3, 0, 1),
(32, '2019-02-28 13:39:05', '2019-02-27 15:43:51', 'modules/suiviClient/images/27-02-2019c1364d0048cd6f5ef054e96c420f0dd6ca377a944bf461b013.png', '', 1, 3, 0, 1),
(33, NULL, '2019-02-28 08:20:32', '', 'A l\'aide', 1, 1, 0, 1),
(34, NULL, '2019-02-28 08:20:52', '', 'Comment faire ...', 1, 8, 0, 1),
(35, NULL, '2019-02-28 08:21:38', '', 'New test', 1, 3, 0, 1),
(36, NULL, '2019-02-28 08:21:59', '', 'erqtgeqrgqeg', 1, 7, 0, 1),
(37, NULL, '2019-02-28 08:22:15', '', 'rhrhrh', 1, 6, 0, 1),
(38, NULL, '2019-02-28 08:22:38', '', 'treghbeth', 1, 2, 0, 0),
(39, NULL, '2019-02-28 08:22:48', '', 'qegeqrgqeg', 1, 14, 0, 1),
(40, NULL, '2019-02-28 08:23:00', '', 'rhrhrh', 1, 4, 0, 0),
(41, NULL, '2019-02-28 08:23:25', 'modules/suiviClient/images/28-02-20194006e31a18f94a35f2f21dc0004ede961a4e68c382a53ee355.png', 'rgqergeqrg', 1, 5, 0, 1),
(42, '2019-02-28 11:00:59', '2019-02-28 08:23:43', '', 'Modifier\n\nencore une modif de plus\ngegeg', 1, 9, 0, 1),
(43, NULL, '2019-02-28 08:24:03', '', 'trhrh', 1, 15, 0, 1),
(44, '2019-03-03 13:51:43', '2019-02-28 08:26:31', '', 'Bonjour,\n\nPremier message du sujet.\nDeuxième ligne du message\n\nCordialement,\nMaxime Rivierre', 1, 16, 0, 1),
(45, NULL, '2019-02-28 08:38:42', '', 'Test pour ajout topic en AJAX', 1, 17, 1, 1),
(46, NULL, '2019-02-28 08:48:32', '', 'Premier topic archive', 1, 18, 0, 1),
(47, NULL, '2019-02-28 09:04:38', '', 'Test', 1, 19, 1, 1),
(48, NULL, '2019-02-28 09:05:57', '', 'TEST', 1, 20, 1, 1),
(49, NULL, '2019-02-28 09:09:13', '', 'rthrh', 1, 21, 0, 1),
(50, NULL, '2019-02-28 09:11:58', '', 'Ok', 1, 21, 0, 1),
(51, '2019-02-28 09:16:27', '2019-02-28 09:12:17', '', 'Test\n\nhttp://google.fr', 1, 22, 0, 1),
(52, NULL, '2019-02-28 09:15:14', '', 'retgergeg', 1, 23, 1, 1),
(53, '2019-02-28 09:17:35', '2019-02-28 09:17:17', '', 'Test\n\nhttps://google.fr', 1, 24, 1, 1),
(54, NULL, '2019-02-28 09:17:45', '', 'retgeg', 1, 25, 1, 1),
(55, '2019-02-28 09:19:11', '2019-02-28 09:18:57', '', 'retgeg\n\n\nrthrhrhrh', 1, 26, 0, 1),
(56, NULL, '2019-02-28 09:20:14', '', 'ergegeg', 1, 27, 1, 1),
(57, NULL, '2019-02-28 09:21:16', '', 'trhrhr', 1, 27, 1, 1),
(58, NULL, '2019-02-28 09:22:22', '', 'rhtrthrh', 1, 27, 1, 1),
(59, NULL, '2019-02-28 09:23:03', '', 'TETSFZSFZ', 1, 27, 1, 1),
(60, NULL, '2019-02-28 09:28:44', 'modules/suiviClient/images/28-02-2019e0a6b514c2b3bfea5d760d8b87285807caf42139777e71556e.png', 'Testeuh', 1, 27, 1, 1),
(61, NULL, '2019-02-28 09:28:56', '', 'test again', 1, 27, 0, 1),
(62, NULL, '2019-02-28 09:29:22', '', 'test', 1, 28, 1, 1),
(63, NULL, '2019-02-28 09:29:28', '', 'rhrh', 1, 29, 0, 1),
(64, '2019-02-28 15:15:11', '2019-02-28 14:59:18', '', 'thjtjtjtj', 1, 30, 1, 1),
(65, NULL, '2019-02-28 15:19:14', '', 'Message du topic', 1, 30, 0, NULL),
(66, NULL, '2019-02-28 15:22:04', '', 'Test', 1, 31, 1, 1),
(67, NULL, '2019-02-28 16:49:49', NULL, 'Test autre user', 2, 31, 0, 1),
(68, NULL, '2019-03-01 09:36:21', '', 'Message a delete', 1, 2, 0, 0),
(69, NULL, '2019-03-01 09:37:07', '', 'rth', 1, 2, 0, 0),
(70, NULL, '2019-03-01 09:37:56', '', 'ergtegeg', 1, 2, 0, 0),
(71, NULL, '2019-03-01 11:16:24', '', 'Réponse au message', 2, 16, 1, 1),
(72, NULL, '2019-03-01 11:18:04', '', 'Test notif', 1, 4, 1, 0),
(73, NULL, '2019-03-01 11:19:46', '', 'dgeg', 1, 4, 1, 0),
(74, NULL, '2019-03-01 11:20:19', '', 'dfgdg', 1, 4, 1, 0),
(75, NULL, '2019-03-01 11:21:38', '', 'ergergegegarethge', 1, 4, 1, 0),
(76, NULL, '2019-03-01 11:22:34', '', 'rdegege', 1, 4, 1, 0),
(77, NULL, '2019-03-01 11:24:04', '', 'ergegegeg', 1, 4, 0, 0),
(78, NULL, '2019-03-01 11:25:49', '', 'ergegegeg', 1, 4, 0, 0),
(79, NULL, '2019-03-01 11:26:52', '', 'rthrhrh', 1, 4, 0, 0),
(80, NULL, '2019-03-01 11:28:53', '', 'ergegeegegege', 1, 4, 1, 0),
(81, NULL, '2019-03-01 11:32:19', '', 'regegg', 1, 4, 1, 0),
(82, NULL, '2019-03-01 11:34:37', '', 'regzgzg', 1, 4, 1, 0),
(83, NULL, '2019-03-01 11:44:57', '', 'rhrhrhrhtshsh', 1, 4, 1, 0),
(84, NULL, '2019-03-01 11:46:06', '', 'zergzegzg', 2, 4, 0, 0),
(85, NULL, '2019-03-01 11:48:15', '', 'rtyhrthyrhrh', 2, 4, 1, 0),
(86, NULL, '2019-03-01 11:53:16', '', 'regegeg\r\n\r\n\r\negege\r\nge\r\ng\r\negegeg', 2, 4, 1, 1),
(87, NULL, '2019-03-01 13:16:29', '', 'up', 2, 3, 1, 1),
(88, NULL, '2019-03-01 14:33:43', '', 'regegr', 1, 32, 1, 1),
(89, NULL, '2019-03-01 14:34:52', '', 'egegeg\r\n\r\neg\r\neg\r\ne\r\ng\r\negeg', 1, 33, 1, 1),
(90, NULL, '2019-03-01 14:36:32', '', 'rezgzgzg', 1, 34, 1, 1),
(91, NULL, '2019-03-01 14:37:57', '', 'message', 1, 35, 0, 1),
(92, NULL, '2019-03-01 14:38:54', '', 'rthgerg', 1, 36, 1, 1),
(93, NULL, '2019-03-01 14:48:23', '', 'Test', 1, 37, 0, 1),
(94, NULL, '2019-03-01 15:03:54', '', 'Envoi notif créateur + users', 2, 3, 1, 1),
(95, NULL, '2019-03-01 15:06:11', '', 'Double notif', 2, 3, 1, 1),
(96, NULL, '2019-03-01 15:08:45', '', 'Test', 1, 38, 1, 1),
(97, NULL, '2019-03-01 15:09:26', '', 'Sauvegarde', 2, 38, 0, 1),
(98, NULL, '2019-03-01 15:10:38', '', 'New', 2, 38, 1, 1),
(99, NULL, '2019-03-01 15:13:14', '', 'New test\r\n\r\n2 notifs', 2, 38, 1, 1),
(100, NULL, '2019-03-01 15:14:53', '', 'New post test', 2, 38, 1, 1),
(101, NULL, '2019-03-01 15:19:01', '', 'rthrhrhrhrh', 2, 38, 1, 1),
(102, NULL, '2019-03-01 15:20:35', '', 'rthrthrh\r\nr\r\nhr\r\n\r\nhrhrhrhrh', 1, 38, 1, 1),
(103, NULL, '2019-03-01 15:21:58', '', 'regege\r\neg\r\n\r\neg\r\ne\r\ngege', 1, 38, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `forum_topic`
--

DROP TABLE IF EXISTS `forum_topic`;
CREATE TABLE IF NOT EXISTS `forum_topic` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `texte` text COLLATE utf8mb4_unicode_ci,
  `creation_date` datetime DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  `get_creation_notif` tinyint(1) NOT NULL,
  `last_post` int(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `user_id` (`user_id`),
  KEY `last_post` (`last_post`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `forum_topic`
--

INSERT INTO `forum_topic` (`id`, `texte`, `creation_date`, `user_id`, `category_id`, `get_creation_notif`, `last_post`, `status`) VALUES
(1, 'Besoin d\'aide ? Postez içi', '2019-02-20 15:05:12', 1, 1, 1, 33, 1),
(2, 'Mon site ne marche plus', '2019-02-20 15:05:18', 2, 2, 1, 70, 1),
(3, 'Marche à suivre en cas de problème', '2019-02-26 11:04:25', 1, 2, 1, 95, 1),
(4, 'Nouveau sujet', '2019-02-26 10:21:11', 1, 2, 1, 86, 1),
(5, 'Bla', '2019-02-26 10:26:09', 1, 2, 0, 41, 1),
(6, 'New topic', '2019-02-26 10:29:01', 1, 2, 0, 37, 1),
(7, 'rege', '2019-02-26 10:30:58', 1, 2, 0, 36, 1),
(8, 'Foire Aux Questions', '2019-02-26 10:33:16', 1, 1, 0, 34, 1),
(9, 'Formulaire de contact', '2019-02-26 10:39:28', 1, 1, 0, 42, 1),
(14, 'ergeg', '2019-02-26 14:24:40', 1, 2, 0, 39, 1),
(15, 'Modération', '2019-02-27 14:30:21', 1, 5, 1, 43, 1),
(16, 'Nouveau sujet', '2019-02-28 08:26:31', 1, 1, 1, 71, 1),
(17, 'Test prepend ', '2019-02-28 08:38:42', 1, 2, 1, 45, 1),
(18, 'Old topic', '2019-02-28 08:48:32', 1, 4, 0, 46, 1),
(19, 'Tout et rien', '2019-02-28 09:04:38', 1, 3, 1, 47, 1),
(20, 'Deuxième', '2019-02-28 09:05:57', 1, 3, 1, 48, 1),
(21, 'Troisième', '2019-02-28 09:09:13', 1, 3, 0, 50, 1),
(22, 'Encore un topic', '2019-02-28 09:12:17', 1, 3, 0, 51, 1),
(23, 'Un de plus', '2019-02-28 09:15:14', 1, 3, 1, 52, 1),
(24, 'Dev', '2019-02-28 09:17:17', 1, 6, 1, 53, 1),
(25, 'Dev 2', '2019-02-28 09:17:45', 1, 6, 1, 54, 1),
(26, 'Dev 2', '2019-02-28 09:18:57', 1, 6, 0, 55, 1),
(27, 'Autre dev', '2019-02-28 09:20:14', 1, 6, 1, 61, 1),
(28, 'Last dev', '2019-02-28 09:29:22', 1, 6, 1, 62, 1),
(29, 'hrth', '2019-02-28 09:29:28', 1, 6, 0, 63, 1),
(30, 'Test topic sans notif', '2019-02-28 15:19:14', 1, 6, 0, 64, 1),
(31, 'Recrutement', '2019-02-28 15:22:04', 2, 5, 1, 67, 1),
(32, 'Nouveau sujet pour notif', '2019-03-01 14:33:43', 1, 2, 1, 88, 1),
(33, 'New new', '2019-03-01 14:34:52', 1, 2, 1, 89, 1),
(34, 'New topipic', '2019-03-01 14:36:32', 1, 6, 1, 90, 1),
(35, 'Notif après création', '2019-03-01 14:37:57', 1, 6, 0, 91, 1),
(36, 'Pas de notif après création', '2019-03-01 14:38:54', 1, 6, 1, 92, 1),
(37, 'Archivage', '2019-03-01 14:48:23', 1, 4, 0, 93, 1),
(38, 'Sauvegarde', '2019-03-01 15:08:45', 1, 4, 1, 103, 1);

-- --------------------------------------------------------

--
-- Structure de la table `forum_user`
--

DROP TABLE IF EXISTS `forum_user`;
CREATE TABLE IF NOT EXISTS `forum_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_status` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_status` (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `forum_user`
--

INSERT INTO `forum_user` (`id`, `first_name`, `last_name`, `mail`, `password`, `id_status`) VALUES
(1, 'Maxime', 'Rivierre', 'maxime.rivierre@1984.agency', NULL, 1),
(2, 'Membre', 'Classique', '', NULL, 2);

-- --------------------------------------------------------

--
-- Structure de la table `forum_user_type`
--

DROP TABLE IF EXISTS `forum_user_type`;
CREATE TABLE IF NOT EXISTS `forum_user_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `forum_user_type`
--

INSERT INTO `forum_user_type` (`id`, `name`) VALUES
(1, 'Responsable'),
(2, 'Membre');

-- --------------------------------------------------------

--
-- Structure de la table `goalstype`
--

DROP TABLE IF EXISTS `goalstype`;
CREATE TABLE IF NOT EXISTS `goalstype` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fieldName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `goalstype`
--

INSERT INTO `goalstype` (`id`, `name`, `description`, `fieldName`, `active`) VALUES
(42, 'Heures', '', 'Heures', 1),
(43, 'Chiffres d\'affaires', '', 'Chiffres d\'affaires', 1),
(44, 'Random', '', 'Random', 1),
(45, 'Encore', '', 'Encore', 1),
(46, 'Test', '', 'Test', 1);

-- --------------------------------------------------------

--
-- Structure de la table `organization`
--

DROP TABLE IF EXISTS `organization`;
CREATE TABLE IF NOT EXISTS `organization` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `organization`
--

INSERT INTO `organization` (`id`, `nom`) VALUES
(45, 'Orléans'),
(46, 'Chartres'),
(47, 'Paris'),
(48, 'Test');

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

DROP TABLE IF EXISTS `projet`;
CREATE TABLE IF NOT EXISTS `projet` (
  `numero` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idclient` int(10) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_creation` date NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`numero`),
  KEY `idclient` (`idclient`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `projet`
--

INSERT INTO `projet` (`numero`, `nom`, `idclient`, `url`, `date_creation`, `hidden`) VALUES
(11, 'Projet Centre Services 2018', 1, '11-Projet+Centre+Services+2018', '2019-01-15', 0),
(12, 'Maison Savouré 130 ans', 2, '23-Maison+Savour%C3%A9+130+ans', '2019-01-15', 1),
(21, 'Projet ajout AJAX', 4, '21-Projet+ajout+AJAX', '2019-01-18', 1),
(25, 'Test intégration', 4, '25-Test+int%C3%A9gration', '2019-01-28', 1),
(54, 'Exemple de projet', 146, '54-Test+ajout+2', '2019-01-30', 0),
(56, 'Test', 140, '56-Test', '2019-02-01', 1),
(57, 'Test', 56, '57-Test', '2019-02-01', 1),
(58, 'Test', 146, '58-Test', '2019-02-05', 1),
(59, 'Refonte charte', 216, '59-Refonte+charte', '2019-02-06', 1),
(60, 'Développement', 140, '60-Dev', '2019-02-06', 1),
(61, 'Projet test intro', 140, '61-Projet+test+intro', '2019-02-07', 1),
(62, 'Test edit', 140, '62-Test', '2019-02-08', 1),
(65, 'Ttt', 55, '65-Ttt', '2019-02-14', 1),
(66, 'Test', 55, '66-Test', '2019-02-14', 1),
(67, 'Projet Démo', 140, '67-Projet+d%C3%A9mo', '2019-02-19', 1),
(68, 'Modernisation Charte Graphique', 216, '68-Modernisation+charte+graphique', '2019-02-21', 1),
(69, 'Projet Démo', 140, '69-Projet+D%C3%A9mo', '2019-02-24', 1),
(70, 'Htyhtyj', 140, '70-Htyhtyj', '2019-03-04', 1),
(71, 'Undefined', 4, 'Test', '2019-04-02', 1),
(73, 'Reef\"f\"\'', 4, '73-Reef%22f%22%27', '2019-04-02', 1),
(74, 'Refergfe', 4, '74-Refergfe', '2019-04-29', 1),
(75, 'Gergerg', 4, '75-Gergerg', '2019-04-29', 1),
(76, 'Tefs', 4, '76-Tefs', '2019-04-29', 1),
(77, 'Zefzf', 4, '77-Zefzf', '2019-04-29', 1),
(78, 'Gerge', 4, '78-Gerge', '2019-04-29', 1),
(79, 'Test projet', 4, '79-Test+projet', '2019-04-29', 1),
(80, 'Test projet qui marche', 4, '80-Test+projet+qui+marche+po', '2019-05-03', 1),
(81, 'Projet test', 4, '81-Projet+test', '2019-05-06', 1),
(82, 'Sam sam', 4, '82-Sam+sam', '2019-05-06', 1);

-- --------------------------------------------------------

--
-- Structure de la table `reportings`
--

DROP TABLE IF EXISTS `reportings`;
CREATE TABLE IF NOT EXISTS `reportings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `idOrganization` int(10) NOT NULL,
  `month` int(2) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `value` int(20) DEFAULT NULL,
  `idGoalType` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idOrganization` (`idOrganization`),
  KEY `idGoalType` (`idGoalType`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `reportings`
--

INSERT INTO `reportings` (`id`, `idOrganization`, `month`, `year`, `value`, `idGoalType`) VALUES
(101, 45, 1, 2019, 400, 42),
(102, 45, 1, 2019, 1200, 43),
(103, 45, 1, 2019, NULL, 44),
(104, 45, 1, 2019, 8, 45),
(105, 45, 1, 2019, 150, 46),
(106, 45, 2, 2019, 500, 42),
(107, 45, 2, 2019, 1200, 43),
(108, 45, 2, 2019, 2, 44),
(109, 45, 2, 2019, 7, 45),
(110, 45, 2, 2019, 325, 46),
(111, 45, 3, 2019, 600, 42),
(112, 45, 3, 2019, 1700, 43),
(113, 45, 3, 2019, 3, 44),
(114, 45, 3, 2019, 6, 45),
(115, 45, 3, 2019, 500, 46),
(116, 45, 4, 2019, 700, 42),
(117, 45, 4, 2019, 1300, 43),
(118, 45, 4, 2019, 4, 44),
(119, 45, 4, 2019, 5, 45),
(120, 45, 4, 2019, 675, 46),
(121, 45, 5, 2019, 800, 42),
(122, 45, 5, 2019, 1200, 43),
(123, 45, 5, 2019, 5, 44),
(124, 45, 5, 2019, 4, 45),
(125, 45, 5, 2019, 850, 46),
(126, 45, 6, 2019, 900, 42),
(127, 45, 6, 2019, 1000, 43),
(128, 45, 6, 2019, 6, 44),
(129, 45, 6, 2019, 3, 45),
(130, 45, 6, 2019, 1025, 46),
(131, 46, 1, 2018, 1500, 42),
(132, 46, 1, 2018, 2500, 43),
(133, 46, 1, 2018, 25, 44),
(134, 46, 1, 2018, 150, 45),
(135, 46, 1, 2018, 12, 46),
(136, 46, 2, 2018, 1400, 42),
(137, 46, 2, 2018, 2700, 43),
(138, 46, 2, 2018, 24, 44),
(139, 46, 2, 2018, 140, 45),
(140, 46, 2, 2018, 13, 46),
(141, 46, 3, 2018, 1300, 42),
(142, 46, 3, 2018, 2300, 43),
(143, 46, 3, 2018, 23, 44),
(144, 46, 3, 2018, 150, 45),
(145, 46, 3, 2018, 14, 46),
(146, 46, 4, 2018, 1200, 42),
(147, 46, 4, 2018, 2500, 43),
(148, 46, 4, 2018, 22, 44),
(149, 46, 4, 2018, 140, 45),
(150, 46, 4, 2018, 15, 46),
(151, 46, 5, 2018, 1100, 42),
(152, 46, 5, 2018, 2700, 43),
(153, 46, 5, 2018, 21, 44),
(154, 46, 5, 2018, 150, 45),
(155, 46, 5, 2018, 16, 46),
(156, 46, 6, 2018, 1000, 42),
(157, 46, 6, 2018, 2300, 43),
(158, 46, 6, 2018, 20, 44),
(159, 46, 6, 2018, 140, 45),
(160, 46, 6, 2018, 17, 46),
(161, 46, 7, 2018, 900, 42),
(162, 46, 7, 2018, 2500, 43),
(163, 46, 7, 2018, 19, 44),
(164, 46, 7, 2018, 150, 45),
(165, 46, 7, 2018, 18, 46),
(166, 46, 8, 2018, 800, 42),
(167, 46, 8, 2018, 2700, 43),
(168, 46, 8, 2018, 18, 44),
(169, 46, 8, 2018, 140, 45),
(170, 46, 8, 2018, 19, 46),
(171, 47, 12, 2019, 1400, 42),
(172, 47, 12, 2019, 1500, 43),
(173, 47, 12, 2019, 19, 44),
(174, 47, 12, 2019, 141, 45),
(175, 47, 12, 2019, 1200, 46),
(176, 47, 11, 2019, 1400, 42),
(177, 47, 11, 2019, 1520, 43),
(178, 47, 11, 2019, 20, 44),
(179, 47, 11, 2019, 142, 45),
(180, 47, 11, 2019, 1300, 46),
(181, 47, 10, 2019, 1400, 42),
(182, 47, 10, 2019, 1540, 43),
(183, 47, 10, 2019, 21, 44),
(184, 47, 10, 2019, 143, 45),
(185, 47, 10, 2019, 1400, 46),
(186, 47, 9, 2019, 1400, 42),
(187, 47, 9, 2019, 1560, 43),
(188, 47, 9, 2019, 22, 44),
(189, 47, 9, 2019, 144, 45),
(190, 47, 9, 2019, 1500, 46),
(191, 47, 8, 2019, 1400, 42),
(192, 47, 8, 2019, 1580, 43),
(193, 47, 8, 2019, 23, 44),
(194, 47, 8, 2019, 145, 45),
(195, 47, 8, 2019, 1600, 46),
(196, 47, 7, 2019, 1400, 42),
(197, 47, 7, 2019, 1600, 43),
(198, 47, 7, 2019, 24, 44),
(199, 47, 7, 2019, 146, 45),
(200, 47, 7, 2019, 1700, 46);

-- --------------------------------------------------------

--
-- Structure de la table `site`
--

DROP TABLE IF EXISTS `site`;
CREATE TABLE IF NOT EXISTS `site` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `site`
--

INSERT INTO `site` (`id`, `url`) VALUES
(1, 'https://1984.agency/'),
(2, 'http://php.net/manual/fr/language.types.array.php'),
(3, 'https://1984.agency/404.php'),
(4, 'https://1984.agency/sefefe'),
(5, 'https://fr.wikipedia.org/wiki/Erreur_HTTP_302'),
(6, 'https://fr.wikipedia.org/wiki/Erreur_HTTP_302/nimp.php'),
(7, 'https://centreservices.fr/random.html'),
(9, 'https://getbootstrap.com/'),
(11, 'https://gambetta.centreservices.fr'),
(12, 'https://paris.multisites-manager.com');

-- --------------------------------------------------------

--
-- Structure de la table `slide`
--

DROP TABLE IF EXISTS `slide`;
CREATE TABLE IF NOT EXISTS `slide` (
  `numero` int(10) NOT NULL AUTO_INCREMENT,
  `num_projet` int(10) NOT NULL,
  `type_slide` int(10) NOT NULL,
  `lien_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `couleur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `couleur_texte` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `texte` text COLLATE utf8mb4_unicode_ci,
  `gras` text COLLATE utf8mb4_unicode_ci,
  `lien_video` text COLLATE utf8mb4_unicode_ci,
  `ordre` int(11) DEFAULT NULL,
  `date_creation` date NOT NULL,
  `date_modif` datetime DEFAULT NULL,
  `lien_externe` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`numero`),
  KEY `num_projet` (`num_projet`),
  KEY `type_slide` (`type_slide`)
) ENGINE=InnoDB AUTO_INCREMENT=640 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `slide`
--

INSERT INTO `slide` (`numero`, `num_projet`, `type_slide`, `lien_image`, `couleur`, `couleur_texte`, `titre`, `texte`, `gras`, `lien_video`, `ordre`, `date_creation`, `date_modif`, `lien_externe`) VALUES
(8, 11, 2, NULL, '#2239d5', '', '', '', '0', 'https://www.youtube.com/embed/lQz-AgmTm0w?rel=0&amp;showinfo=0', 7, '0000-00-00', '0000-00-00 00:00:00', ''),
(9, 11, 1, '', '#0000ff', '', 'Test', 'Sans image avec couleur', '1', '', 5, '0000-00-00', '2019-02-01 02:05:45', ''),
(10, 11, 1, '', '#de792f', '', 'Test couleur', 'Sans code', '1', '', 6, '0000-00-00', '0000-00-00 00:00:00', ''),
(13, 11, 1, 'images/01-02-2019f833fb16ad401e37690a0e7c43d437990c0d77aa9ba9903a15.png', '#ffffff', '', '', '', '0', '', 3, '0000-00-00', '2019-02-01 10:40:33', ''),
(14, 11, 1, 'images/01-02-2019eb70a481e5c844ff4c3810d06657acb3a577c653ddc5d10a92.png', '#ffffff', '#1d241d', 'Centre services', '', '1', '', 1, '0000-00-00', '2019-02-08 10:56:39', ''),
(15, 11, 1, 'images/01-02-201910691572dea3cc99bf88b2a439bb3e6c5cad272e4efb2ec85b.png', '#ffffff', '', 'Test insert', 'Image', '1', '', 4, '0000-00-00', '2019-02-01 10:40:53', ''),
(27, 11, 1, '', '#33ca35', '', 'Test', 'Update AJAX', '1', '', 2, '0000-00-00', '0000-00-00 00:00:00', ''),
(31, 11, 1, '', '#29914a', '', 'Test', 'Fermeture', '1', NULL, 10, '0000-00-00', '0000-00-00 00:00:00', ''),
(35, 11, 1, '', '#4f4b67', '', 'Test', 'New colopicker palette', '1', '', 8, '0000-00-00', '2019-01-29 09:24:32', ''),
(36, 11, 1, '', '#264856', '', 'Test', 'New palette', '1', NULL, 9, '0000-00-00', '0000-00-00 00:00:00', ''),
(39, 11, 1, '', '#564fc5', '', 'Test edit', 'Edit format datetime', '1', '', 11, '2019-01-24', '2019-01-24 03:58:03', ''),
(43, 12, 2, NULL, '#ffffff', '', NULL, NULL, NULL, 'https://www.dailymotion.com/embed/video/x719j70', 1, '2019-01-25', '0000-00-00 00:00:00', ''),
(70, 25, 1, '', '#94d717', '', 'gr', 'grtht', '1', '', 2, '2019-01-29', '2019-01-29 02:29:06', ''),
(154, 54, 1, '', '#c3f2f9', '#70ae26', 'Reset boutons crop', 'Première phrase du texte très long, deuxième phrase du texte très long, troisième phrase du texte très long, phrase encore plus longue pour voir comment ça se passe mais avec un texte beaucoup trop long', '1', '', 2, '2019-02-01', '2019-02-07 11:26:56', ''),
(163, 54, 1, '', '#1aa391', '#000000', 'Upload', 'Gif + inputs hidden', '1', '', 4, '2019-02-01', '2019-02-11 11:15:38', ''),
(190, 54, 1, 'images/14-02-20192d9ec5d8842e11625d3802b44016462a0c27157d13575cf3c2.png', '#ffffff', '#ffffff', 'Manque de temps', '', '0', '', 3, '2019-02-01', '2019-02-14 03:21:51', ''),
(199, 54, 1, '', '#421d3a', '#ffffff', 'Titre ajax', 'Update', '1', '', 7, '2019-02-01', '2019-05-10 06:29:53', ''),
(203, 54, 1, 'images/14-02-2019147d21996dafcfe34be3325339dc61ffbce49d3fe059ef8366.png', '#ffffff', '#ffffff', 'Centre Services', 'Plus que du ménage', '1', '', 8, '2019-02-01', '2019-02-14 03:26:11', 'https://docs.google.com/presentation/d/1QnWATcTlvjrYZZuC3xkDLJLnpJ4i3DNWfFpJ3q0EbFc/edit?ts=5c63e85c#slide=id.g4eff382980_0_24'),
(204, 25, 1, 'images/01-02-20199141209dd91604efcc210106478f72ab15c2707cadd73e14ea.png', '#ffffff', '', 'testos', 'stérone', '1', '', 1, '2019-02-01', '2019-02-04 09:37:56', ''),
(208, 59, 1, '', '#816f6f', '#ffffff', 'Centre Services', 'Orientis vero limes in longum protentus et rectum ab Euphratis fluminis ripis ad usque supercilia porrigitur Nili, laeva Saracenis conterminans gentibus, dextra pelagi fragoribus', '1', '', 1, '2019-02-06', '2019-02-08 10:23:39', ''),
(209, 59, 1, '', '#2d2828', '', 'TITRE 2', 'Test texte 2 ', '0', NULL, 2, '2019-02-06', '0000-00-00 00:00:00', ''),
(210, 59, 1, 'images/06-02-20197ce85515d0d7e4ee23d932afc4ea4b91f6551cb65d70b55dfc.png', '#2d2828', '#000000', 'TITRE 3 ', 'Test texte3', '0', '', 3, '2019-02-06', '2019-02-08 09:00:18', ''),
(211, 21, 1, '', '#69cef6', '', 'Test titres', 'test texte ', '1', '', 1, '2019-02-06', '2019-02-06 10:08:34', ''),
(288, 54, 1, '', '#ffffff', '#ab4848', 'Centre services', '', '1', '', 9, '2019-02-07', '2019-02-14 10:40:36', 'https://1984.agency'),
(429, 54, 5, NULL, NULL, '#5be83a', 'Merci', 'Première personne,Deuxième personne,Troisième personne', '1', NULL, NULL, '0000-00-00', '2019-02-13 08:11:49', ''),
(437, 54, 4, '', NULL, '#ffffff', 'Centre Services', 'Intro', '1', NULL, NULL, '2019-02-08', '2019-02-14 03:49:21', ''),
(450, 60, 5, NULL, NULL, '#57a377', 'Remerciements à :', 'Première personne,Deuxième personne', '0', NULL, 8, '0000-00-00', '2019-02-22 10:57:06', ''),
(464, 54, 1, '', '#6f6d6d', '#aff4ee', 'Ceci est un titre', 'Ceci est le texte associé au titre', '1', '', 10, '2019-02-12', '2019-02-13 08:13:50', ''),
(465, 60, 4, '', NULL, '#f5ffe3', 'Fghbhnr', NULL, '0', NULL, 1, '2019-02-12', '2019-02-22 03:35:38', ''),
(485, 54, 1, '', '#2d2121', '#6f989a', 'Test', 'Ajout lien externe', '1', '', 11, '2019-02-14', '2019-02-14 09:52:33', 'https://developers.google.com/speed/libraries/'),
(493, 54, 1, '', '#ffffff', '#1898fd', 'Titre en gras', 'Texte', '1', '', 12, '2019-02-14', '2019-05-10 06:29:34', ''),
(502, 68, 4, '', NULL, '#ffffff', 'Pompes Funèbres Orbecquoises', NULL, '0', NULL, 1, '2019-02-26', '0000-00-00 00:00:00', ''),
(503, 68, 1, '', '#ffffff', '#858585', 'Modernisation de votre charte graphique', '', '0', '', 2, '2019-02-21', '2019-02-22 03:28:25', ''),
(504, 68, 1, '', '#ffffff', '#858585', 'Éléments existants', 'Couleurs, signes distinctifs, typographie...', '1', '', 3, '2019-02-21', '2019-02-22 03:28:34', ''),
(505, 68, 1, 'images/21-02-2019fc8e944116c444cef26bbb17dd180121188d0d6ce7c3aad5c4.png', '#ffffff', '#ffffff', '', '', '0', NULL, 4, '2019-02-21', '0000-00-00 00:00:00', ''),
(506, 68, 1, 'images/21-02-20199bc84ab5d557560e8259d2b2ff4f860b0d93b36c8f0cefbfe8.png', '#ffffff', '#ffffff', '', '', '0', NULL, 5, '2019-02-21', '0000-00-00 00:00:00', ''),
(507, 68, 1, '', '#858585', '#ffffff', 'Observation de la concurrence', '', '0', '', 8, '2019-02-21', '2019-02-25 01:47:13', ''),
(511, 68, 1, '', '#ffffff', '#858585', 'Les symboles :', 'Certains éléments sont fréquemment utilisés.', '1', '', 9, '2019-02-21', '2019-02-25 04:37:13', ''),
(523, 60, 1, '', '#cace8d', '#ab2032', 'Lien', 'ergrebgrb', '0', '', 5, '2019-02-22', '2019-03-05 03:41:52', ''),
(524, 60, 1, '', '#3737b2', '#f90000', 'rth', 'rhr', '0', '', 3, '2019-02-22', '2019-02-25 10:07:51', ''),
(530, 60, 1, '', '#71b4f6', '#04ff00', 'rtegh', 'yegrgeg', '0', '', 7, '2019-02-22', '2019-02-25 10:06:11', ''),
(535, 68, 1, '', '#ffffff', '#858585', 'Objectif', 'Moderniser votre charte, et proposer un cadre pour les outils à venir.', '1', '', 6, '2019-02-22', '2019-02-25 10:54:19', ''),
(536, 68, 1, '', '#ffffff', '#858585', 'Impératifs', 'Exploiter la ligne actuelle pour déclencher la reconnaissance de certains marqueurs.', '1', '', 7, '2019-02-22', '2019-02-25 11:33:35', ''),
(537, 68, 1, 'images/22-02-2019e6d86e66244e556b89be37b40843217273fbf46bb87711674a.png', '#ffffff', '#ffffff', '', '', '0', '', 10, '2019-02-22', '2019-02-22 01:53:20', ''),
(538, 68, 1, 'images/22-02-20190d3d52695ca2e5c2444cb37bf9787a73772bc6de804b28e524.png', '#ffffff', '#ffffff', '', '', '0', NULL, 11, '2019-02-22', '0000-00-00 00:00:00', ''),
(539, 68, 1, 'images/25-02-20192c37883712bd17a04301e6e8838c0244e9e99c51e84195ca23.png', '#ffffff', '#ffffff', '', '', '0', '', 12, '2019-02-22', '2019-02-25 01:58:28', ''),
(540, 68, 1, 'images/25-02-2019c8cd0d386177989754960cc53aed92a25ed72739eb7c15fdfc.png', '#ffffff', '#ffffff', '', '', '0', '', 13, '2019-02-22', '2019-02-25 01:56:25', ''),
(541, 68, 1, 'images/22-02-201959b88292a232da8387d48e7a0ed483c62c4eccbc3c1994a659.png', '#ffffff', '#ffffff', '', '', '0', NULL, 20, '2019-02-22', '0000-00-00 00:00:00', ''),
(542, 68, 1, 'images/25-02-2019abbfcd53f4024e1d1aec82589b710d03dc689a1b6de47dbbf4.png', '#ffffff', '#ffffff', '', '', '0', '', 14, '2019-02-22', '2019-02-25 01:57:19', ''),
(543, 68, 1, '', '#fbfbfb', '#858585', 'Les couleurs :', 'Le bleu est très présent.', '1', '', 15, '2019-02-22', '2019-02-22 03:39:18', ''),
(546, 60, 1, '', '#837272', '#20bde7', 'jrjrtjrtj', 'rjrj', '0', '', 4, '2019-02-22', '2019-02-25 10:06:54', ''),
(566, 68, 1, '', '#fbfbfb', '#858585', 'Nous vous présentons votre nouvelle image', '', '0', '', 27, '2019-02-22', '2019-02-25 02:12:11', ''),
(567, 60, 1, '', '#9adda5', '#627383', 'trhrh', '', '0', '', 2, '2019-02-22', '2019-02-22 03:36:43', ''),
(572, 68, 1, 'images/25-02-2019cc7eeb23587c87fe548f7a39004a6c98973955553ed7698c0f.png', '#ffffff', '#ffffff', '', '', '0', '', 16, '2019-02-22', '2019-02-25 02:00:28', ''),
(573, 68, 1, 'images/22-02-20191e0557b84f43ef748a783d0054216688a8ddd3aee04e8131df.png', '#ffffff', '#ffffff', '', '', '0', NULL, 17, '2019-02-22', '0000-00-00 00:00:00', ''),
(574, 68, 1, 'images/25-02-2019dbf4c433c93127ba3d1969150d6ef33fc847998fd1c9a235cf.png', '#ffffff', '#ffffff', '', '', '0', '', 18, '2019-02-22', '2019-02-25 02:00:45', ''),
(575, 68, 1, 'images/22-02-20196e07b567e4c0c59753781418da8c81d27329058d58bfcf2bb1.png', '#ffffff', '#ffffff', '', '', '0', NULL, 19, '2019-02-22', '0000-00-00 00:00:00', ''),
(576, 69, 1, '', '#000000', '#ffffff', 'Première slide', 'Avec du texte', '0', NULL, 1, '2019-02-24', '0000-00-00 00:00:00', ''),
(577, 69, 1, '', '#ffffff', '#000000', 'Deuxième slide', 'Avec du texte', '0', NULL, 2, '2019-02-24', '0000-00-00 00:00:00', ''),
(578, 60, 1, '', '#71b4f6', '#ab2032', '', '', '0', NULL, 6, '2019-02-25', '0000-00-00 00:00:00', ''),
(579, 68, 1, '', '#fbfbfb', '#858585', 'Une recherche effectuée en 4 étapes ', '', '0', '', 28, '2019-02-25', '2019-02-25 11:10:58', ''),
(580, 68, 1, '', '#ffffff', '#858585', 'Première étape', 'Poser les marqueurs de votre identité visuelle', '1', '', 29, '2019-02-25', '2019-02-25 11:18:43', ''),
(581, 68, 1, '', '#fbfbfb', '#858585', 'Deuxième étape ', 'Moderniser', '1', '', 37, '2019-02-25', '2019-02-25 11:18:52', ''),
(582, 68, 1, '', '#ffffff', '#858585', 'Troisième étape ', 'Aller plus loin ', '1', '', 44, '2019-02-25', '2019-02-25 11:18:13', ''),
(583, 68, 1, '', '#fbfbfb', '#858585', 'Quatrième étape', 'Imaginer ', '1', '', 65, '2019-02-25', '2019-02-25 11:20:48', ''),
(584, 68, 1, '', '#ffffff', '#858585', 'La notoriété, principal facteur de reconnaissance.', '', '0', '', 21, '2019-02-25', '2019-02-25 02:07:04', ''),
(585, 68, 1, '', '#ffffff', '#858585', 'Une activité dont l\'image est parfois vieillissante, et souvent peu travaillée.', '', '0', '', 22, '2019-02-25', '2019-02-25 02:04:29', ''),
(586, 68, 1, 'images/25-02-2019145edf6e99e3ba024a75e55c39842b341053481270257562b2.png', '#ffffff', '#ffffff', '', '', '0', NULL, 24, '2019-02-25', '0000-00-00 00:00:00', ''),
(587, 68, 1, 'images/25-02-2019ef600da327189d61d723b6abca3789582247d6483e15168182.png', '#ffffff', '#ffffff', '', '', '0', NULL, 25, '2019-02-25', '0000-00-00 00:00:00', ''),
(588, 68, 1, '', '#fbfbfb', '#858585', 'Si ce n\'est pour certaines enseignes...', '', '0', '', 23, '2019-02-25', '2019-02-25 12:02:27', ''),
(589, 68, 1, 'images/25-02-2019e9398fefc394cab4f1d3f57226073df315a112210e0221115f.png', '#fbfbfb', '#ffffff', '', '', '0', '', 26, '2019-02-25', '2019-02-25 01:49:33', ''),
(590, 68, 5, NULL, NULL, '#ffffff', 'Merci.', 'Godeffroy Mbilisi, François Petit, Sandrine Augusto', '0', NULL, 73, '0000-00-00', '2019-02-25 02:13:57', ''),
(591, 68, 1, 'images/25-02-2019b62c2ac01a210a35b6d1416f0cb14e3a2ec1135bde3bbc837c.png', '#ffffff', '#ffffff', '', '', '0', NULL, 31, '2019-02-25', '0000-00-00 00:00:00', ''),
(592, 68, 1, 'images/25-02-20190a89155aed372c6cd99ea0439a4379f273d82a52fc035de8bb.png', '#ffffff', '#ffffff', '', '', '0', NULL, 32, '2019-02-25', '0000-00-00 00:00:00', ''),
(593, 68, 1, 'images/25-02-2019da816a144cde8557bb55707bc1dcdb5b3c85e8fa57d9976a4a.png', '#ffffff', '#ffffff', 'test texte ', '', '0', '', 35, '2019-02-25', '2019-02-25 03:01:45', ''),
(594, 68, 1, 'images/25-02-20195437454846922d84427a3c50c4d50b7e0747fe37885be12e60.png', '#ffffff', '#ffffff', '', '', '0', NULL, 39, '2019-02-25', '0000-00-00 00:00:00', ''),
(595, 68, 1, '', '#ffffff', '#858585', 'Ceci est notre espace de travail.', '', '0', '', 30, '2019-02-25', '2019-02-25 04:42:32', ''),
(596, 68, 1, '', '#ffffff', '#858585', 'Bandeau de couleur violette pour rappeler la charte précédente.', '', '0', '', 33, '2019-02-25', '2019-02-25 04:42:23', ''),
(597, 68, 1, '', '#ffffff', '#858585', 'Simplification des photos des fleurs par des pastilles de couleurs.', '', '0', '', 34, '2019-02-25', '2019-02-25 04:42:17', ''),
(598, 68, 1, '', '#ffffff', '#858585', 'Toujours dans des teintes rappelant l\'ancienne charte graphique.', '', '0', '', 36, '2019-02-25', '2019-02-25 04:42:02', ''),
(599, 68, 1, 'images/25-02-201982c983c627bcb71186b6c68573cf25dd28cc8a255b4d7cbcb7.png', '#ffffff', '#ffffff', '', '', '0', NULL, 40, '2019-02-25', '0000-00-00 00:00:00', ''),
(600, 68, 1, 'images/25-02-201997bae25c53e59d03eac12ed466a9a4b2279e968c6c9990e3f7.png', '#ffffff', '#ffffff', '', '', '0', NULL, 42, '2019-02-25', '0000-00-00 00:00:00', ''),
(601, 68, 1, '', '#ffffff', '#858585', 'Choix d\'une typo haute sans empattement pour plus de modernité, et d\'esthétisme.', '', '0', '', 38, '2019-02-25', '2019-02-25 04:40:44', ''),
(602, 68, 1, '', '#ffffff', '#858585', 'Mettre l\'accent sur le mot \"ORBECQUOISES\"  ', 'qui sera dans la même typo, mais dans une graisse et taille plus importantes. ', '0', '', 41, '2019-02-25', '2019-02-25 04:41:11', ''),
(603, 68, 1, 'images/25-02-2019a87f1bfa10a6ec9c1db094dc13846056e8d209d0f60c3cb946.png', '#ffffff', '#ffffff', '', '', '0', NULL, 43, '2019-02-25', '0000-00-00 00:00:00', ''),
(604, 68, 1, 'images/25-02-20190a46260699ceaae8568e2b752218b298dadd385257e22dfb38.png', '#ffffff', '#ffffff', '', '', '0', NULL, 45, '2019-02-25', '0000-00-00 00:00:00', ''),
(605, 68, 1, 'images/25-02-20199b5b429dddda8d538af38c20a1bd365aa28d9c1858d69ae310.png', '#ffffff', '#ffffff', '', '', '0', NULL, 46, '2019-02-25', '0000-00-00 00:00:00', ''),
(606, 68, 1, 'images/25-02-201939b3af1a49ae5c0e6a880c77caf2ae0de63039f57d426b2ef8.png', '#ffffff', '#ffffff', '', '', '0', NULL, 50, '2019-02-25', '0000-00-00 00:00:00', ''),
(607, 68, 1, '', '#ffffff', '#858585', 'Tentons de fusionner ces deux éléments.', '', '0', '', 47, '2019-02-25', '2019-02-25 03:43:10', ''),
(608, 68, 1, '', '#ffffff', '#858585', 'Pourquoi ? ', 'pour ne pas perdre cet aspect qui existait dans l\'ancienne charte graphique.', '1', '', 48, '2019-02-25', '2019-02-25 04:41:32', ''),
(609, 68, 1, '', '#ffffff', '#858585', 'Mais surtout, ', 'pour créer un signe distinctif, fort et unique pour votre entreprise.', '1', '', 49, '2019-02-25', '2019-02-25 04:41:39', ''),
(610, 68, 1, 'images/25-02-2019a548bf383748dfac41350df5917324cb66cd67908d4fd99aa3.png', '#ffffff', '#ffffff', '', '', '0', NULL, 51, '2019-02-25', '0000-00-00 00:00:00', ''),
(611, 68, 1, 'images/25-02-201987843d94d9b885b7db7019ff047e8b63929160af0d7f6e2c94.png', '#ffffff', '#ffffff', '', '', '0', NULL, 52, '2019-02-25', '0000-00-00 00:00:00', ''),
(612, 68, 1, 'images/25-02-2019405db76f0535d17707e51dbf9c5c72feb27c43f5d0664ba1bc.png', '#ffffff', '#ffffff', '', '', '0', NULL, 53, '2019-02-25', '0000-00-00 00:00:00', ''),
(613, 68, 1, 'images/25-02-2019358cc1038ae7ba35bf86fb5611462392a300b11fa953755f03.png', '#ffffff', '#ffffff', '', '', '0', NULL, 54, '2019-02-25', '0000-00-00 00:00:00', ''),
(614, 68, 1, 'images/25-02-2019d5aef8a1a67e91cabe3279f72248979d0b9ae6de20e1be97ca.png', '#ffffff', '#ffffff', '', '', '0', '', 55, '2019-02-25', '2019-02-25 04:09:49', ''),
(615, 68, 1, 'images/25-02-20190456e9c5d9a205a51fe26b56df690a9491dcd74ea7005245b2.png', '#ffffff', '#ffffff', '', '', '0', NULL, 56, '2019-02-25', '0000-00-00 00:00:00', ''),
(616, 68, 1, 'images/25-02-2019597a34c8a41952a0b9f93a19153c8b7c22d187cbdeeee97466.png', '#ffffff', '#ffffff', '', '', '0', NULL, 57, '2019-02-25', '0000-00-00 00:00:00', ''),
(617, 68, 1, 'images/25-02-20198a5471bf8f031bb64f27957046cb630c334180287e2e1513dd.png', '#ffffff', '#ffffff', '', '', '0', NULL, 58, '2019-02-25', '0000-00-00 00:00:00', ''),
(618, 68, 1, 'images/25-02-201973cba96d5ae3e9b9188042a52978dc94161d4d547fe40a6950.png', '#ffffff', '#ffffff', '', '', '0', NULL, 59, '2019-02-25', '0000-00-00 00:00:00', ''),
(619, 68, 1, 'images/25-02-20192b2d14c7509c2a0be868b4b8d4f61c93de31004034a98951d7.png', '#ffffff', '#ffffff', '', '', '0', NULL, 60, '2019-02-25', '0000-00-00 00:00:00', ''),
(620, 68, 1, 'images/25-02-20194b48a43a4a67462ee07bc9b0e0353d4b3fa1aa6ca58f444ade.png', '#ffffff', '#ffffff', '', '', '0', NULL, 62, '2019-02-25', '0000-00-00 00:00:00', ''),
(621, 68, 1, 'images/25-02-20191ea3497d77263c515461f80f9eb6d8819475addf1ae6eea7d7.png', '#ffffff', '#ffffff', '', '', '0', NULL, 61, '2019-02-25', '0000-00-00 00:00:00', ''),
(622, 68, 1, 'images/25-02-201976f5147ac7f705520294580bb5574f3698ac487d7e51b1e0dc.png', '#ffffff', '#ffffff', '', '', '0', NULL, 63, '2019-02-25', '0000-00-00 00:00:00', ''),
(623, 68, 1, 'images/25-02-2019fc2d7c7e84fde0ef15e68e02de8b22d6ba54e296b02b92fe62.png', '#ffffff', '#ffffff', '', '', '0', NULL, 64, '2019-02-25', '0000-00-00 00:00:00', ''),
(624, 68, 1, 'images/25-02-2019272ef339b66ad9ae2d2a3828eaa4d4cb4830ba99fa264ecee2.png', '#ffffff', '#ffffff', '', '', '0', NULL, 66, '2019-02-25', '0000-00-00 00:00:00', ''),
(625, 68, 1, 'images/25-02-2019a44a9d291a2f1c48dbe0d88fb1b4fee53ac996bdb96cbb4c60.png', '#ffffff', '#ffffff', '', '', '0', NULL, 67, '2019-02-25', '0000-00-00 00:00:00', ''),
(626, 68, 1, 'images/25-02-2019868a329faf0a8ccb92717a38cf117369848584453c5be69f85.png', '#ffffff', '#ffffff', '', '', '0', NULL, 68, '2019-02-25', '0000-00-00 00:00:00', ''),
(627, 68, 1, 'images/25-02-20194499a99e2a43421b686db9db27725d41d73d6343d36552a9f7.png', '#ffffff', '#ffffff', '', '', '0', NULL, 69, '2019-02-25', '0000-00-00 00:00:00', ''),
(629, 68, 1, 'images/25-02-201928d920105652e75c41774b23f7fc7df7a812a134b3397c15d7.png', '#ffffff', '#ffffff', '', '', '0', '', 70, '2019-02-25', '2019-02-25 04:34:04', ''),
(631, 68, 1, 'images/25-02-2019f1304f063959a6fa8606a825f0643ae6a60326a5625928544b.png', '#ffffff', '#ffffff', '', '', '0', NULL, 72, '2019-02-25', '0000-00-00 00:00:00', ''),
(632, 68, 1, 'images/25-02-2019d4ad88f9d4b7ec07612c66b77b5570928c7760b8764c670be0.png', '#ffffff', '#ffffff', '', '', '0', NULL, 71, '2019-02-25', '0000-00-00 00:00:00', ''),
(636, 69, 1, '', '#ab8787', '#000000', 'fbfbf', 'bfb', '1', '', 3, '2019-04-30', '2019-04-30 12:30:06', 'fbfb'),
(638, 82, 1, '', '#000000', '#ffffff', 'Epreuve E4', 'Samvel', '1', '', 1, '2019-05-06', '2019-05-06 07:06:13', 'youtube.com'),
(639, 82, 1, '', '#d69b9b', '#3971ca', 'ergerg', 'g', '0', NULL, 2, '2019-05-06', NULL, 'geg');

-- --------------------------------------------------------

--
-- Structure de la table `type_slide`
--

DROP TABLE IF EXISTS `type_slide`;
CREATE TABLE IF NOT EXISTS `type_slide` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type_slide`
--

INSERT INTO `type_slide` (`id`, `nom`) VALUES
(1, 'Présentation'),
(2, 'Vidéo'),
(4, 'Introduction'),
(5, 'Remerciements');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `forum_category`
--
ALTER TABLE `forum_category`
  ADD CONSTRAINT `forum_category_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `forum_user` (`id`);

--
-- Contraintes pour la table `forum_notification`
--
ALTER TABLE `forum_notification`
  ADD CONSTRAINT `forum_notification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `forum_user` (`id`);

--
-- Contraintes pour la table `forum_post`
--
ALTER TABLE `forum_post`
  ADD CONSTRAINT `forum_post_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `forum_topic` (`id`),
  ADD CONSTRAINT `forum_post_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `forum_user` (`id`);

--
-- Contraintes pour la table `forum_topic`
--
ALTER TABLE `forum_topic`
  ADD CONSTRAINT `forum_topic_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `forum_category` (`id`),
  ADD CONSTRAINT `forum_topic_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `forum_user` (`id`),
  ADD CONSTRAINT `forum_topic_ibfk_3` FOREIGN KEY (`last_post`) REFERENCES `forum_post` (`id`);

--
-- Contraintes pour la table `forum_user`
--
ALTER TABLE `forum_user`
  ADD CONSTRAINT `forum_user_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `forum_user_type` (`id`);

--
-- Contraintes pour la table `projet`
--
ALTER TABLE `projet`
  ADD CONSTRAINT `projet_ibfk_1` FOREIGN KEY (`idclient`) REFERENCES `client` (`id`);

--
-- Contraintes pour la table `reportings`
--
ALTER TABLE `reportings`
  ADD CONSTRAINT `reportings_ibfk_1` FOREIGN KEY (`idOrganization`) REFERENCES `organization` (`id`),
  ADD CONSTRAINT `reportings_ibfk_2` FOREIGN KEY (`idGoalType`) REFERENCES `goalstype` (`id`);

--
-- Contraintes pour la table `slide`
--
ALTER TABLE `slide`
  ADD CONSTRAINT `slide_ibfk_1` FOREIGN KEY (`num_projet`) REFERENCES `projet` (`numero`),
  ADD CONSTRAINT `slide_ibfk_2` FOREIGN KEY (`type_slide`) REFERENCES `type_slide` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

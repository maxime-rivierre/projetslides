<?php
    require_once $_SERVER['DOCUMENT_ROOT'].'/projet_slides/conf.php';
	ini_set('display_errors', 1);
	error_reporting(E_ALL);

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;;

    function sendMail($arrayResponse)
    {
		$mail = new PHPMailer;
		$mail->CharSet = 'UTF-8';
		$mail->Encoding = 'base64';
		$mail->isSMTP();

		// 0 = off
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 0;

		
		$mail->Debugoutput = 'html';
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 587;
		$mail->SMTPSecure = 'tls';
		$mail->SMTPAuth = true;
		$mail->Username = "contact.maximerivierre@gmail.com";
	    $mail->Password = "Contact1248";
		// Envoyeur
        try {
            $mail->setFrom('contact.maximerivierre@gmail.com', 'Contact Maxime Rivierre');
        } catch (Exception $e) {
            $e->getMessage();
        }
        // Adresse réponse
		$mail->addReplyTo('contact.maximerivierre@gmail.com', '');
		// Destinataire
		$mail->addAddress('maximerivir@gmail.com', 'Maxime Rivierre');

		$nb_erreurs = count($arrayResponse);
		if($nb_erreurs == 1)
		{
			$titre = "URGENT - Site en panne";
		}

		else
		{
			$titre = "URGENT - Liste des sites en pannne";
		}
		$mail->Subject = $titre;
		date_default_timezone_set("Europe/Paris");
		$date_modif = gmdate('d-m-Y à H:i:s');
		$message = "Résultat de l'analyse du ".$date_modif." :<br/><br/>";

		if(!empty($arrayResponse))
		{
			foreach ($arrayResponse as $row) 
			{
				$message .= $row;
			}

			$message .= "<br/>";
		}

		else
		{
			$message .= " - Aucune panne détectée.<br/><br/>";
		}
		
		$mail->Body = $message;
		$mail->AltBody = "Une erreur s'est produite pendant l'envoi du mail";

        try {
            if (!$mail->send()) {
                echo "Erreur: " . $mail->ErrorInfo;
            } else {
                echo "Message envoyé!";
            }
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function dbConnect()
    {
        $server = "mysql:host=localhost;dbname=".$GLOBALS['server'];
        $user = $GLOBALS['user'];
        $pwd = $GLOBALS['pwd'];

        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_CASE => PDO::CASE_LOWER,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        try
        {
            $link = new PDO($server, $user, $pwd, $options);
            return $link;
        }
        catch (Exception $e)
        {
            echo "Connection à MySql impossible : ", $e->getMessage();
            die();
        }
    }

	function get_http_response_code($url)
	{
		stream_context_set_default( [
		    'ssl' => [
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		    ],
		]);
		$url = trim($url);
		$headers = get_headers($url);
		return substr($headers[0], 9, 3);
	}

	function checkUrl($url)
	{
		$httpCode = get_http_response_code($url);

		switch($httpCode) 
		{
		   	case 200:
		        $error_status="200: Tout est normal.";
		        break;
		    case 302:
		        $error_status="302: Redirection vers une autre page.";
		        break;
		    case 401:
		        $error_status="401: Une authentification est requise.";
		        break;
		    case 403:
		        $error_status="403: Accès interdit.";
		        break;
		    case 404:
		        $error_status="404: Page non trouvée.";
		        break;
		    case 500:
		        $error_status="500: Erreur interne.";
		        break;
		    case 502:
		        $error_status="502: Erreur de passerelle ou proxy.";
		        break;
		    case 503:
		    	$error_status="503: Service temporairement indisponible ou en maintenance.";
		        break;
		    case 504:
		        $error_status="504: Temps d'attente dépassé.";
		        break;
		    default:
		        $error_status="Une erreur est survenue - Code : " . $httpCode. ".";
		        break;
		}

		return $error_status;
	}

	function addhttp($url)
	{
	    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) 
	    {
	        $url = "http://" . $url;
	    }
	    return $url;
	}

	$arrayResponse = [];
    $db = dbConnect();
	$data = $db->query('SELECT * FROM client WHERE site != "" ORDER BY id DESC');
	$data = $data->fetchAll(PDO::FETCH_ASSOC);
	foreach ($data as $row) 
	{
		if(!empty($row)) {
            $url = $row['site'];
			$url_verified = addhttp($url);
			$status = checkUrl($url_verified);
			if($status == 200)
			{
				continue;
			}
			else
			{
				echo $response =  'Site : '.$url.' -- Status : '.$status.'<br/>';
				array_push($arrayResponse, $response);
			}
		}
	}

	$nb_erreurs = count($arrayResponse);

	if($nb_erreurs != 0)
	{
		sendMail($arrayResponse);
	}
?>
<li id='infoReseauNav' class='defaultHeaderTitle'>
    <span class='headerTitle'>Outil de présentation de projet</span>
    <div class='search-row pull-right'>
        <div class="iconInput relative">
            <i class="material-icons openSearch">search</i>
            <i class="material-icons placeholder">search</i>
            <input type='text' id='search-user' class='form-control searchBar' placeholder='Nom de projet ?'/>
        </div>
    </div>
</li>
<?php
	include_once 'main.php';
	ini_set('display_errors',1); 
	error_reporting(E_ALL); 

	$url_projet = $_POST['uprojectUrl'];

	$bold = 0;

	if(isset($_POST['ubold']))
	{
		$bold = 1;
	}
		
	$numero = $_POST['unumero'];

	$colorpicker = $_POST['ucolorPicked'];
	$colorHex = $_POST['ucolorHex'];
	$couleur = "";

	if($colorpicker == "#ffffff" && $colorHex != "")
	{
		$couleur = $colorHex;
	}

	else
	{
		$couleur = $colorpicker;
	}

	$colorpickerText = $_POST['ucolorPickedText'];
	$colorHexText = $_POST['ucolorHexText'];
	$couleur_text = "";

	if($colorpickerText == "#ffffff" && $colorHexText != "")
	{
		$couleur_text = $colorHexText;
	}

	else
	{
		$couleur_text = $colorpickerText;
	}

	$titre = $_POST['utitre'];
	$texte = $_POST['utexte'];
	$video = $_POST['uvideo'];
	$externe = $_POST['uexterne'];
	date_default_timezone_set("Europe/Paris");
	$date_modif = gmdate('Y-m-d h:i:s');

	if(!empty($_POST['imgUploaded'])) {
    	// Récupére l'image
	    $split = explode(',',$_POST['imgUploaded']);
	    $data = $split[1];
	    $data = base64_decode($data);
	    $split = explode(';',$split[0])[0];
	    // Récupére l'extension
	    $extension = explode('/',$split)[1];
	    // Définit le dossier
	    $directory = '../images/';
	    $chemin = 'images/';
	    // Définit le nom
	    $token = createToken(25);
	    $filename = date('d-m-Y').$token.'.'.$extension;
	    $chemin_image = $chemin.$filename;
	    // Upload
	    file_put_contents($directory.$filename, $data);
	    // Mise à jour en BDD

	    $temp = $_POST['uoldimg'];

	    if(!empty($temp))
	    {
	    	$image = "../".$temp;
			$del = deleteFile($image);
	    }
		
		updateSlideImage($numero, $chemin_image, $couleur, $couleur_text, $titre, $texte, $video, $bold, $date_modif, $externe);
	}  

	else
	{
		if($_POST['utodelete'] == "true" && !empty($_POST['uoldimg']))
		{
			$temp = $_POST['uoldimg'];

		    if(!empty($temp))
		    {
		    	$image = "../".$temp;
				$del = deleteFile($image);
		    }

		    $chemin_image = "";	

		    updateSlideImage($numero, $chemin_image, $couleur, $couleur_text, $titre, $texte, $video, $bold, $date_modif, $externe);
		}

		else
		{
			updateSlide($numero, $couleur, $couleur_text, $titre, $texte, $video, $bold, $date_modif, $externe);
		}	
	}

	$purls = createUrl($url_projet);
	$purl_paral = $purls['paral'];

	$data = queryDb('SELECT type_slide.nom as type, slide.* FROM type_slide, slide WHERE type_slide.id = slide.type_slide AND numero ='.$numero);

	foreach ($data as $row) 
	{
		$pnumero = $row["numero"];
		$ptype_slide = $row["type"];
		$plien_image = $row["lien_image"];
		$pcouleur = $row["couleur"];
		$pfont = $row["couleur_texte"];
		$ptitre = $row["titre"];
		$ptexte = $row["texte"];
		$pgras = $row["gras"];
		$plien_video =$row["lien_video"];
		$pordre = $row["ordre"];
		
					if ($ptype_slide == "Présentation") 
					{
						// Affichage si image
						if(!empty($plien_image))
						{
							if(!empty($ptitre) && $pgras == 1)
							{
								if(!empty($ptexte))
								{
									// Image + titre gras + texte
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $pnumero.'">
									<div class="preview-slide">
								    		<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$plien_image.'" class="img-thumbnail"></a>
								    		<div class="contentp">
								    			<h3 style="color:'.$pfont.'"><strong>'.$ptitre.'</strong></h3>
								    			<h3 style="color:'.$pfont.'">'.$ptexte.'</h3>
								    		</div>
								    		<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    		<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
								    	</div>';
									}

								else
								{	// Image + titre gras
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $pnumero.'">
									<div class="preview-slide">
							    			<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$plien_image.'" class="img-thumbnail"></a>
							    			<div class="contentp">
							    				<h3 style="color:'.$pfont.'"><strong>'.$ptitre.'</strong></h3>
							    			</div>
							    			<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
							    			<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
							    		</div>';
								}						
							}

							elseif (!empty($ptitre) && $pgras == 0) 
							{
								if(!empty($ptexte))
								{
									// Image + titre + texte
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $pnumero.'">
									<div class="preview-slide">
								    		<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$plien_image.'" class="img-thumbnail"></a>
								    		<div class="contentp">
								    			<h3 style="color:'.$pfont.'">'.$ptitre.'</h3>
								    			<h3 style="color:'.$pfont.'">'.$ptexte.'</h3>
								    		</div>
								    		<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    		<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
								    	</div>';
								}

								else
								{	
									// Image + titre
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $pnumero.'">
									<div class="preview-slide">
								    	<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$plien_image.'" class="img-thumbnail"></a>
								    	<div class="contentp">
								    		<h3 style="color:'.$pfont.'">'.$ptitre.'</h3>
								    	</div>
								    	<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    	<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    	</div>
								    </div>';
								}
							}

							else
							{
								// Image
								echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $pnumero.'">
								<div class="preview-slide">
								    	<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$plien_image.'" class="img-thumbnail"></a>
								    	<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    	<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    	</div>
								    </div>';
							}	
						}

						// Affichage sans image
						else
						{
							if(!empty($ptitre) && $pgras == 1)
							{
								if(!empty($ptexte))
								{
									// Couleur + Titre gras + texte
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $pnumero.'">
									<div class="no-image" style="background-color:'.$pcouleur.'">
						    				<a href="javascript:void(0);" class="editSlide"></a>
						    				<div class="contentp">
						    					<h3 style="color:'.$pfont.'"><strong>'.$ptitre.'</strong></h3>
								    			<h3 style="color:'.$pfont.'">'.$ptexte.'</h3>
								    		</div>
								    		<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    		<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
						    			</div>';
								}

								else
								{
									// Couleur + Titre gras
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $pnumero.'">
									<div class="no-image" style="background-color:'.$pcouleur.'">
						    				<a href="javascript:void(0);" class="editSlide"></a>
						    				<div class="contentp">
						    					<h3 style="color:'.$pfont.'"><strong>'.$ptitre.'</strong></h3>
						    				</div>
						    				<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
						    				<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
						    			</div>';
								}						
							}

							elseif (!empty($ptitre) && $pgras == 0) 
							{
								if(!empty($ptexte))
								{
									// Couleur + Titre + texte
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $pnumero.'">
									<div class="no-image" style="background-color:'.$pcouleur.'">
						    				<a href="javascript:void(0);" class="editSlide"></a>
						    				<div class="contentp">
						    					<h3 style="color:'.$pfont.'">'.$ptitre.'</h3>
								    			<h3 style="color:'.$pfont.'">'.$ptexte.'</h3>
								    		</div>
								    		<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    		<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
						    			</div>';
									
								}

								else
								{
									// Couleur + Titre
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $pnumero.'">
									<div class="no-image" style="background-color:'.$pcouleur.'">
						    				<a href="javascript:void(0);" class="editSlide"></a>
						    				<div class="contentp">
						    					<h3 style="color:'.$pfont.'">'.$ptitre.'</h3>
						    				</div>
						    				<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
						    				<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
						    			</div>';
								}
							}

							else
							{
								// Couleur 
								echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $pnumero.'">
								<div class="no-image" style="background-color:'.$pcouleur.'">
						    			<a href="javascript:void(0);" class="editSlide"></a>
						    			<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
						    			<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    	</div>
						    		</div>';				
							}	
						}
					}

					// Affichage si vidéo
					else
					{
						echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-video s" id="item_'. $pnumero.'">
						<div class="video editSlide">
				    			<iframe class="preview-video" width="100%" height="100%" src="'.$plien_video.'" frameborder="0" allowfullscreen></iframe>
				    			<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
				    			<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								</button>
								</div>
				    		</div>';
					}
				}
?>
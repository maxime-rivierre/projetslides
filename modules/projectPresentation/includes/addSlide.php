<?php 
	include_once 'main.php';
	ini_set('display_errors',1); 
	error_reporting(E_ALL); 

	$url_projet = $_POST['projectUrl'];

	$bold = 0;

	if(isset($_POST['bold']))
	{
		$bold = 1;
	}
		
	$id_url_projet = getProjectId($url_projet);	
	$type_choisi = $_POST['selectedType'];

	$colorpicker = $_POST['colorPicked'];
	$colorHex = $_POST['colorHex'];
	$couleur = "";

	if($colorpicker == "#ffffff" && $colorHex != "")
	{
		$couleur = $colorHex;
	}

	else
	{
		$couleur = $colorpicker;
	}

	$colorpickerText = $_POST['colorTextPicked'];
	$colorHexText = $_POST['colorHexText'];
	$couleurText = "";

	if($colorpickerText == "#ffffff" && $colorHexText != "")
	{
		$couleurText = $colorHexText;
	}

	else
	{
		$couleurText = $colorpickerText;
	}

	$titre = $_POST['titre'];
	$texte = $_POST['texte'];
	$video = $_POST['video'];
	$externe = $_POST['externe'];
	$last_ordre = getLastSlideOrder($id_url_projet);
	$ordre = 0;

	if($last_ordre == 0)
	{
		$ordre = 1;
	}

	else
	{
		$ordre = $last_ordre + 1;
	}
	
	$date_creation = date('Y-m-d');

	$types = queryDb('SELECT * FROM type_slide');
	$id_type = 0;

	foreach ($types as $type) 
	{
		if ($type['nom'] == $type_choisi) 
		{
			$id_type = $type['id'];
		}
	}

	if(!empty($_POST['imgUploaded'])) {
    	// Récupére l'image
	    $split = explode(',',$_POST['imgUploaded']);
	    $data = $split[1];
	    $data = base64_decode($data);
	    $split = explode(';',$split[0])[0];
	    // Récupére l'extension
	    $extension = explode('/',$split)[1];
	    // Définit le dossier
	    $directory = '../images/';
	    $chemin = 'images/';
	    // Définit le nom
	    $token = createToken(25);
	    $filename = date('d-m-Y').$token.'.'.$extension;
	    $chemin_image = $chemin.$filename;
	    // Upload
	    file_put_contents($directory.$filename, $data);
	    // Mise à jour en BDD

		if ($type_choisi == "Présentation")
		{
			addSlide($id_url_projet, $id_type, $chemin_image, $couleur, $couleurText, $titre, $texte, $bold, $ordre, $date_creation, $externe);
		}

		elseif ($type_choisi == "Vidéo") 
		{
			addSlideVideo($id_url_projet, $id_type, $couleur, $video, $ordre, $date_creation, $externe);
		}
	}

	else
	{
		$chemin_image = "";
		if ($type_choisi == "Présentation")
		{
			addSlide($id_url_projet, $id_type, $chemin_image, $couleur, $couleurText, $titre, $texte, $bold, $ordre, $date_creation, $externe);
			
		}

		elseif ($type_choisi == "Vidéo") 
		{
			addSlideVideo($id_url_projet, $id_type, $couleur, $video, $ordre, $date_creation, $externe);
		}
	}

		$purls = createUrl($url_projet);
		$purl_paral = $purls['paral'];

		$data = queryDb('SELECT type_slide.nom as type, slide.* FROM type_slide, slide WHERE type_slide.id = slide.type_slide AND num_projet ='.$id_url_projet.' ORDER BY numero DESC LIMIT 1');

				foreach ($data as $row) 
				{
					$pnumero = $row["numero"];
					$ptype_slide = $row["type"];
					$plien_image = $row["lien_image"];
					$pcouleur = $row["couleur"];
					$pfont = $row["couleur_texte"];
					$ptitre = $row["titre"];
					$ptexte = $row["texte"];
					$pgras = $row["gras"];
					$plien_video =$row["lien_video"];
					$pordre = $row["ordre"];

					if ($ptype_slide == "Présentation") 
					{
						// Affichage si image
						if(!empty($plien_image))
						{
							if(!empty($ptitre) && $pgras == 1)
							{
								if(!empty($ptexte))
								{
									// Image + titre gras + texte
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $pnumero.'">
										<div class="preview-slide">
								    		<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$plien_image.'" class="img-thumbnail"></a>
								    		<div class="contentp">
								    			<h3 style="color:'.$pfont.'"><strong>'.$ptitre.'</strong></h3>
								    			<h3 style="color:'.$pfont.'">'.$ptexte.'</h3>
								    		</div>
								    		<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    		<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
								    	</div>';
									}

								else
								{	// Image + titre gras
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $pnumero.'">
									<div class="preview-slide">
							    			<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$plien_image.'" class="img-thumbnail"></a>
							    			<div class="contentp">
							    				<h3 style="color:'.$pfont.'"><strong>'.$ptitre.'</strong></h3>
							    			</div>
							    			<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
							    			<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
							    		</div>';
								}						
							}

							elseif (!empty($ptitre) && $pgras == 0) 
							{
								if(!empty($ptexte))
								{
									// Image + titre + texte
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $pnumero.'">
									<div class="preview-slide">
								    		<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$plien_image.'" class="img-thumbnail"></a>
								    		<div class="contentp">
								    			<h3 style="color:'.$pfont.'">'.$ptitre.'</h3>
								    			<h3 style="color:'.$pfont.'">'.$ptexte.'</h3>
								    		</div>
								    		<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    		<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
								    	</div>';
								}

								else
								{	
									// Image + titre
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $pnumero.'">
									<div class="preview-slide">
								    	<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$plien_image.'" class="img-thumbnail"></a>
								    	<div class="contentp">
								    		<h3 style="color:'.$pfont.'">'.$ptitre.'</h3>
								    	</div>
								    	<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    	<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    	</div>
								    </div>';
								}
							}

							else
							{
								// Image
								echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $pnumero.'">
									<div class="preview-slide">
								    	<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$plien_image.'" class="img-thumbnail"></a>
								    	<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    	<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    	</div>
								    </div>';
							}	
						}

						// Affichage sans image
						else
						{
							if(!empty($ptitre) && $pgras == 1)
							{
								if(!empty($ptexte))
								{
									// Couleur + Titre gras + texte
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $pnumero.'">
									<div class="no-image" style="background-color:'.$pcouleur.'">
						    				<a href="javascript:void(0);" class="editSlide"></a>
						    				<div class="contentp">
						    					<h3 style="color:'.$pfont.'"><strong>'.$ptitre.'</strong></h3>
								    			<h3 style="color:'.$pfont.'">'.$ptexte.'</h3>
								    		</div>
								    		<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    		<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
						    			</div>';
								}

								else
								{
									// Couleur + Titre gras
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $pnumero.'">
									<div class="no-image" style="background-color:'.$pcouleur.'">
						    				<a href="javascript:void(0);" class="editSlide"></a>
						    				<div class="contentp">
						    					<h3 style="color:'.$pfont.'"><strong>'.$ptitre.'</strong></h3>
						    				</div>
						    				<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
						    				<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
						    			</div>';
								}						
							}

							elseif (!empty($ptitre) && $pgras == 0) 
							{
								if(!empty($ptexte))
								{
									// Couleur + Titre + texte
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $pnumero.'">
									<div class="no-image" style="background-color:'.$pcouleur.'">
						    				<a href="javascript:void(0);" class="editSlide"></a>
						    				<div class="contentp">
						    					<h3 style="color:'.$pfont.'">'.$ptitre.'</h3>
								    			<h3 style="color:'.$pfont.'">'.$ptexte.'</h3>
								    		</div>
								    		<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    		<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
						    			</div>';
									
								}

								else
								{
									// Couleur + Titre
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $pnumero.'">
									<div class="no-image" style="background-color:'.$pcouleur.'">
						    				<a href="javascript:void(0);" class="editSlide"></a>
						    				<div class="contentp">
						    					<h3 style="color:'.$pfont.'">'.$ptitre.'</h3>
						    				</div>
						    				<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
						    				<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
						    			</div>';
								}
							}

							else
							{
								// Couleur 
								echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $pnumero.'">
								<div class="no-image" style="background-color:'.$pcouleur.'">
						    			<a href="javascript:void(0);" class="editSlide"></a>
						    			<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
						    			<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    	</div>
						    		</div>';				
							}	
						}
					}

					// Affichage si vidéo
					else
					{
						echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-video s" id="item_'. $pnumero.'">
						<div class="video editSlide">
				    			<iframe class="preview-video" width="100%" height="100%" src="'.$plien_video.'" frameborder="0" allowfullscreen></iframe>
				    			<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
				    			<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								</button>
								</div>
				    		</div>';
					}
				}
?>
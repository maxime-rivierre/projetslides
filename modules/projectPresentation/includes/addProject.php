<?php 
	include_once 'main.php'; 

	$nom = $_POST['projectName'];
	$nom_complet = $_POST['name'];

	$id_client = $_POST['idClient'];
	$name_client = $_POST['nameClient'];
	$entreprise = $_POST['entreprise'];

	$nom_final = encodeString($nom);
	$encoded_name = urlencode($nom_final);

	$date_creation = date('Y-m-d');

	if(empty($nom) || empty($nom_complet))
	{
		header('Location: ../index.php');
	}

	else
	{
		$client = queryDb('SELECT * FROM client WHERE id ='.$id_client);
		$exists = count($client);

		if($exists == 0)
		{
			addClient($id_client, $name_client, $entreprise);
		}

		addProject($nom_final, $id_client, $encoded_name, $date_creation);

		$datas = queryDb('SELECT numero, url FROM projet ORDER BY numero DESC LIMIT 1');
		$numero = $datas[0]['numero'];
		$code = $datas[0]['url'];
		$url = $numero . "-" . $encoded_name;

		updateUrl($numero, $url);
	}

	$date_format = '%d/%m/%Y';
	$data = queryDb('SELECT client.nom as nom_client, client.entreprise as entreprise, projet.*, DATE_FORMAT(date_creation, "'.$date_format.'") AS date_creation FROM client, projet WHERE client.id = projet.idclient AND hidden = 0 ORDER BY numero DESC LIMIT 1');

	foreach ($data as $row) 
	{
		$url = $row['url'];
		$urls = createUrl($url);
		$url_modif = $urls['modif'];
		$url_paral = $urls['paral']; 

		echo '<tr id="projet-'.$row['numero'].'"">
				<td class="editMe">'.$row["nom"].'</td>
				<td style="cursor: default">'.$row["nom_client"].'</td>
				<td style="cursor: default">'.$row["entreprise"].'</td>
				<td style="cursor: default">'.$row["date_creation"].'</td>
				<td><a href="'.$url_modif.'" class="btn btn-primary btn-md " role="button">Modifier<i class="material-icons">edit</i></a>
				</td>
				<td><a href="'.$url_paral.'" target="_blank" class="btn btn-success btn-md" role="button">Visualiser<i class="material-icons">search</i></a>
				</td>
				<td><a href="#" id='.$row['numero'].' class="button-delete btn btn-danger btn-md" role="button">Supprimer<i class="material-icons">delete_forever</i></a>
				</td>
			</tr>';
	}

?>
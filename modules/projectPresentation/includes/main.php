<?php 
	// Affiche les erreurs PHP
	ini_set('display_errors',1);
	error_reporting(E_ALL);

	function dbConnect()
	{
		/*$server = "mysql:host=localhost;dbname=projet";
		$user = "root";
		$pwd = "";*/

        $server = "mysql:host=localhost;dbname=".$GLOBALS['database'];
        $user = $GLOBALS['user'];
        $pwd = $GLOBALS['password'];

		$options = array(
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
			PDO::ATTR_CASE => PDO::CASE_LOWER,
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
		);

		try 
		{
			$link = new PDO($server, $user, $pwd, $options);
			return $link;
		}
		catch (Exception $e)
		{
			echo "Connection à MySql impossible : ", $e->getMessage();
			die();
		}
	}

	function queryDb($query)
	{
		$dbh = dbConnect();
		try
		{
			$res = $dbh->query($query);
			$data = $res->fetchAll();
			return $data;
		}
		catch(PDOException $exception)
		{
			return $exception;
		}
		
	}

	function getProjectId($urlProject)
	{
		$data_nom = queryDb('SELECT numero FROM projet WHERE url ="'.$urlProject.'"');
		$id_nom_projet = $data_nom[0]['numero'];
		return $id_nom_projet;
	}

	function getNomClient($id)
	{
		$data_nom = queryDb('SELECT nom FROM client WHERE id ='.$id);
		$nom = $data_nom[0]['nom'];
		return $nom;
	}

	function getEntrepriseClient($id)
	{
		$data_entreprise = queryDb('SELECT entreprise FROM client WHERE id ='.$id);
		$entreprise = $data_entreprise[0]['entreprise'];
		return $entreprise;
	}

	function getLastSlideOrder($projectId)
	{
		$data_ordre = queryDb('SELECT ordre FROM slide WHERE num_projet ="'.$projectId.'" ORDER BY ordre DESC LIMIT 1');
		$exists = count($data_ordre);
		$last_ordre = 0;

		if($exists == 1)
		{
			$last_ordre = $data_ordre[0]['ordre'];
		}
		
		return $last_ordre;
	}

	function addClient($id_client, $nom, $entreprise)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare("INSERT INTO client (id, nom, entreprise) VALUES (:id, :nom, :entreprise)");
		$query->bindParam(':id', $id_client);
		$query->bindParam(':nom', $nom);
		$query->bindParam(':entreprise', $entreprise);
		$query->execute();
	}

	function addProject($nom, $id_client, $encoded_name, $date_creation)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare("INSERT INTO projet (nom, idclient, url, date_creation) VALUES (:nom, :id_client, :url, :date_creation)");
		$query->bindParam(':nom', $nom);
		$query->bindParam(':id_client', $id_client);
		$query->bindParam(':url', $encoded_name);
		$query->bindParam(':date_creation', $date_creation);
		$query->execute();
	}

	function createUrl($nom)
	{
		$encode_name = urlencode($nom);
        $url_modif = "http://localhost/projet_slides/index.php?page=projectPresentation&projet=".$encode_name;
        $url_paral = "http://localhost/projet_slides/slides/index.php?parallax=".$encode_name;
		/*$url_modif = "https://app.1984.agency/projectPresentation?projet=".$encode_name;
		$url_paral = "https://app.1984.agency/projet/presentation.php?parallax=".$encode_name;*/
		$tab = array('modif' => $url_modif ,'paral' => $url_paral);
		return $tab;
	}

	function addSlide($id_nom_projet, $type, $lien_image, $couleur, $couleurText, $titre, $texte, $gras, $ordre, $date_creation, $externe)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare("INSERT INTO slide (num_projet, type_slide, lien_image, couleur, couleur_texte, titre, texte, gras, ordre, date_creation, lien_externe) VALUES (:num_projet, :type_slide, :lien_image, :couleur, :couleur_texte, :titre, :texte, :gras, :ordre, :date_creation, :externe)");
		$query->bindParam(':num_projet', $id_nom_projet);
		$query->bindParam(':type_slide', $type);
		$query->bindParam(':lien_image', $lien_image);
		$query->bindParam(':couleur', $couleur);
		$query->bindParam(':couleur_texte', $couleurText);
		$query->bindParam(':titre', $titre);
		$query->bindParam(':texte', $texte);
		$query->bindParam(':gras', $gras);
		$query->bindParam(':ordre', $ordre);
		$query->bindParam(':date_creation', $date_creation);
		$query->bindParam(':externe', $externe);
		$query->execute();
	}

	function addSlideVideo($id_nom_projet, $type, $couleur, $lien_video, $ordre, $date_creation, $externe)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare("INSERT INTO slide (num_projet, type_slide, couleur, lien_video, ordre, date_creation, lien_externe) VALUES (:num_projet, :type_slide, :couleur, :video, :ordre, :date_creation, :externe)");
		$query->bindParam(':num_projet', $id_nom_projet);
		$query->bindParam(':type_slide', $type);
		$query->bindParam(':couleur', $couleur);
		$query->bindParam(':video', $lien_video);
		$query->bindParam(':ordre', $ordre);
		$query->bindParam(':date_creation', $date_creation);
		$query->bindParam(':externe', $externe);
		$query->execute();
	}

	function addIntro($id_nom_projet, $type, $couleur_texte, $lien_image, $titre, $date_creation, $gras)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare("INSERT INTO slide (num_projet, type_slide, couleur_texte, lien_image, titre, date_creation, gras) VALUES (:num_projet, :type_slide, :couleur_texte, :lien_image, :titre, :date_creation, :gras)");
		$query->bindParam(':num_projet', $id_nom_projet);
		$query->bindParam(':type_slide', $type);
		$query->bindParam(':couleur_texte', $couleur_texte);
		$query->bindParam(':lien_image', $lien_image);
		$query->bindParam(':titre', $titre);
		$query->bindParam(':date_creation', $date_creation);
		$query->bindParam(':gras', $gras);
		$query->execute();
	}

	function addEnding($id_nom_projet, $type, $couleur_texte, $texte, $participants, $gras)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare("INSERT INTO slide (num_projet, type_slide, couleur_texte, titre, texte, gras) VALUES (:num_projet, :type_slide, :couleur_texte, :texte, :participants, :gras)");
		$query->bindParam(':num_projet', $id_nom_projet);
		$query->bindParam(':type_slide', $type);
		$query->bindParam(':couleur_texte', $couleur_texte);
		$query->bindParam(':texte', $texte);
		$query->bindParam(':participants', $participants);
		$query->bindParam(':gras', $gras);
		$query->execute();
	}

	function updateUrl($numero, $url)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare('UPDATE projet SET url = :url WHERE numero =:numero');
		$query->bindParam(':url', $url);
		$query->bindParam(':numero', $numero);
		$query->execute();
	}

	function updateName($nom, $numero)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare('UPDATE projet SET nom = :nom WHERE numero =:numero');
		$query->bindParam(':nom', $nom);
		$query->bindParam(':numero', $numero);
		$query->execute();
	}

	function updateSlide($numero, $couleur, $couleur_text, $titre, $texte, $video, $gras, $date_modif, $externe)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare('UPDATE slide SET couleur = :couleur, couleur_texte = :couleur_text, titre = :titre, texte =:texte, lien_video = :lien_video, gras =:gras, date_modif = :date_modif, lien_externe = :lien_externe WHERE numero =:numero');
		$query->bindParam(':couleur', $couleur);
		$query->bindParam(':couleur_text', $couleur_text);
		$query->bindParam(':titre', $titre);
		$query->bindParam(':texte', $texte);
		$query->bindParam(':lien_video', $video);
		$query->bindParam(':gras', $gras);
		$query->bindParam(':date_modif', $date_modif);
		$query->bindParam(':lien_externe', $externe);
		$query->bindParam(':numero', $numero);
		$query->execute();	
	}

	function updateSlideImage($numero, $lien_image, $couleur, $couleur_text, $titre, $texte, $video, $gras, $date_modif, $externe)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare('UPDATE slide SET lien_image = :lien_image, couleur = :couleur, couleur_texte =:couleur_text, titre = :titre, texte =:texte, lien_video = :lien_video, gras =:gras, date_modif = :date_modif, lien_externe = :lien_externe WHERE numero =:numero');
		$query->bindParam(':lien_image',$lien_image);
		$query->bindParam(':couleur', $couleur);
		$query->bindParam(':couleur_text', $couleur_text);
		$query->bindParam(':titre', $titre);
		$query->bindParam(':texte', $texte);
		$query->bindParam(':lien_video', $video);
		$query->bindParam(':gras', $gras);
		$query->bindParam(':date_modif', $date_modif);
		$query->bindParam(':lien_externe', $externe);
		$query->bindParam(':numero', $numero);
		$query->execute();	
	}

	function updateIntroImage($numero, $couleur_texte, $lien_image, $titre, $date_creation, $date_modif, $gras)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare('UPDATE slide SET lien_image = :lien_image, couleur_texte = :couleur_texte, titre = :titre, date_creation = :date_creation, date_modif = :date_modif, gras = :gras WHERE numero =:numero');
		$query->bindParam(':lien_image',$lien_image);
		$query->bindParam(':couleur_texte', $couleur_texte);
		$query->bindParam(':titre', $titre);
		$query->bindParam(':date_creation', $date_creation);
		$query->bindParam(':date_modif', $date_modif);
		$query->bindParam(':numero', $numero);
		$query->bindParam(':gras', $gras);
		$query->execute();	
	}

	function updateIntro($numero, $couleur_texte, $titre, $date_creation, $date_modif, $gras)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare('UPDATE slide SET couleur_texte = :couleur_texte, titre = :titre, date_creation = :date_creation, date_modif = :date_modif, gras = :gras WHERE numero = :numero');
		$query->bindParam(':couleur_texte', $couleur_texte);
		$query->bindParam(':titre', $titre);
		$query->bindParam(':date_creation', $date_creation);
		$query->bindParam(':date_modif', $date_modif);
		$query->bindParam(':numero', $numero);
		$query->bindParam(':gras', $gras);
		$query->execute();	
	}

	function updateEnding($numero, $couleur_texte, $texte, $participants, $date_modif, $gras)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare('UPDATE slide SET couleur_texte =:couleur_texte, titre = :titre, texte =:texte, date_modif = :date_modif, gras = :gras WHERE numero =:numero');
		$query->bindParam(':couleur_texte', $couleur_texte);
		$query->bindParam(':titre', $texte);
		$query->bindParam(':texte', $participants);
		$query->bindParam(':date_modif', $date_modif);
		$query->bindParam(':numero', $numero);
		$query->bindParam(':gras', $gras);
		$query->execute();
	}

	function deleteFile($filename)
	{
		if(file_exists($filename))
		{
			unlink($filename);
		}
	}

	function createToken($longueur)
	{
		$random = random_bytes($longueur);
		$nb = bin2hex($random);
		return $nb;
	}

	function encodeString($string)
	{
		$nb_nom = strlen($string);
		$nb = $nb_nom - 1;
		$letter = mb_substr($string, 0,1,'utf-8');
		$end = mb_substr($string, 1, $nb, 'utf-8');
		$maj = mb_strtoupper($letter,'utf-8');
		$nom_final = $maj.$end;
		return $nom_final;
	}
?>
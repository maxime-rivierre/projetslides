<?php 
	include_once 'main.php';
	ini_set('display_errors',1); 
	error_reporting(E_ALL);

	$date = $_POST['idate'];
	$type_slide = "Introduction";
	$client = $_POST['iclient'];
	$url_projet = $_POST['iprojectUrl'];
	$id_projet = getProjectId($url_projet);

	$colorpickerText = $_POST['icolorTextPicked'];
	$colorHexText = $_POST['icolorHexText'];
	$couleurText = "";

	if($colorpickerText == "#ffffff" && $colorHexText != "")
	{
		$couleurText = $colorHexText;
	}

	else
	{
		$couleurText = $colorpickerText;
	}

	$bold = 0;

	if(isset($_POST['ibold']))
	{
		$bold = 1;
	}

	$types = queryDb('SELECT * FROM type_slide');
	$id_type = 0;

	foreach ($types as $type) 
	{
		if ($type['nom'] == $type_slide) 
		{
			$id_type = $type['id'];
		}
	}

	if(!empty($_POST['imgUploaded'])) 
	{
    	// Récupére l'image
	    $split = explode(',',$_POST['imgUploaded']);
	    $data = $split[1];
	    $data = base64_decode($data);
	    $split = explode(';',$split[0])[0];
	    // Récupére l'extension
	    $extension = explode('/',$split)[1];
	    // Définit le dossier
	    $directory = '../images/';
	    $chemin = 'images/';
	    // Définit le nom
	    $token = createToken(25);
	    $filename = date('d-m-Y').$token.'.'.$extension;
	    $chemin_image = $chemin.$filename;
	    // Upload
	    file_put_contents($directory.$filename, $data);
	    // Mise à jour en BDD

	    addIntro($id_projet, $id_type, $couleurText, $chemin_image, $client, $date, $bold);
	}

	else
	{
		$chemin_image = "";
		addIntro($id_projet, $id_type, $couleurText, $chemin_image, $client, $date, $bold);
	}

	$data_intro = queryDb('SELECT slide.* FROM slide, type_slide WHERE type_slide.id = slide.type_slide AND type_slide.id = '.$id_type.' ORDER BY slide.numero DESC LIMIT 1');

	$purls = createUrl($url_projet);
	$purl_paral = $purls['paral'];

	foreach ($data_intro as $row_intro) 
	{
		$pnum_intro = $row_intro["numero"];
		$plien_image_intro = $row_intro["lien_image"];
		$ptexte_intro = $row_intro["titre"];
		$date = $row_intro["date_creation"];
		$pfont = $row_intro["couleur_texte"];
		$pgras = $row_intro["gras"];

		$val = explode('-', $date);
		$pdate_intro = $val[2]."-".$val[1].'-'.$val[0];

		if(!empty($plien_image_intro))
		{
			echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-intro" id="item_'. $pnum_intro.'">
					<div class="intro" style="background-color:#000">
						<a href="javascript:void(0);"><img src="https://app.1984.agency/modules/projectPresentation/'.$plien_image_intro.'" class="img-thumbnail"></a>
						<div class="block-logo">
							<img class="img-logo" src="https://app.1984.agency/modules/projectPresentation/images/logo-transparent.png">
						</div>
						<div class="contentp">';
						if($pgras == 1)
						{
							echo '<h3 style="color:'.$pfont.'"><strong>'.$ptexte_intro.'</strong></h3>';
						}

						else
						{
							echo '<h3 style="color:'.$pfont.'">'.$ptexte_intro.'</h3>';
						}
						echo '</div>
						<div class="contentp-date">
							<h3 style="color:'.$pfont.'">'.$pdate_intro.'</h3>
						</div>
						<a href="'.$purl_paral.'#'.$pnum_intro.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
						<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
						</button>
					</div>
				</div>';
		}

		else
		{
			echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-intro" id="item_'. $pnum_intro.'">
					<div class="intro" style="background-color:#000">
						<div class="block-logo">
							<img class="img-logo" src="https://app.1984.agency/modules/projectPresentation/images/logo-transparent.png">
						</div>
						<div class="contentp">';
						if($pgras == 1)
						{
							echo '<h3 style="color:'.$pfont.'"><strong>'.$ptexte_intro.'</strong></h3>';
						}

						else
						{
							echo '<h3 style="color:'.$pfont.'">'.$ptexte_intro.'</h3>';
						}

				echo '</div>
						<div class="contentp-date">
							<h3 style="color:'.$pfont.'">'.$pdate_intro.'</h3>
						</div>
						<a href="'.$purl_paral.'#'.$pnum_intro.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
						<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
						</button>
					</div>
				</div>';
		}
	}
?>
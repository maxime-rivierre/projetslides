<?php
	include_once 'main.php';

	$id = $_POST['id'];

	$dbh = dbConnect();

	try
	{
		$query = $dbh->prepare('UPDATE projet SET hidden = 1 WHERE numero =:numero');
		$query->bindParam(':numero', $id);
		$query->execute();
	}

	catch(Exception $e)
	{
		echo $e->getMessage();
	}
	
?>
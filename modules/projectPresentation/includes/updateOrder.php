<?php
	include_once 'main.php';

	$dbh = dbConnect();
	$count = 1;
	foreach ($_POST['item'] as $value) 
	{
		$query = $dbh->prepare('UPDATE slide SET ordre = :count WHERE numero =:numero');
		$query->bindParam(':count', $count);
		$query->bindParam(':numero', $value);
		$query->execute();
		$count++;
	}
?>
<?php
	include_once 'main.php';
	ini_set('display_errors',1); 
	error_reporting(E_ALL); 

	$url_projet = $_POST['uieprojectUrl'];	
	$numero = $_POST['uienumero'];
	$type_slide = $_POST['uieType'];
	$titre = $_POST['uietitre'];
	$texte = $_POST['uietexte'];
	$date = $_POST['uiedate'];
	$couleur_texte = "";
	$bold = 0;

	if(isset($_POST['uiebold']))
	{
		$bold = 1;
	}

	$colorpickerText = $_POST['uiecolorPickedText'];
	$colorHexText = $_POST['uiecolorHexText'];

	if($colorpickerText == "#ffffff" && $colorHexText != "")
	{
		$couleur_texte = $colorHexText;
	}

	else
	{
		$couleur_texte = $colorpickerText;
	}

	$types = queryDb('SELECT * FROM type_slide');
	$id_type = 0;

	foreach ($types as $type) 
	{
		if ($type['nom'] == $type_slide) 
		{
			$id_type = $type['id'];
		}
	}

	date_default_timezone_set("Europe/Paris");
	$date_modif = gmdate('Y-m-d h:i:s');

	if(!empty($_POST['imgUploaded'])) {
    	// Récupére l'image
	    $split = explode(',',$_POST['imgUploaded']);
	    $data = $split[1];
	    $data = base64_decode($data);
	    $split = explode(';',$split[0])[0];
	    // Récupére l'extension
	    $extension = explode('/',$split)[1];
	    // Définit le dossier
	    $directory = '../images/';
	    $chemin = 'images/';
	    // Définit le nom
	    $token = createToken(25);
	    $filename = date('d-m-Y').$token.'.'.$extension;
	    $chemin_image = $chemin.$filename;
	    // Upload
	    file_put_contents($directory.$filename, $data);
	    // Mise à jour en BDD

	    $temp = $_POST['uieoldimg'];

	    if(!empty($temp))
	    {
	    	$image = "../".$temp;
			$del = deleteFile($image);
	    }

	    if($type_slide == "Introduction")
	    {
	    	updateIntroImage($numero, $couleur_texte, $chemin_image, $titre, $date, $date_modif, $bold);
	    }

	    else
	    {
	    	updateEnding($numero, $couleur_texte, $titre, $texte, $date_modif, $bold);
	    }
		
	}  

	else
	{
		if($_POST['uietodelete'] == "true" && !empty($_POST['uieoldimg']))
		{
			$temp = $_POST['uieoldimg'];

		    if(!empty($temp))
		    {
		    	$image = "../".$temp;
				$del = deleteFile($image);
		    }

		    $chemin_image = "";	

		    if($type_slide == "Introduction")
		    {
		    	updateIntroImage($numero, $couleur_texte, $chemin_image, $titre, $date, $date_modif, $bold);
		    }

		    else
		    {
		    	updateEnding($numero, $couleur_texte, $titre, $texte, $date_modif, $bold);
		    }
		}

		else
		{
			if($type_slide == "Introduction")
		    {
		    	updateIntro($numero, $couleur_texte, $titre, $date, $date_modif, $bold);
		    }

		    else
		    {
		    	updateEnding($numero, $couleur_texte, $titre, $texte, $date_modif, $bold);
		    }
		}	
	}

	$purls = createUrl($url_projet);
	$purl_paral = $purls['paral'];

	$data = queryDb('SELECT type_slide.nom as type, slide.* FROM type_slide, slide 				WHERE type_slide.id = slide.type_slide AND numero ='.$numero);

	foreach ($data as $row) 
	{
		$pnumero = $row["numero"];
		$ptype_slide = $row["type"];
		$plien_image = $row["lien_image"];
		$pfont = $row["couleur_texte"];
		$ptitre = $row["titre"];
		$pparticipants = $row["texte"];
		$date = $row["date_creation"];
		$pgras = $row["gras"];

		$val = explode('-', $date);
		$pdate = $val[2]."-".$val[1].'-'.$val[0];

		$list = explode(",", $pparticipants);

		if($ptype_slide == "Introduction")
		{
			if(!empty($plien_image))
			{
				if($pgras == 1)
				{
					echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-intro" id="item_'. $numero.'">
							<div class="intro" style="background-color:#000">
								<a href="javascript:void(0);"><img src="https://app.1984.agency/modules/projectPresentation/'.$plien_image.'" class="img-thumbnail"></a>
								<div class="block-logo">
									<img class="img-logo" src="https://app.1984.agency/modules/projectPresentation/images/logo-transparent.png">
								</div>
								<div class="contentp">
									<h3 style="color:'.$pfont.'"><strong>'.$ptitre.'</strong></h3>
								 </div>
								<div class="contentp-date">
									<h3 style="color:'.$pfont.'">'.$pdate.'</h3>
								</div>
								<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								</button>
							</div>
						</div>';	
				}

				else
				{
					echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-intro" id="item_'. $numero.'">
							<div class="intro" style="background-color:#000">
								<a href="javascript:void(0);"><img src="https://app.1984.agency/modules/projectPresentation/'.$plien_image.'" class="img-thumbnail"></a>
								<div class="block-logo">
									<img class="img-logo" src="https://app.1984.agency/modules/projectPresentation/images/logo-transparent.png">
								</div>
								<div class="contentp">
									<h3 style="color:'.$pfont.'">'.$ptitre.'</h3>
								 </div>
								<div class="contentp-date">
									<h3 style="color:'.$pfont.'">'.$pdate.'</h3>
								</div>
								<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								</button>
							</div>
						</div>';
				}
			}

			else
			{
				if($pgras == 1)
				{
					echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-intro" id="item_'. $numero.'">
							<div class="intro" style="background-color:#000">
								<div class="block-logo">
									<img class="img-logo" src="https://app.1984.agency/modules/projectPresentation/images/logo-transparent.png">
								</div>
								<div class="contentp">
									<h3 style="color:'.$pfont.'"><strong>'.$ptitre.'</strong></h3>
								 </div>
								<div class="contentp-date">
									<h3 style="color:'.$pfont.'">'.$pdate.'</h3>
								</div>
								<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								</button>
							</div>
						</div>';
				}

				else
				{
					echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-intro" id="item_'. $numero.'">
							<div class="intro" style="background-color:#000">
								<div class="block-logo">
									<img class="img-logo" src="https://app.1984.agency/modules/projectPresentation/images/logo-transparent.png">
								</div>
								<div class="contentp">
									<h3 style="color:'.$pfont.'">'.$ptitre.'</h3>
								 </div>
								<div class="contentp-date">
									<h3 style="color:'.$pfont.'">'.$pdate.'</h3>
								</div>
								<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								</button>
							</div>
						</div>';
				}
			}
		}

		else
		{
			if($pgras == 1)
			{
				echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-ending" id="item_'. $pnumero.'">
					<div class="ending" style="background-color: #000">
						<div class="contentp">
							<h3 style="color:'.$pfont.'"><strong>'.$ptitre.'</strong></h3>
						</div>
						<div class="contentp-ending">';
							foreach ($list as $participants) 
							{
								echo '<h4 style="color:'.$pfont.'">'.$participants.'</h4>';
							}
				echo '</div>
						<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
						<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								</button>
					</div>
				</div>';
			}

			else
			{
				echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-ending" id="item_'. $pnumero.'">
					<div class="ending" style="background-color: #000">
						<div class="contentp">
							<h3 style="color:'.$pfont.'">'.$ptitre.'</h3>
						</div>
						<div class="contentp-ending">';
							foreach ($list as $participants) 
							{
								echo '<h4 style="color:'.$pfont.'">'.$participants.'</h4>';
							}
				echo '</div>
						<a href="'.$purl_paral.'#'.$pnumero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
						<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								</button>
					</div>
				</div>';
			}	
		}
	}

?>
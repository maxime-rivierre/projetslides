<?php 
	include_once 'main.php';
	ini_set('display_errors',1); 
	error_reporting(E_ALL);

	$type_slide = "Remerciements";
	$texte = $_POST['etexte'];
	$participants = $_POST['eparticipants'];
	$url_projet = $_POST['eprojectUrl'];
	$id_projet = getProjectId($url_projet);

	$colorpickerText = $_POST['ecolorTextPicked'];

	$colorpickerText = $_POST['ecolorTextPicked'];
	$colorHexText = $_POST['ecolorHexText'];
	$couleurText = "";

	if($colorpickerText == "#ffffff" && $colorHexText != "")
	{
		$couleurText = $colorHexText;
	}

	else
	{
		$couleurText = $colorpickerText;
	}
	
	$bold = 0;

	if(isset($_POST['ebold']))
	{
		$bold = 1;
	}

	$types = queryDb('SELECT * FROM type_slide');
	$id_type = 0;

	foreach ($types as $type) 
	{
		if ($type['nom'] == $type_slide) 
		{
			$id_type = $type['id'];
		}
	}

	addEnding($id_projet, $id_type, $couleurText, $texte, $participants, $bold);

	$data_ending = queryDb('SELECT slide.* FROM slide, type_slide WHERE type_slide.id = slide.type_slide AND type_slide.id = '.$id_type.' ORDER BY slide.numero DESC LIMIT 1');

	$purls = createUrl($url_projet);
	$purl_paral = $purls['paral'];

	foreach ($data_ending as $row_ending) 
	{
		$pnum_ending = $row_ending["numero"];
		$ptexte_ending = $row_ending["titre"];
		$pparticipants = $row_ending["texte"];
		$pfont = $row_ending["couleur_texte"];
		$pgras = $row_ending["gras"];

		$list = explode(",", $pparticipants);

		echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-ending" id="item_'. $pnum_ending.'">
				<div class="ending" style="background-color: #000">
					<div class="contentp">';
					if($pgras == 1)
					{
						echo '<h3 style="color:'.$pfont.'"><strong>'.$ptexte_ending.'</strong></h3>';
					}
					else
					{
						echo '<h3 style="color:'.$pfont.'">'.$ptexte_ending.'</h3>';
					}
					
			echo '</div>
					<div class="contentp-ending">';
					foreach ($list as $participants) 
					{
						echo '<h4 style="color:'.$pfont.'">'.$participants.'</h4>';
					}
			echo  '</div>
					<a href="'.$purl_paral.'#'.$pnum_ending.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
					<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								</button>
				</div>
			</div>';
	}
?>
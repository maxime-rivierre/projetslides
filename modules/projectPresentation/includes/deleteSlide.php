<?php
	include_once 'main.php';

	$id = $_POST['id'];

	$dbh = dbConnect();

	try
	{
		$data = queryDb('SELECT lien_image FROM slide WHERE numero ='.$id);
		$lien_image = $data[0]['lien_image'];
		$image = "../";
		$image .= $lien_image;

		if(!empty($lien_image) || $lien_image != "" || $lien_image == null)
		{
			$del = deleteFile($image);
		}	

		$query = $dbh->prepare('DELETE FROM slide WHERE numero =:numero');
		$query->bindParam(':numero', $id);
		$query->execute();
	}

	catch(Exception $e)
	{
		echo $e->getMessage();
	}
	
?>
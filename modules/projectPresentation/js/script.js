/*
*
*	Script commun
*
*/

$(document).ready(function() {
	function getParam(param) {
		let vars = {};
		window.location.href.replace( location.hash, '' ).replace( 
			/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
			function( m, key, value ) { // callback
				vars[key] = value !== undefined ? value : '';
			}
		);

		if ( param ) {
			return vars[param] ? vars[param] : null;	
		}
		return vars;
	}

	let param = getParam(), projet = param['projet'];

	if(projet == null)
	{
		$('.search-row').show();
	}
	else 
	{
		$('.search-row').hide();
	}
});

/*
*
* Script pour tableau.php
*
*/

$(document).ready(function() {
	$('#name').keyup(function () {
        let value = $(this).val();
        let block = $(this).parent();
        let list = block.find('.listing-client');
        if (value.length >= 3) {
            $.ajax({
                url: "modules/projectPresentation/includes/getClients.php",
                data: { term: value },
                cache: false,
                error: function (request) {
                    alert("Erreur : responseText: "+request.responseText);
                },
                success: function (data) {
                	$('.listing-client').show();
                    list.html(data);
                }
            });
        } else {
            list.html('');
        }
    });

    $('#colorHex').keyup(function(event) {
    	let value = $(this).val();
    	if(!value)
    		value = "#ffffff";
    	$('.couleur .col-sm-4 .sp-replacer .sp-preview .sp-preview-inner').css('background-color', value);
    });

    $('#colorHexText, #icolorHexText, #ecolorHexText').keyup(function(event) {
    	let value = $(this).val();
    	if(!value)
    		value = "#ffffff";
    	$('.font .col-sm-4 .sp-replacer .sp-preview .sp-preview-inner').css('background-color', value);
    });

    $('#search-user').keyup(function() {
    	let value = $(this).val().toLowerCase();

    	$("table#projets tbody tr").each(function() {
			let $row = $(this);
    		let id = $row.text();
    		id = id.toLowerCase();

    		if(id.indexOf(value) <= 0)
    		{
    			$(this).hide();
    		}

    		else 
    		{
    			$(this).show();
    		}

    		if(value === '')
    		{
    			$(this).show();
    		}
    	});
    });

	$('.listing-client').on('click', '.select-client', function(event) {
		event.preventDefault();
		let id = $(this).attr('id-target');
		let nom = $(this).text();
		let infos = nom.split('-');
		$('#name').val(nom);
		$('#idClient').val(id);
		$('#nameClient').val($.trim(infos[0]));
		$('#entreprise').val($.trim(infos[1]));
		$('.listing-client').hide();	
	});
	
  	$('#addProjet').submit(function(event) {
	    event.preventDefault();
    	let donnees = $(this).serialize();

    	if ($.trim($("#projectName").val()) === "" && $.trim($("#name").val()) === "") 
		{
		    alert('Vous devez remplir au moins un des champs.');
		    return false;
		}

		if($('#idClient').val() === "")
		{
			alert('Nom de client invalide.');
		    return false;
		}

		$.ajax({
			url: 'modules/projectPresentation/includes/addProject.php',
			type: 'POST',
			data: donnees,
			success: function(data){
				$('#projets').prepend(data);
				$('#addProjetForm').modal('hide');
				$('#addProjet').find('input').val('');
			}
		});
		event.stopImmediatePropagation();
	});

	$(function() {
 		$("#projets").tablesorter();
	});

	$('#projets').on('click', '.button-delete', function(event)
	{
		event.preventDefault();
		let c = confirm("Voulez vous vraiment supprimer ce projet ? Cette action est irréversible.");
		if(c)
		{
			let id = this.id;
			let nb = id.replace('projet-','');
			$.ajax({
				url: 'modules/projectPresentation/includes/deleteProject.php',
				type: 'POST',
				data: {id: nb},
				success: function(data){
					$('#projet-'+nb).remove();
					//console.log(data);
				}
			});
		}
		else
		{
			return false;
		}
	});

	$('#projets').on('click', 'tbody tr', function(event) {
		event.stopPropagation();
		$(this).addClass('highlight').siblings().removeClass('highlight');
	});

	let getHighlightRow = function() {
		return $('table > tbody > tr.highlight');
	};

	let editor = new SimpleTableCellEditor("projets");
	editor.SetEditableClass("editMe");

	$('#projets').on("cell:edited", function (event) {
		event.stopPropagation();
		let nom = `${event.newValue}`;
		let id = 0;
		let row = getHighlightRow();
		if (row !== undefined) {
			let id_projet = row.attr('id');
			id = id_projet.replace('projet-','');
		}
		$.ajax({
			url : 'modules/projectPresentation/includes/updateProjectName.php',
			type : 'POST',
			data : {
				'nom' : nom,
				'numero' : id
			},
			success : function(data){

			},
			error : function(data){
				alert(data + "Echec de la modification.");
			}
		});
	});
});


/*
*
*	Script pour projet.php
*
*/

$(document).ready(function() {
	$("input[data-toggle='tooltip']").on('focus', function() {
	    $(this).tooltip('show');
	});

	$('.upload-input').croppie({
		url: 'demo/demo-1.jpg',
	});

	function hideAll()
	{
		let elements = document.getElementsByClassName("g");
		for(let i=0; i<elements.length; i++) 
		{
			elements[i].style.display = "none";
		}
	}
	function getModal(selectedText)
	{
		let elements = document.getElementsByClassName("g");
		let elements_p = document.getElementsByClassName("g-p");
		let elements_v = document.getElementsByClassName("g-v");
		let elements_a = document.getElementsByClassName("g-a");

		for(let i=0; i<elements.length; i++) 
		{
			elements[i].style.display = "none";
		}

		if(selectedText == "Présentation")
		{
		    for(let j=0; j<elements_p.length; j++)
		    {
		    	elements_p[j].style.display = "block";
			}
		    for(let jj=0; jj<elements_v.length; jj++)
		    {
		    	elements_v[jj].style.display = "none";
		    }
		    for (let a = 0; a < elements_a.length; a++) 
		    {
				elements_a[a].style.display = "block";
			}
		}

		else
		{
		    for(let k=0; k<elements_v.length;k++)
		    {
		    	elements_v[k].style.display = "block";
		    }
		    for(let kk=0; kk<elements_p.length;kk++)
		    {
		    	elements_p[kk].style.display = "none";
		    }
		    for (let a = 0; a < elements_a.length; a++) 
		    {
				elements_a[a].style.display = "block";
			}
		}
	}


	$("#dropdownMenu a").click(function(e){
		e.preventDefault();
		let selectedText = $(this).text();
		$("#buttonType").text(selectedText);
		$("#selectedType").val(selectedText);

		getModal(selectedText);
	});

	$('form').on('click', '.upload-result', function(event) {
		event.preventDefault();
		$('.upload-result').html('<i class="material-icons">done</i>');
		$('.upload-result').css('line-height', '0px');
		$('.upload-result').attr('disabled');
		$('.upload-result').css('background-color', '#00e600');
		$('.cr-slider').css('display', 'none');
		$('.g-c').css('display', 'none');
		$('.uc').css('display', 'none');
		$('.uct').css('display', 'block');
	});

	$('form').on('click', '.upload-cancel', function() {
		$('.upload-result').removeAttr('disabled');
		$('.upload-result').html('Valider');
		$('.upload-result').css('line-height', '40px');
		$('.upload-result').css('background-color', '#00ac00');
		$('.imgInput').removeAttr('value');
		$('.cr-slider').css('display', 'block');
		$('.g-c').css('display', 'block');
		$('.uc').css('display', 'block');
		$('.uct').css('display', 'block');
	});

	// Submit ajout slide
	$('#slideFormulaire').submit(function(event) 
		{
	    	event.preventDefault();
	    	let titre = $('#titre').val();
	    	let texte = $('#texte').val();
			let bold = $('#bold').prop('checked');
			let c;
			    			
			while(titre == "" && bold == true)
			{
				c = confirm("Veuillez saisir un titre ou déchocher la case.");
				return false;
			}

	    	let form = $('#slideFormulaire')[0];
	    	let fd = new FormData(form);

			$('.g').css('display', 'none');
			$('#type').addClass('hidden');
	    	$('.loading').css('display', 'block');

			$.ajax({
			  	url: 'modules/projectPresentation/includes/addSlide.php',
			  	data: fd,
			  	processData: false,
			 	contentType: false,
			  	type: 'POST',
			  	success: function(data){
			  		$('#addSlideForm').modal('hide');
			  		$('.upload-cancel').click();
			  		$('.upload-result').html('');
			  		$('.upload-result').removeAttr('disabled');
			  		$('.upload-result').html('Valider');
					$('.upload-result').css('line-height', '40px');
					$('.upload-result').css('background-color', '#00ac00');
					$('#slideFormulaire input[type="text"]').val('');
					$('#slideFormulaire input[type="color"]').val('');
					$('#slideFormulaire input[type="checkbox"]').prop('checked', false);
					$('.imgInput').removeAttr('value');
					$('#type').removeClass('hidden');
					$('.loading').css('display', 'none');
					$('.cr-slider').css('display', 'block');

					$('#colorPicked').val('#ffffff');
					$('#colorTextPicked').val('#ffffff');
					$('#ucolorPicked').val('');
					$('#ucolorPickedText').val('');

					if($('.box-preview-intro').length)
			  		{
			  			if($('.s').length > 0)
			  			{
			  				$(data).insertAfter('.s:last');
			  			}

			  			else 
			  			{
			  				$(data).insertAfter('.box-preview-intro:last');
			  			}
			  		}

			  		else 
			  		{
			  			if($('.s').length)
			  			{
			  				$(data).insertAfter('.s:last');
			  			}

			  			else 
			  			{
			  				$(data).insertAfter('#divAddSlide');
			  			}		  			
			  		}
			  	}
			});
		});

	// Submit update slide
	$('#updateSlideFormulaire').submit(function(event) 
	{
		event.preventDefault();
		let titre = $('#utitre').val();
		let bold = $('#ubold').prop('checked');
		let c;
		    			
		while(titre == "" && bold == true)
		{
			c = confirm("Veuillez saisir un titre ou déchocher la case.");
			return false;
		}

		let old = $('#uoldimg').val();
		let form = $('#updateSlideFormulaire')[0];
	    let fd = new FormData(form);  

	    let num = $("#unumero").val();

	    $('.upd').css('display', 'none');
	    $('.loading').css('display', 'block');

		$.ajax({
			url: 'modules/projectPresentation/includes/updateSlide.php',
			data: fd,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function(data){
				$('#item_'+num).replaceWith(data);
			  	$('#updateSlideForm').modal('hide');
			  	$('.upload-cancel').click();
			  	$('.upload-result').html('');
			  	$('.upload-result').removeAttr('disabled');
			  	$('.upload-result').html('Valider');
				$('.upload-result').css('line-height', '40px');
				$('.upd').css('display', 'block');
				$('.loading').css('display', 'none');
				$('.cr-slider').css('display', 'block');
				$('.uc').css('display', 'block');

				$('#ucolorPicked').val('');
				$('#ucolorPickedText').val('');
			}
		});
	});

	$('#addIntroForm').submit(function(event) {
		event.preventDefault();
		let value = $('.datepicker').val();
		let parts = value.split("/");
		let new_date = parts[2] + "-" + parts[1] + "-" + parts[0];
		$('#idate').val(new_date);

		let titre = $('#iclient').val();
		let bold = $('#ibold').prop('checked');
		let c;
			    			
		while(titre == "" && bold == true)
		{
			c = confirm("Veuillez saisir un titre ou déchocher la case.");
			return false;
		}

		$('.i').css('display', 'none');
	    $('.loading').css('display', 'block');

		let form = $('#introForm')[0];
	    let fd = new FormData(form);

	    $.ajax({
	    	url: '/modules/projectPresentation/includes/addIntro.php',
	    	data: fd,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function(data){
				$('#addIntroForm').modal('hide');
			  	$('.upload-cancel').click();
			  	$('.upload-result').html('');
			  	$('.upload-result').removeAttr('disabled');
			  	$('.upload-result').html('Valider');
				$('.upload-result').css('line-height', '40px');
				$('.i').css('display', 'block');
				$('.loading').css('display', 'none');
				$('.cr-slider').css('display', 'block');

				$('.datepicker').val('');
				$('#iclient').val('');
				$('#idate').val('');

				if($('.box-preview-intro').length)
				{
					$(data).insertAfter('.box-preview-intro:last');
				}

				else 
				{
					$(data).insertAfter('#divAddSlide');
				}

				$('#icolorTextPicked').val('#ffffff');
				$('#icolorHexText').val('');
				$('#uiecolorPickedText').val('');
				$('#ibold').prop('checked', false);
			}
	    })
	});

	$('#addEndingForm').submit(function(event) {
		event.preventDefault();

		let titre = $('#etexte').val();
		let bold = $('#ebold').prop('checked');
		let c;
			    			
		while(titre == "" && bold == true)
		{
			c = confirm("Veuillez saisir un titre ou déchocher la case.");
			return false;
		}

		$('.e').css('display', 'none');
	    $('.loading').css('display', 'block');

		let form = $('#endingForm')[0];
	    let fd = new FormData(form);

	    $.ajax({
	    	url: '/modules/projectPresentation/includes/addEnding.php',
	    	data: fd,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function(data){
				$('#addEndingForm').modal('hide');
			  	$('.upload-cancel').click();
			  	$('.upload-result').html('');
			  	$('.upload-result').removeAttr('disabled');
			  	$('.upload-result').html('Valider');
				$('.upload-result').css('line-height', '40px');
				$('.e').css('display', 'block');
				$('.loading').css('display', 'none');

				$('#etexte').val('');
				$('#eparticipants').val('');

				if($('.box-preview-ending').length)
				{
					$(data).insertAfter('.box-preview-ending:last');
				}

				else
				{
					if($('.s').length)
					{
						$(data).insertAfter('.s:last');
					}

					else 
					{
						$('.sort').append(data);
					}
					
				}

				$('#ecolorTextPicked').val('#ffffff');
				$('#ecolorHexText').val('');
				$('#uiecolorPickedText').val('');
				$('#ebold').prop('checked', false);
			}
	    })    
	});

	$('#updateIEForm').submit(function(event) {
		event.preventDefault();

		let value = $("#IEForm .datepicker").val();
		let parts = value.split("/");
		let new_date = parts[2] + "-" + parts[1] + "-" + parts[0];
		$('#uiedate').val(new_date);

		let titre = $('#uietitre').val();
		let bold = $('#uiebold').prop('checked');
		let c;
			    			
		while(titre == "" && bold == true)
		{
			c = confirm("Veuillez saisir un titre ou déchocher la case.");
			return false;
		}

	    let old = $('#uieoldimg').val();
		let form = $('#IEForm')[0];
	    let fd = new FormData(form);

	    let num = $("#uienumero").val();

	    $('.uie').css('display', 'none');
	    $('.loading').css('display', 'block');

	    $.ajax({
	    	url: '/modules/projectPresentation/includes/updateIntroEnding.php',
	    	data: fd,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function(data){
				$('#item_'+num).replaceWith(data);
			  	$('#updateIEForm').modal('hide');
			  	$('.upload-cancel').click();
			  	$('.upload-result').html('');
			  	$('.upload-result').removeAttr('disabled');
			  	$('.upload-result').html('Valider');
				$('.upload-result').css('line-height', '40px');
				$('.uie').css('display', 'block');
				$('.loading').css('display', 'none');
				$('.cr-slider').css('display', 'block');
				$('.uc').css('display', 'block');

				$('#uiebold').prop('checked', false);
				$('#uiedate').val('');
			}	
	    })
	});

	function getColor()
	{
		let id_projet = $('.sort').attr('id');
		let f_palette = new Array();
		let f_paletteColor = new Array();

		$.ajax({
			url: 'modules/projectPresentation/includes/getColor.php',
			type: 'POST',
			data:{id: id_projet},
			dataType: 'json',
			success: function(data){
				for(let i = 0; i <data.length; i++)
				{
					let color = data[i].couleur;
					f_paletteColor.push(color);
					if(i % 3 == 0)
					{
						f_palette.push(f_paletteColor);
						f_paletteColor = new Array();
					}						
				}

				f_palette.push(f_paletteColor);
			}
		});

		return f_palette;
	}

	function getColorText()
	{
		let id_projet = $('.sort').attr('id');
		let f_palette = new Array();
		let f_paletteColor = new Array();

		$.ajax({
			url: 'modules/projectPresentation/includes/getColorText.php',
			type: 'POST',
			data:{id: id_projet},
			dataType: 'json',
			success: function(data){
				for(let i = 0; i <data.length; i++)
				{
					let color = data[i].couleur_texte;
					f_paletteColor.push(color);
					if(i % 3 == 0)
					{
						f_palette.push(f_paletteColor);
						f_paletteColor = new Array();
					}						
				}

				f_palette.push(f_paletteColor);
			}
		});

		return f_palette;
	}

	let g_palette = getColor();
	let g_palette_text = getColorText();

	$('#addSlideForm').on('shown.bs.modal', function(event) {
		event.preventDefault();
		$('#color').spectrum("destroy");
		$("#color").spectrum({
			color: "#ffffff",
			palette: g_palette,
			showPalette : true,
			hideAfterPaletteSelect:true,
			preferredFormat: "hex",
			change: function(color) {
				$("#colorPicked").val(color.toHexString());
				$("#colorHex").val(color.toHexString());
			}
		});

		$('#colorText').spectrum("destroy");
		$("#colorText").spectrum({
			color: "#ffffff",
			palette: g_palette_text,
			showPalette : true,
			hideAfterPaletteSelect:true,
			preferredFormat: "hex",
			change: function(color) {
				$("#colorTextPicked").val(color.toHexString());
				$("#colorHexText").val(color.toHexString());
			}
		});
	});

	$('.box').on('shown.bs.modal', function(event) {
		event.preventDefault();
		g_palette = getColor();
		g_palette_text = getColorText();
		console.log(g_palette);
		console.log(g_palette_text);
	});

	$('#addIntroForm').on('shown.bs.modal', function(event) {
		event.preventDefault();
		$('#icolorText').spectrum("destroy");
		$("#icolorText").spectrum({
			color: "#ffffff",
			palette: g_palette_text,
			showPalette : true,
			hideAfterPaletteSelect:true,
			preferredFormat: "hex",
			change: function(color) {
				$("#icolorTextPicked").val(color.toHexString());
				$("#icolorHexText").val(color.toHexString());
			}
		});
	});

	$('#addEndingForm').on('shown.bs.modal', function(event) {
		event.preventDefault();
		$('#ecolorText').spectrum("destroy");
		$("#ecolorText").spectrum({
			color: "#ffffff",
			palette: g_palette_text,
			showPalette: true,
			hideAfterPaletteSelect:true,
			preferredFormat: "hex",
			change: function(color) {
				$("#ecolorTextPicked").val(color.toHexString());
				$("#ecolorHexText").val(color.toHexString());
			}
		});
	});

	let typeSlide = "";

	function getSlide(id){
		$.ajax({
			url: 'modules/projectPresentation/includes/getSlide.php',
			type: 'POST',
			data: {id: id},
			success: function(data){
				let json = JSON.parse(data);
				console.log(json);
				let numero = json[0].numero;
				let type_nom = json[0].type_nom;
				let lien_image = json[0].lien_image;
				let couleur = json[0].couleur;
				let couleur_texte = json[0].couleur_texte;
				let externe = json[0].lien_externe;

				if(!couleur)
				{
					couleur = "#ffffff";
				}

				if(!couleur_texte)
				{
					couleur_texte = "#ffffff";
				}

				let titre = json[0].titre;
				let texte = json[0].texte;
				let gras = json[0].gras;
				let lien_video = json[0].lien_video;

				if(type_nom == "Présentation")
				{
					$('#luimg').show();
				  	$('#uimg').show();
				  	$('#lnoCurrentImage').show();
				  	$('#lcurrentImage').show();
				  	$('#currentImage').show();
				  	$('#divColor').removeClass('hidden');
				 	$('#lucolor').show();
				  	$('#ucolor').show();
				  	$('#lucolorText').show();
				  	$('#ucolorText').show();
				  	$('#lutitre').show();
				  	$('#utitre').show();
				  	$('#lutexte').show();
				  	$('#utexte').show();
				  	$('#ubold').show();
				  	$('#lubold').show();

					$('#luvideo').hide();
					$('#uvideo').hide();
					$('#luvideoCurrent').hide();
					$('#uvideoCurrent').hide();

					$('#ucolor').spectrum("destroy");
					$("#ucolor").spectrum({
						color: couleur,
						palette: g_palette,
						showPalette : true,
						hideAfterPaletteSelect: true,
						preferredFormat: "hex",
						change: function(color){
							let couleurChoisie = $('#ucolor').spectrum("get");
							let col = couleurChoisie.toHexString();
							$('#ucolorPicked').val(col);
							$('#ucolorHex').val(col);
						}
					});
					$('#ucolorHex').val(couleur);

					$('#ucolorHex').keyup(function(event) {
				    	let value = $(this).val();
				    	if(!value)
				    	{
				    		value = "#ffffff";
				    	}
				    	if(value.length == 7)
				    	{
				    		$("#ucolor").spectrum("set", value);
				    		$('#ucolorPicked').val('#ffffff');
				    	}
				    });

					$('#ucolorText').spectrum("destroy");
					$("#ucolorText").spectrum({
						color: couleur_texte,
						palette: g_palette_text,
						showPalette : true,
						hideAfterPaletteSelect: true,
						preferredFormat: "hex",
						change: function(color){
							let couleurChoisie = $('#ucolorText').spectrum("get");
							let col = couleurChoisie.toHexString();
							$('#ucolorPickedText').val(col);
							$('#ucolorHexText').val(col);
						}
					});
					$('#ucolorHexText').val(couleur_texte);

					$('#ucolorHexText').keyup(function(event) {
				    	let value = $(this).val();
				    	if(!value)
				    	{
				    		value = "#ffffff";
				    	}
				    	if(value.length == 7)
				    	{
				    		$("#ucolorText").spectrum("set", value);
				    		$('#ucolorPickedText').val('#ffffff');
				    	}
				    });

				}

				else
				{
					$('#luimg').hide();
				  	$('#uimg').hide();
				  	$('#lcurrentImage').hide();
				  	$('#currentImage').hide();
				  	$('#lnoCurrentImage').hide();
				  	$('#divColor').addClass('hidden');
				  	$('#lucolor').hide();
				  	$('#ucolor').hide();
				  	$('#lucolorText').hide();
				  	$('#ucolorText').hide();
				  	$('#lutitre').hide();
				  	$('#utitre').hide();
				  	$('#lutexte').hide();
				  	$('#utexte').hide();
				  	$('#ubold').hide();
				  	$('#lubold').hide();

				  	$('#luvideo').show();
					$('#uvideo').show();
					$('#luvideoCurrent').show();
					$('#uvideoCurrent').show();
				}

				if(lien_image != "")
				{
					$('#currentImgContent').removeClass('hidden');
				  	$('#lnoCurrentImage').addClass('hidden');
				  	$('#currentImage').removeClass('hidden');
				  	$("#currentImage").attr("src", "https://app.1984.agency/modules/projectPresentation/"+lien_image);
				  	$('#udelete').show();
				  	$('#ludelete').show();
				}

				else 
				{
					$('#currentImgContent').addClass('hidden');
					$('#lnoCurrentImage').text("Pas d'image pour le moment");
				  	$('#lnoCurrentImage').removeClass('hidden');
				  	$('#currentImage').addClass('hidden');
				  	$('#currentImage').css('display', 'block');
				  	$('#udelete').hide();
				  	$('#ludelete').hide();
				}

				if(lien_video != "")
				{
				  	$("#uvideoCurrent").attr("src", lien_video);
				  	$('#uvideo').val(lien_video);
				}

				$("#uoldimg").val(lien_image);

				$('#ucolorPicked').val(couleur);
				$('#ucolorPickedText').val(couleur_texte);

				$("#unumero").val(numero);
				$("#utitre").val(titre);
				$("#utexte").val(texte); 
				$("#uexterne").val(externe);

				if(gras == 1)
				{
					$('#ubold').prop('checked', true);
				}

				else
				{
					$('#ubold').prop('checked', false);
				}

				$('#utodelete').val('false');
			}
		});
	}

	function getSlideIntroEnding(id)
	{
		$.ajax({
			url: 'modules/projectPresentation/includes/getSlide.php',
			type: 'POST',
			data: {id: id},
			success: function(data){
				let json = JSON.parse(data);
				let numero = json[0].numero;
				let type = json[0].type_nom;
				let lien_image = json[0].lien_image;
				let couleur_texte = json[0].couleur_texte;
				let titre = json[0].titre;
				let texte = json[0].texte;
				let value = json[0].date_creation;
				let gras = json[0].gras;

				let parts = value.split("-");
				let date = parts[2] + "/" + parts[1] + "/" + parts[0];

				if(type == "Introduction")
				{
					$('#luieimg').show();
					$('#uieimg').show();
					$('#lienoCurrentImage').show();
				  	$('#liecurrentImage').show();
				  	$('#iecurrentImage').show(); 
				  	$('#luiedate').show(); 
				  	$('#uieDate').show();

				  	$('#luietexte').hide();
				  	$('#uietexte').hide();

					if(lien_image != "")
					{
						$('#uiecurrentImgContent').removeClass('hidden');
					  	$('#lienoCurrentImage').addClass('hidden');
					  	$('#iecurrentImage').removeClass('hidden');
					  	$("#iecurrentImage").attr("src", "https://app.1984.agency/modules/projectPresentation/"+lien_image);
					  	$('#uiedelete').show();
				  		$('#luiedelete').show();

				  		$("#uieoldimg").val(lien_image);
					}

					else 
					{
						$('#uiecurrentImgContent').addClass('hidden');
						$('#lienoCurrentImage').text("Pas d'image pour le moment");
					  	$('#lienoCurrentImage').removeClass('hidden');
					  	$('#iecurrentImage').addClass('hidden');
					  	$('#iecurrentImage').css('display', 'block');
					  	$('#uiedelete').hide();
				  		$('#luiedelete').hide();
					}

					$('#uieType').val("Introduction");
				}

				else 
				{
					$('#luieimg').hide();
					$('#uieimg').hide();
					$('#lienoCurrentImage').hide();
				  	$('#liecurrentImage').hide();
				  	$('#iecurrentImage').hide();  
				  	$('#uiedelete').hide();
				  	$('#luiedelete').hide();
				  	$('#luiedate').hide();
				  	$('#uieDate').hide();

				  	$('#luietexte').show();
				  	$('#uietexte').show();

				  	$('#uieType').val("Remerciements");
				}
		
				$('#uieDate').val(date);
				$('#uietitre').val(titre);
				$('#uietexte').val(texte);
				$('#uienumero').val(numero);

				$('#uiecolorText').spectrum("destroy");
				$("#uiecolorText").spectrum({
					color: couleur_texte,
					palette: g_palette_text,
					showPalette : true,
					hideAfterPaletteSelect: true,
					preferredFormat: "hex",
					change: function(color){
						let couleurChoisie = $('#uiecolorText').spectrum("get");
						let col = couleurChoisie.toHexString();
						$('#uiecolorPickedText').val(col);
						$('#uiecolorHexText').val(col);
					}
				});
				$('#uiecolorHexText').val(couleur_texte);

				$('#uiecolorHexText').keyup(function(event) {
				    let value = $(this).val();
				    if(!value)
				    {
				    	value = "#ffffff";
				    }
				    if(value.length == 7)
				    {
				    	$("#uiecolorText").spectrum("set", value);
				    	$('#uiecolorPickedText').val('#ffffff');
				    }
				});

				$('#uiecolorPickedText').val(couleur_texte);
				$('#uiedelete').prop('checked', false);
				$('#uietodelete').val('false');

				if(gras == 1)
				{
					$('#uiebold').prop('checked', true);
				}

				else
				{
					$('#uiebold').prop('checked', false);
				}
			}
		});
	}

	$('.sort').sortable({
		cancel: '.first-slide, .intro, .ending',
		helper: 'clone',
		tolerance: 'pointer',
		stop: function (event, ui) {
			let ids = $(this).sortable('serialize');
			$.ajax({
				url: 'modules/projectPresentation/includes/updateOrder.php',
				type: 'POST',
				data: ids,
				success: function(data){
				    //console.log("modifications effectuées.");
				}
			});
		}
	});
		
	$(".sort").disableSelection();

	function setLinks(id_slide_p, nb_p, e_p)
	{
		let id_projet = $('.sort').attr('id');
		let id_slide = id_slide_p;

		if($(e_p.target).is('.btn-delete') || $(e_p.target).is('.d'))
		{
			let c = confirm("Voulez vous vraiment supprimer cette slide ? Cette action est irréversible.");
			if(c== true)
			{
								
				$.ajax({
					url: 'modules/projectPresentation/includes/deleteSlide.php',
					type: 'POST',
					data: {id: nb_p},
					success: function(data){
						$('#item_'+nb_p).remove();
					}
				});
			}

			else 
			{
				return false;
			}
							
		}

		else if($(e_p.target).is('.lien-preview') || $(e_p.target).is('.v'))
		{
			e_p.stopPropagation();
		}

		else
		{
			getSlide(nb_p);
			$('#updateSlideForm').modal('show');
		}
	}

	function setLinksIntroEnding(id_slide_p, nb_p, e_p, type_p)
	{
		let id_projet = $('.sort').attr('id');
		let id_slide = id_slide_p;
	
		if($(e_p.target).is('.btn-delete') || $(e_p.target).is('.d'))
		{
			let c = confirm("Voulez vous vraiment supprimer cette slide ? Cette action est irréversible.");
			if(c== true)
			{
								
				$.ajax({
					url: 'modules/projectPresentation/includes/deleteSlide.php',
					type: 'POST',
					data: {id: nb_p},
					success: function(data){
						$('#item_'+nb_p).remove();
					}
				});
			}

			else 
			{
				return false;
			}
							
		}

		else if($(e_p.target).is('.lien-preview') || $(e_p.target).is('.v'))
		{
			e_p.stopPropagation();
		}

		else
		{
			getSlideIntroEnding(nb_p);
			if(type_p == "Introduction")
			{
				$('#updateIEForm').modal('show');
			}
			else 
			{
				$('#updateIEForm').modal('show');
			}			
		}
	}

	$('.sort').on('click', '.box-preview-intro', function(e) {
		$('#updateIEForm').find('.modal-title').text('Modifier une introduction');
	    $('.i').css('display', 'block');
		$('.loading').css('display', 'none');
		let id_slide = this.id;
		let nb = id_slide.replace('item_','');
		setLinksIntroEnding(id_slide, nb, e, type);
	});

	$('.sort').on('click', '.box-preview-ending', function(e) {
		$('#updateIEForm').find('.modal-title').text('Modifier les remerciements');
	    $('.e').css('display', 'block');
		$('.loading').css('display', 'none');
		let id_slide = this.id;
		let nb = id_slide.replace('item_','');
		let type = "Remerciements";
		setLinksIntroEnding(id_slide, nb, e, type);
	});

	$('.sort').on('click', '.box-preview-image', function(e) {
	    $('.upd').css('display', 'block');
		$('.loading').css('display', 'none');
		let id_slide = this.id;
		let nb = id_slide.replace('item_','');
		setLinks(id_slide, nb, e);
	});

	$('.sort').on('click', '.box-preview', function(e) {
	    $('.upd').css('display', 'block');
		$('.loading').css('display', 'none');
		let id_slide = this.id;
		let nb = id_slide.replace('item_','');
		setLinks(id_slide, nb, e);
	});

	$('.sort').on('click', '.box-preview-video', function(e) {
	    $('.upd').css('display', 'block');
		$('.loading').css('display', 'none');
		let id_slide = this.id;
		let nb = id_slide.replace('item_','');
		setLinks(id_slide, nb, e);
	});

	$('.sort').on('click', '#divAddSlide', function(e) {
		hideAll();
		$('.loading').css('display', 'none');
		$('.g-c').css('display', 'none');
		let selectedText = $('#selectedType').val();

		if(selectedText)
		{
			getModal(selectedText);
		}

		$('#addSlideForm').modal('show');
		g_palette = getColor();
		g_palette_text = getColorText();
	});

	$('.nav').on('click', '#btnIntro, #btnEnding', function(event) {
		event.preventDefault();
		g_palette = getColor();
		g_palette_text = getColorText();
	});

	$('#introForm').on('change', '.datepicker', function(event) {
		event.preventDefault();
		$('.datetimepicker').css('display', 'none');
	});

	$('#IEForm').on('change', '.datepicker', function(event) {
		event.preventDefault();
		$('.datetimepicker').css('display', 'none');
	});

	$('.current').on('click', '#udelete', function(event) {
		event.preventDefault();		
		let c = confirm("Voulez vous vraiment supprimer cette image ?");
		if(c == true)
		{
			$('#utodelete').val('true');
			$('#lnoCurrentImage').text("L'image sera supprimée définitivement");
			$('#currentImgContent').addClass('hidden');
			$('#lnoCurrentImage').removeClass('hidden');
			return false;
		}

		else 
		{
			$('#lnoCurrentImage').text("Pas d'image pour le moment");
			$('#utodelete').val('false');
			$('#currentImgContent').removeClass('hidden');
			$('#lnoCurrentImage').addClass('hidden');
			return false;
		}
	});

	$('.current').on('click', '#uiedelete', function(event) {
		event.preventDefault();		
		let c = confirm("Voulez vous vraiment supprimer cette image ?");
		if(c == true)
		{
			$('#uietodelete').val('true');
			$('#lienoCurrentImage').text("L'image sera supprimée définitivement a la validation");
			$('#uiecurrentImgContent').addClass('hidden');
			$('#lienoCurrentImage').removeClass('hidden');
			return false;
		}

		else 
		{
			$('#lienoCurrentImage').text("Pas d'image pour le moment");
			$('#uietodelete').val('false');
			$('#uiecurrentImgContent').removeClass('hidden');
			$('#lienoCurrentImage').addClass('hidden');
			return false;
		}
	});
});

		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
			    <ul class="nav navbar-nav">
			      	<li><a type="button" class="btn previous" href="index.php?page=projectPresentation"><i class="material-icons">keyboard_backspace</i>Liste des projets</a></li>
			      	<li><a type="button" data-toggle="modal" data-target="#addIntroForm" class="btn btn-add intro" id="btnIntro"><i class="material-icons">add</i>Introduction</a></li>
			      	<li><a type="button" data-toggle="modal" data-target="#addEndingForm" class="btn btn-add" id="btnEnding"><i class="material-icons">add</i>Remerciements</a></li>
			    </ul>
			    <ul class="nav navbar-nav navbar-right">
			      	<?php 
			      		$getId = getProjectId($_GET["projet"]);
			      		$url = $_GET["projet"];
						$urls = createUrl($url);
						$url_paral = $urls['paral'];
			      		$data = queryDb('SELECT nom FROM projet WHERE numero ='.$getId);
			      		$nom_projet = $data[0]['nom'];
			      		echo '<li><a href="'.$url_paral.'" target="_blank" type="button" class="btn">'.$nom_projet.'</a></li>'; 
			      	?>   	
			    </ul>
			</div>
		</nav>
			
		<div id="<?php echo $getId = getProjectId($_GET["projet"]); ?>" class="container-fluid list-slides sort">	
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box" id="divAddSlide">
					<div class="first-slide">
						<i class="i-first material-icons white000">add</i>
					</div>
				</div>

			<?php

				$getId = getProjectId($_GET["projet"]);
				$url = $_GET["projet"];
				$urls = createUrl($url);
				$url_paral = $urls['paral'];

				$data_intro = queryDb('SELECT * FROM slide,type_slide WHERE slide.type_slide = type_slide.id AND type_slide.nom = "Introduction" AND slide.num_projet ='.$getId);

				foreach ($data_intro as $row_intro) 
				{
					$num_intro = $row_intro["numero"];
					$lien_image_intro = $row_intro["lien_image"];
					$texte_intro = $row_intro["titre"];
					$date = $row_intro["date_creation"];
					$font = $row_intro["couleur_texte"];
					$gras = $row_intro["gras"];

					$val = explode('-', $date);
					$date_intro = $val[2]."-".$val[1].'-'.$val[0];

					if(!empty($lien_image_intro))
					{
						echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-intro" id="item_'. $num_intro.'">
								<div class="intro" style="background-color:#000">
									<a href="javascript:void(0);"><img src="'.$lien_image_intro.'" class="img-thumbnail"></a>
									<div class="block-logo">
										<img class="img-logo" src="https://app.1984.agency/modules/projectPresentation/images/logo-transparent.png">
									</div>
									<div class="contentp">';
									if($gras == 1)
									{
										echo '<h3 style="color:'.$font.'"><strong>'.$texte_intro.'</strong></h3>';
									}
									else
									{
										echo '<h3 style="color:'.$font.'">'.$texte_intro.'</h3>';
									}

							echo '</div>
									<div class="contentp-date">
										<h3 style="color:'.$font.'">'.$date_intro.'</h3>
									</div>
									<a href="'.$url_paral.'#'.$num_intro.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
									<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i></button>
								</div>
							</div>';
					}

					else
					{
						echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-intro" id="item_'. $num_intro.'">
								<div class="intro" style="background-color:#000">
									<div class="block-logo">
										<img class="img-logo" src="https://app.1984.agency/modules/projectPresentation/images/logo-transparent.png">
									</div>
									<div class="contentp">';
									if($gras == 1)
									{
										echo '<h3 style="color:'.$font.'"><strong>'.$texte_intro.'</strong></h3>';
									}
									else
									{
										echo '<h3 style="color:'.$font.'">'.$texte_intro.'</h3>';
									}

							echo 	'</div>
									<div class="contentp-date">
										<h3 style="color:'.$font.'">'.$date_intro.'</h3>
									</div>
									<a href="'.$url_paral.'#'.$num_intro.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
									<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i></button>
								</div>
							</div>';
					}
				}

				$data = queryDb('SELECT type_slide.nom as type, slide.*
								FROM type_slide, slide 
								WHERE type_slide.id = slide.type_slide 
								AND num_projet = '.$getId.' 
								AND slide.type_slide NOT IN (
								    SELECT type_slide.id 
								    FROM type_slide 
								    WHERE type_slide.nom = "Introduction" 
								    OR type_slide.nom = "Remerciements" )
								ORDER BY ordre ASC');

				$nb = count($data);

				foreach ($data as $row) 
				{
					$numero = $row["numero"];
					$type_slide = $row["type"];
					$lien_image = $row["lien_image"];
					$couleur = $row["couleur"];
					$font = $row["couleur_texte"];
					$font = $row["couleur_texte"];
					$titre = $row["titre"];
					$texte = $row["texte"];
					$gras = $row["gras"];
					$lien_video =$row["lien_video"];
					$ordre = $row["ordre"];

					if ($type_slide == "Présentation") 
					{
						// Affichage si image
						if(!empty($lien_image))
						{
							if(!empty($titre) && $gras == 1)
							{
								if(!empty($texte))
								{
									// Image + titre gras + texte
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $numero.'">
									<div class="preview-slide">
								    		<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$lien_image.'" class="img-thumbnail"></a>
								    		<div class="contentp">
								    			<h3 style="color:'.$font.'"><strong>'.$titre.'</strong></h3>
								    			<h3 style="color:'.$font.'">'.$texte.'</h3>
								    		</div>
								    		<a href="'.$url_paral.'#'.$numero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    		<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
								    	</div>';
									}

								else
								{	// Image + titre gras
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $numero.'">
									<div class="preview-slide">
							    			<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$lien_image.'" class="img-thumbnail"></a>
							    			<div class="contentp">
							    				<h2 style="color:'.$font.'"><strong>'.$titre.'</strong></h2>
							    			</div>
							    			<a href="'.$url_paral.'#'.$numero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
							    			<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
							    		</div>';
								}						
							}

							elseif (!empty($titre) && $gras == 0) 
							{
								if(!empty($texte))
								{
									// Image + titre + texte
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $numero.'">
										<div class="preview-slide">
								    		<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$lien_image.'" class="img-thumbnail"></a>
								    		<div class="contentp">
								    			<h3 style="color:'.$font.'">'.$titre.'</h3>
								    			<h3 style="color:'.$font.'">'.$texte.'</h3>
								    		</div>
								    		<a href="'.$url_paral.'#'.$numero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    		<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
								    	</div>';
								}

								else
								{	
									// Image + titre
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $numero.'">
										<div class="preview-slide">
								    	<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$lien_image.'" class="img-thumbnail"></a>
								    	<div class="contentp">
								    		<h2 style="color:'.$font.'">'.$titre.'</h2>
								    	</div>
								    	<a href="'.$url_paral.'#'.$numero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    	<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    	</div>
								    </div>';
								}
							}

							else
							{
								// Image
								echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-image s" id="item_'. $numero.'">
									<div class="preview-slide">
								    	<a href="javascript:void(0);" class="editSlide"><img src="https://app.1984.agency/modules/projectPresentation/'.$lien_image.'" class="img-thumbnail"></a>
								    	<a href="'.$url_paral.'#'.$numero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    	<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    	</div>
								    </div>';
							}	
						}

						// Affichage sans image
						else
						{
							if(!empty($titre) && $gras == 1)
							{
								if(!empty($texte))
								{
									// Couleur + Titre gras + texte
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $numero.'">
									<div class="no-image" style="background-color:'.$couleur.'">
						    				<a href="javascript:void(0);" class="editSlide"></a>
						    				<div class="contentp">
						    					<h3 style="color:'.$font.'"><strong>'.$titre.'</strong></h3>
								    			<h3 style="color:'.$font.'">'.$texte.'</h3>
								    		</div>
								    		<a href="'.$url_paral.'#'.$numero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    		<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
						    			</div>';
								}

								else
								{
									// Couleur + Titre gras
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $numero.'">
									<div class="no-image" style="background-color:'.$couleur.'">
						    				<a href="javascript:void(0);" class="editSlide"></a>
						    				<div class="contentp">
						    					<h3 style="color:'.$font.'"><strong>'.$titre.'</strong></h3>
						    				</div>
						    				<a href="'.$url_paral.'#'.$numero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
						    				<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
						    			</div>';
								}						
							}

							elseif (!empty($titre) && $gras == 0) 
							{
								if(!empty($texte))
								{
									// Couleur + Titre + texte
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $numero.'">
									<div class="no-image" style="background-color:'.$couleur.'">
						    				<a href="javascript:void(0);" class="editSlide"></a>
						    				<div class="contentp">
						    					<h3 style="color:'.$font.'">'.$titre.'</h3>
								    			<h3 style="color:'.$font.'">'.$texte.'</h3>
								    		</div>
								    		<a href="'.$url_paral.'#'.$numero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								    		<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
						    			</div>';
									
								}

								else
								{
									// Couleur + Titre
									echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $numero.'">
									<div class="no-image" style="background-color:'.$couleur.'">
						    				<a href="javascript:void(0);" class="editSlide"></a>
						    				<div class="contentp">
						    					<h3 style="color:'.$font.'">'.$titre.'</h3>
						    				</div>
						    				<a href="'.$url_paral.'#'.$numero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
						    				<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    		</div>
						    			</div>';
								}
							}

							else
							{
								// Couleur 
								echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview s" id="item_'. $numero.'">
								<div class="no-image" style="background-color:'.$couleur.'">
						    			<a href="javascript:void(0);" class="editSlide"></a>
						    			<a href="'.$url_paral.'#'.$numero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
						    			<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								    		</button>
								    	</div>
						    		</div>';				
							}	
						}
					}

					// Affichage si vidéo
					else
					{
						echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-video s" id="item_'. $numero.'">
						<div class="video editSlide">
				    			<iframe class="preview-video" width="100%" height="100%" src="'.$lien_video.'" frameborder="0" allowfullscreen></iframe>
				    			<a href="'.$url_paral.'#'.$numero.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
				    			<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								</button>
								</div>
				    		</div>';
					}
				}

				$data_ending = queryDb('SELECT * FROM slide,type_slide WHERE slide.type_slide = type_slide.id AND type_slide.nom = "Remerciements" AND slide.num_projet ='.$getId);

				foreach ($data_ending as $row_ending) 
				{
					$pnum_ending = $row_ending["numero"];
					$ptexte_ending = $row_ending["titre"];
					$pparticipants = $row_ending["texte"];
					$font = $row_ending["couleur_texte"];
					$pgras = $row_ending["gras"];

					$list = explode(",", $pparticipants);

					if($pgras == 1)
					{
						echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-ending" id="item_'. $pnum_ending.'">
							<div class="ending" style="background-color: #000">
								<div class="contentp">
									<h3 style="color:'.$font.'"><strong>'.$ptexte_ending.'</strong></h3>
								</div>
								<div class="contentp-ending">';
								foreach ($list as $participants) 
								{
									echo '<h4 style="color:'.$font.'">'.$participants.'</h4>';
								}
						echo   '</div>
								<a href="'.$url_paral.'#'.$pnum_ending.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								</button>
							</div>
						</div>';
					}

					else
					{
						echo '<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-xs-12 box box-preview-ending" id="item_'. $pnum_ending.'">
							<div class="ending" style="background-color: #000">
								<div class="contentp">
									<h3 style="color:'.$font.'">'.$ptexte_ending.'</h3>
								</div>
								<div class="contentp-ending">';
								foreach ($list as $participants) 
								{
									echo '<h4 style="color:'.$font.'">'.$participants.'</h4>';
								}
						echo   '</div>
								<a href="'.$url_paral.'#'.$pnum_ending.'" target="_blank" type="button" class="btn btn-success lien-preview"><i class="v material-icons">search</i></a>
								<button type="button" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
								</button>
							</div>
						</div>';
					}
				}
			?>
		</div>
	</div>

		<div class="modal fade" id="addSlideForm" role="dialog">
		    <div class="modal-dialog">
		      	<div class="modal-content">
		        	<div class="modal-header">
		          		<button type="button" class="close" data-dismiss="modal">&times;</button>
		          		<h4 class="modal-title">Ajouter une slide</h4>
		        	</div>
			        <div class="modal-body">
			          	<form class="form-horizontal" method="post" id="slideFormulaire" autocomplete="off" enctype="multipart/form-data">
						    <div class="form-group" id="type">
						      	<label class="control-label col-sm-2" for="type">Type : </label>
						      	<div class="col-sm-5">
							      	<div class="dropdown">
									    <button class="btn btn-default dropdown-toggle" type="button" id="buttonType" data-toggle="dropdown">Type
									    <span class="caret"></span></button>
									    <ul class="dropdown-menu" role="menu" aria-labelledby="menuType" id="dropdownMenu">
									      	<li role="presentation"><a role="menuitem" tabindex="-1">Présentation</a></li>
									      	<li role="video"><a role="menuitem" tabindex="-1">Vidéo</a></li>
									   	</ul>
									</div>
						      	</div>
						      	<label class="control-label col-sm-4" id ="type" for="type"></label>
						    </div>

						    <div class='form-group g g-p'>
						    	<label class="control-label col-sm-2" for="img">Image :</label>
						        <div class='uploadBlock col-sm-10' id="img">
						        	<div class="col-sm-12 upload-input">
						        		<?php include('modules/upload/upload.php'); ?>
							            <div class='param'>
							                <input type='hidden' class='width' value='1920' />
							                <input type='hidden' class='height' value='1080' />
							                <input type="hidden" id="resizable" value="false" >
							                <input type="hidden" id="ajax" value="false" >
							            </div>
						        	</div>    
						        </div>
							</div>

						    <div class="form-group g g-p g-c couleur">
						      	<label class="control-label col-sm-2" for="color">Couleur :</label>
						      	<div class="col-sm-4">          
						        	<input class="form-control" type="text" id="color" name="color">
						      	</div>
						      	<label class="control-label col-sm-3 " for="colorHex">Ou saisir :</label>
						      	<div class="col-sm-3">          
						        	<input class="form-control" type="text" id="colorHex" placeholder="Code héxadécimal" name="colorHex">
						      	</div>
						    </div>

						    <div class="form-group g g-p g-ct font">
						      	<label class="control-label col-sm-2" for="colorText">Couleur du texte :</label>
						      	<div class="col-sm-4">          
						        	<input class="form-control" type="text" id="colorText" name="colorText">
						      	</div>
						      	<label class="control-label col-sm-3 " for="colorHexText">Ou saisir :</label>
						      	<div class="col-sm-3">          
						        	<input class="form-control" type="text" id="colorHexText" placeholder="Code héxadécimal" name="colorHexText">
						      	</div>
						    </div>

						    <div class="form-group g g-p">
						      	<label class="control-label col-sm-2" for="titre">Titre :</label>
						      	<div class="col-sm-10">          
						        	<input class="form-control" type="text" id="titre" placeholder="Titre" name="titre">
						      	</div>
						    </div>

						    <div class="form-group g g-p">
						      	<label class="control-label col-sm-2" for="texte">Texte :</label>
						      	<div class="col-sm-10">          
						        	<input class="form-control" type="text" id="texte" placeholder="Texte" name="texte">
						      	</div>
						    </div>

						    <div class="form-group g g-v">
						      	<label class="control-label col-sm-2" for="video">Vidéo :</label>
						      	<div class="col-sm-10">          
						        	<input class="form-control" type="text" id="video" placeholder="Lien de la vidéo YouTube" name="video">
						      	</div>
						    </div>

						    <div class="form-group g g-p g-a">
						      	<label class="control-label col-sm-2" for="externe">Lien externe :</label>
						      	<div class="col-sm-10">          
						        	<input class="form-control" type="text" id="externe" placeholder="Lien extérieur" name="externe">
						      	</div>
						    </div>
 
						    <div class="form-group g g-p">        
						      	<div class="col-sm-offset-2 col-sm-10">
						        	<div class="checkbox">
						          		<label><input class="cb" type="checkbox" id="bold" name="bold">Titre en gras</label>
						        	</div>
						      	</div>
						    </div>

						    <div class="loading">
						        <img class ="img-loading" src="https://app.1984.agency/modules/projectPresentation/images/loading.gif"> 
							</div>

						    <input type ="hidden" id="selectedType" name="selectedType">
						    <input type="hidden" id ="projectUrl" name="projectUrl" value="<?php echo $_GET['projet']; ?>">
						    <input type="hidden" name="colorPicked" value="#ffffff" id="colorPicked">
						    <input type="hidden" name="colorTextPicked" value="#ffffff" id="colorTextPicked">

						    <div class="modal-footer">
				          		<button type="submit" class="btn btn-success">Enregistrer</button>
				        	</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="updateSlideForm" role="dialog">
		    <div class="modal-dialog">
		      	<div class="modal-content">
		        	<div class="modal-header">
		          		<button type="button" class="close" data-dismiss="modal">&times;</button>
		          		<h4 class="modal-title">Modifier une slide</h4>
		        	</div>
			        <div class="modal-body">
			          	<form class="form-horizontal" method="post" id="updateSlideFormulaire" autocomplete="off" enctype="multipart/form-data">
						    <div class='form-group upd'>
						    	<label class="control-label col-sm-2" for="img" id="luimg">Image :</label>
						        <div class='uploadBlock col-sm-10' id="uimg">
						        	<div class="col-sm-12 upload-input">
							            <?php include('modules/upload/upload.php'); ?>
							            <div class='param'>
							                <input type='hidden' class='width' value='1920' />
							                <input type='hidden' class='height' value='1080' />
							                <input type="hidden" id="resizable" value="false" >
							                <input type="hidden" id="ajax" value="false" >
							            </div>
							        </div>
						        </div>
							</div>

							<div class="form-group upd current">
								<label class="control-label col-sm-2" id="lcurrentImage" for="currentImage">Actuelle :</label>
						      	<label class="control-label col-sm-10" id="lnoCurrentImage" for="currentImage">Pas d'image pour le moment</label>
						    	<div class="col-sm-10 hidden" id="currentImgContent">
						    		<div class="col-sm-12 current">
						    			<div class="block-current">
							    			<img src="" id="currentImage" class="img-thumbnail">
							    			<button type="button" id="udelete" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
											</button>
										</div>
						    		</div>   			
						    	</div>
						    </div>

						    <div id="divColor" class="form-group upd uc couleur">
						      	<label class="control-label col-sm-2" id="lucolor" for="ucolor">Couleur :</label>
						      	<div class="col-sm-4">          
						        	<input class="form-control" type="text" id="ucolor" name="ucolor">
						      	</div>
						      	<label class="control-label col-sm-3 " for="ucolorHex">Ou saisir :</label>
						      	<div class="col-sm-3">          
						        	<input class="form-control" type="text" id="ucolorHex" placeholder="Code héxadécimal" name="ucolorHex">
						      	</div>
						    </div>
	
							<div class="form-group upd uct font">
						      	<label class="control-label col-sm-2" id="lucolorText" for="ucolorText">Couleur du texte :</label>
						      	<div class="col-sm-4">          
						        	<input class="form-control" type="text" id="ucolorText" name="ucolorText">
						      	</div>
						      	<label class="control-label col-sm-3 " for="ucolorHexText">Ou saisir :</label>
						      	<div class="col-sm-3">          
						        	<input class="form-control" type="text" id="ucolorHexText" placeholder="Code héxadécimal" name="ucolorHexText">
						      	</div>
						    </div>

						    <div class="form-group upd">
						      	<label class="control-label col-sm-2" id="lutitre" for="utitre">Titre :</label>
						      	<div class="col-sm-10">          
						        	<input class="form-control" type="text" id="utitre" placeholder="Titre" name="utitre">
						      	</div>
						    </div>

						    <div class="form-group upd">
						      	<label class="control-label col-sm-2" id="lutexte" for="utexte">Texte :</label>
						      	<div class="col-sm-10">          
						        	<input class="form-control" type="text" id="utexte" placeholder="Texte" name="utexte">
						      	</div>
						    </div>

						    <div class="form-group upd">
						      	<label class="control-label col-sm-2" id="luexterne" for="uexterne">Lien externe :</label>
						      	<div class="col-sm-10">          
						        	<input class="form-control" type="text" id="uexterne" placeholder="Lien extérieur" name="uexterne">
						      	</div>
						    </div>

						    <div class="form-group upd">
						      	<label class="control-label col-sm-2" id="luvideo" for="uvideo">Vidéo :</label>
						      	<div class="col-sm-10">          
						        	<input class="form-control" type="text" id="uvideo" placeholder="Lien de la vidéo YouTube" name="uvideo">
						      	</div>
						    </div>

						    <div class="form-group upd">
						      	<label class="control-label col-sm-2" id="luvideoCurrent" for="uvideoCurrent">Actuelle :</label>
						      	<div class="col-sm-10">          
						        	<iframe width="100%" height="100%" id="uvideoCurrent" name="uvideoCurrent" src=""></iframe>
						      	</div>
						    </div>

						    <div class="form-group upd">        
						      	<div class="col-sm-offset-2 col-sm-10">
						        	<div class="checkbox">
						          		<label id="lubold"><input type="checkbox" id="ubold" name="ubold">Titre en gras</label>
						        	</div>
						      	</div>
						    </div>

						    <div class="loading">
						    	<img id ="uloading" src="https://app.1984.agency/modules/projectPresentation/images/loading.gif">
						    </div>

						    <input type="hidden" id ="projectUrl" name="uprojectUrl" value="<?php echo $_GET['projet']; ?>">
						    <input type="hidden" name="ucolorPicked" id="ucolorPicked">
						    <input type="hidden" name="ucolorPickedText" id="ucolorPickedText">
						    <input type="hidden" name="unumero" id="unumero">
						    <input type="hidden" name="uoldimg" id="uoldimg">
							<input type="hidden" name="utodelete" id="utodelete">

						    <div class="modal-footer">
				          		<button type="submit" class="btn btn-success">Enregistrer</button>
				        	</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="addIntroForm" role="dialog">
		    <div class="modal-dialog">
		      	<div class="modal-content">
		        	<div class="modal-header">
		          		<button type="button" class="close" data-dismiss="modal">&times;</button>
		          		<h4 class="modal-title">Ajouter une introduction</h4>
		        	</div>
			        <div class="modal-body">
			          	<form class="form-horizontal" method="post" id="introForm" autocomplete="off" enctype="multipart/form-data">
						    <div class='form-group i'>
						    	<label class="control-label col-sm-2" for="iimg" id="liimg">Image :</label>
						        <div class='uploadBlock col-sm-10' id="iimg">
						        	<div class="col-sm-12 upload-input">
							            <?php include('modules/upload/upload.php'); ?>
							            <div class='param'>
							                <input type='hidden' class='width' value='1920' />
							                <input type='hidden' class='height' value='1080' />
							                <input type="hidden" id="resizable" value="false" >
							                <input type="hidden" id="ajax" value="false" >
							            </div>
							        </div>
						        </div>
							</div>

						    <div class="form-group i">
						      	<label class="control-label col-sm-2" id="lidate" for="idate">Date : </label>
						      	<div class='col-sm-10'>
					                loading<input type="text" class="datepicker form-control" placeholder="Saisir une date de présentation">
					            </div>
						    </div>

						    <div class="form-group i">
						      	<label class="control-label col-sm-2" id="liclient" for="iclient">Client : </label>
						      	<div class="col-sm-10">          
						        	<input class="form-control" type="text" id="iclient" placeholder="Nom du client" name="iclient">
						      	</div>
						    </div>

						    <div class="form-group i font">
						      	<label class="control-label col-sm-2" id="licolorText" for="icolorText">Couleur du texte :</label>
						      	<div class="col-sm-4">          
						        	<input class="form-control" type="text" id="icolorText" name="icolorText">
						      	</div>
						      	<label class="control-label col-sm-3 " for="icolorHexText">Ou saisir :</label>
						      	<div class="col-sm-3">          
						        	<input class="form-control" type="text" id="icolorHexText" placeholder="Code héxadécimal" name="icolorHexText">
						      	</div>
						    </div>

						    <div class="form-group i">        
						      	<div class="col-sm-offset-2 col-sm-10">
						        	<div class="checkbox">
						          		<label id="libold"><input type="checkbox" id="ibold" name="ibold">Titre en gras</label>
						        	</div>
						      	</div>
						    </div>

						    <div class="loading">
						    	<img id ="uloading" src="https://app.1984.agency/modules/projectPresentation/images/loading.gif" alt="loading">
						    </div>

						    <input type="hidden" id ="iprojectUrl" name="iprojectUrl" value="<?php echo $_GET['projet']; ?>">
						    <input type="hidden" name="idate" id="idate">
						    <input type="hidden" name="icolorTextPicked" value="#ffffff" id="icolorTextPicked">
						    <input type="hidden" name="inumero" id="inumero">

						    <div class="modal-footer">
				          		<button type="submit" class="btn btn-success">Enregistrer</button>
				        	</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="addEndingForm" role="dialog">
		    <div class="modal-dialog">
		      	<div class="modal-content">
		        	<div class="modal-header">
		          		<button type="button" class="close" data-dismiss="modal">&times;</button>
		          		<h4 class="modal-title">Ajouter des remerciements</h4>
		        	</div>
			        <div class="modal-body">
			          	<form class="form-horizontal" method="post" id="endingForm" autocomplete="off" enctype="multipart/form-data">
						    <div class="form-group e">
						      	<label class="control-label col-sm-3" id="letexte" for="etexte">Texte :</label>
						      	<div class="col-sm-9">          
						        	<input class="form-control" type="text" id="etexte" placeholder="Texte" name="etexte">
						      	</div>
						    </div>

						    <div class="form-group e">
						      	<label class="control-label col-sm-3" id="leparticipants" for="eparticipants">Participants :</label>
						      	<div class="col-sm-9">          
						        	<input class="form-control" type="text" id="eparticipants" placeholder="Participants" name="eparticipants" data-toggle="tooltip" data-placement="bottom" title="Séparer les personnes par une virgule , afin de créer une liste de personnes">
						      	</div>
						    </div>

						    <div class="form-group e font">
						      	<label class="control-label col-sm-3" id="lecolorText" for="ecolorText">Couleur du texte :</label>
						      	<div class="col-sm-4">          
						        	<input class="form-control" type="text" id="ecolorText" name="ecolorText">
						      	</div>
						      	<label class="control-label col-sm-2 " for="ecolorHexText">Ou saisir :</label>
						      	<div class="col-sm-3">          
						        	<input class="form-control" type="text" id="ecolorHexText" placeholder="Code héxadécimal" name="ecolorHexText">
						      	</div>
						    </div>

						    <div class="form-group e">        
						      	<div class="col-sm-offset-3 col-sm-9">
						        	<div class="checkbox">
						          		<label id="lebold"><input type="checkbox" id="ebold" name="ebold">Titre en gras</label>
						        	</div>
						      	</div>
						    </div>

						    <div class="loading">
						    	<img id ="uloading" src="https://app.1984.agency/modules/projectPresentation/images/loading.gif">
						    </div>

						    <input type="hidden" id ="eprojectUrl" name="eprojectUrl" value="<?php echo $_GET['projet']; ?>">
						    <input type="hidden" name="ecolorTextPicked" value="#ffffff" id="ecolorTextPicked">

						    <div class="modal-footer">
				          		<button type="submit" class="btn btn-success">Enregistrer</button>
				        	</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="updateIEForm" role="dialog">
		    <div class="modal-dialog">
		      	<div class="modal-content">
		        	<div class="modal-header">
		          		<button type="button" class="close" data-dismiss="modal">&times;</button>
		          		<h4 class="modal-title">Modification</h4>
		        	</div>
			        <div class="modal-body">
			          	<form class="form-horizontal" method="post" id="IEForm" autocomplete="off" enctype="multipart/form-data">
						    <div class='form-group uie'>
						    	<label class="control-label col-sm-2" for="uieimg" id="luieimg">Image :</label>
						        <div class='uploadBlock col-sm-10' id="uieimg">
						        	<div class="col-sm-12 upload-input">
							            <?php include('modules/upload/upload.php'); ?>
							            <div class='param'>
							                <input type='hidden' class='width' value='1920' />
							                <input type='hidden' class='height' value='1080' />
							                <input type="hidden" id="resizable" value="false" >
							                <input type="hidden" id="ajax" value="false" >
							            </div>
							        </div>
						        </div>
							</div>

							<div class="form-group uie current">
								<label class="control-label col-sm-2" id="liecurrentImage" for="iecurrentImage">Actuelle :</label>
						      	<label class="control-label col-sm-10" id="lienoCurrentImage" for="iecurrentImage">Pas d'image pour le moment</label>
						    	<div class="col-sm-10 hidden" id="uiecurrentImgContent">
						    		<div class="col-sm-12 current">
						    			<div class="block-current">
						    				<img src="" id="iecurrentImage" class="img-thumbnail" alt="Image actuelle">
						    				<button type="button" id="uiedelete" class="btn-delete btn-danger"><i class="d material-icons">delete_forever</i>
											</button>
						    			</div>
						    		</div>   			
						    	</div>
						    </div>

						    <div class="form-group uie">
						      	<label class="control-label col-sm-2" id="luietitre" for="uietitre">Titre :</label>
						      	<div class="col-sm-10">          
						        	<input class="form-control" type="text" id="uietitre" placeholder="Titre" name="uietitre">
						      	</div>
						    </div>

						    <div class="form-group uie">
						      	<label class="control-label col-sm-2" id="luietexte" for="uietexte">Membres :</label>
						      	<div class="col-sm-10">          
						        	<input class="form-control" type="text" id="uietexte" placeholder="Participants" name="uietexte" data-toggle="tooltip" data-placement="bottom" title="Séparer les personnes par une virgule , afin de créer une liste de personnes">
						      	</div>
						    </div>

						    <div class="form-group uie uct">
						      	<label class="control-label col-sm-2" id="luiecolorText" for="uiecolorText">Couleur du texte :</label>
						      	<div class="col-sm-4">          
						        	<input class="form-control" type="text" id="uiecolorText" name="uiecolorText">
						      	</div>
						      	<label class="control-label col-sm-3 " for="uiecolorHexText">Ou saisir :</label>
						      	<div class="col-sm-3">          
						        	<input class="form-control" type="text" id="uiecolorHexText" placeholder="Code héxadécimal" name="uiecolorHexText">
						      	</div>
						    </div>

						    <div class="form-group uie">
						      	<label class="control-label col-sm-2" id="luiedate" for="uiedate">Date : </label>
						      	<div class='col-sm-10'>
					                <input type="text" class="datepicker form-control" id="uieDate">
					            </div>
						    </div>

						    <div class="form-group uie">        
						      	<div class="col-sm-offset-2 col-sm-10">
						        	<div class="checkbox">
						          		<label id="luiebold"><input type="checkbox" id="uiebold" name="uiebold">Titre en gras</label>
						        	</div>
						      	</div>
						    </div>

						    <div class="loading">
						    	<img id ="uloading" src="https://app.1984.agency/modules/projectPresentation/images/loading.gif">
						    </div>

						    <input type="hidden" id ="projectUrl" name="uieprojectUrl" value="<?php echo $_GET['projet']; ?>">
						    <input type="hidden" name="uiecolorPickedText" id="uiecolorPickedText">
						    <input type="hidden" name="uienumero" id="uienumero">
						    <input type="hidden" name="uieoldimg" id="uieoldimg">
							<input type="hidden" name="uietodelete" id="uietodelete">
							<input type="hidden" name="uiedate" id="uiedate">
							<input type="hidden" name="uieType" id="uieType">

						    <div class="modal-footer">
				          		<button type="submit" class="btn btn-success">Enregistrer</button>
				        	</div>
						</form>
					</div>
				</div>
			</div>
		</div>
<div id='message-affichage' class='alert alert-success alert-white rounded'>
    <button class="close" type="button" data-dismiss="alert" aria-hidden="true">
        <i class="material-icons">add_circle_outline</i>
    </button>
</div>

<a href="#addProjetForm" class="addround" data-toggle="modal" data-target="#addProjetForm">+</a>

<div class='row'>
    <div class='search-row col-md-12'></div>
</div>

<div class="container-fluid">
    <table class="table table-hover tablesorter table-striped" id="projets">
        <thead>
        <tr>
            <th>Nom du projet</th>
            <th>Client</th>
            <th>Entreprise</th>
            <th>Création du projet</th>
            <th>Modifier la présentation</th>
            <th>Voir la présentation</th>
            <th>Supprimer le projet</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $date_format = '%d/%m/%Y';
        $data = queryDb('SELECT *, DATE_FORMAT(date_creation, "' . $date_format . '") AS date FROM projet WHERE hidden = 0 ORDER BY date_creation DESC');

        foreach ($data as $row) {
            $url = $row['url'];
            $urls = createUrl($url);
            $url_modif = $urls['modif'];
            $url_paral = $urls['paral'];

            $idclient = $row['idclient'];
            $nom_client = getNomClient($idclient);
            $entreprise_client = getEntrepriseClient($idclient);

            echo '<tr id="projet-' . $row['numero'] . '"">
							<td class="editMe">' . $row["nom"] . '</td>
							<td style="cursor: default">' . $nom_client . '</td>
							<td style="cursor: default">' . $entreprise_client . '</td>
							<td style="cursor: default">' . $row["date"] . '</td>
							<td><a href="' . $url_modif . '" class="btn btn-primary btn-md " role="button">Modifier<i class="material-icons">edit</i></a>
							</td>
							<td><a href="' . $url_paral . '" target="_blank" class="btn btn-success btn-md" role="button">Visualiser<i class="material-icons">search</i></a>
							</td>
							<td><a href="#" id=' . $row['numero'] . ' class="button-delete btn btn-danger btn-md" role="button">Supprimer<i class="material-icons">delete_forever</i></a>
							</td>
						</tr>';
        }
        ?>
        </tbody>
    </table>
</div>

<div class="modal fade" id="addProjetForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ajouter un projet</h4>
            </div>
            <form class="form-horizontal" id="addProjet" method="post" autocomplete="off">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="projectName">Nom du projet :</label>
                        <div class="col-sm-9">
                            <input class="form-control" id="projectName"
                                   placeholder="Choisir un nom de projet" name="projectName">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="name">Nom du client :</label>
                        <div class="col-sm-9">
                            <input class="form-control" id="name" placeholder="Saisir un nom de client"
                                   name="name">
                            <div class="listing-client">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="idClient" id="idClient">
                    <input type="hidden" name="nameClient" id="nameClient">
                    <input type="hidden" name="entreprise" id="entreprise">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Enregistrer</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
	ini_set('display_errors',1); 
	include_once 'includes/main.php';

	if(!isset($_GET['projet']))
	{
		echo "<div class='block-flat'>
				<div class='row'>
					<div class='clearfix'>
					<div class='col-md-12'>
						<h3>Tous les projets</h3>
					</div>
				</div>
			</div>";
		include 'tableau.php';
	}

	else
	{
		echo "<div class='block-flat'>
				<div class='row'>
					<div class='clearfix'>
					<div class='col-md-12'>
						<h3>Modifications & Ajouts</h3>
					</div>
				</div>
			</div>";
		include 'projet.php';
	}
?>
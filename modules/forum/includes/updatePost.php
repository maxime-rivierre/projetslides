<?php
	ini_set('display_errors',1); 
	error_reporting(E_ALL);
	require_once 'main.php';
	$path = "../../../";
	include_once('../../../ajax/ajax-header.php');

	$post_id = $_POST['id'];
	$content = htmlspecialchars($_POST['content']);
	$image = $_POST['image'];

	date_default_timezone_set("Europe/Paris");
	$modification_date = gmdate('Y-m-d H:i:s');

	if($image == "true")
	{
		try
		{
			$data = queryDb('SELECT image_link FROM forum_post WHERE id ='.$post_id);
			$lien_image = $data[0]['image_link'];
			$image_to_delete = str_replace('modules/suiviClient', '..', $lien_image);

			if(!empty($lien_image) || $lien_image != "" || $lien_image == null)
			{
				$del = deleteFile($image_to_delete);
				$new_image = "";
				updatePostImage($post_id, $content, $modification_date, $new_image);
			}
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	else
	{
		updatePost($post_id, $content, $modification_date);
	}

	$last_post = getPost($post_id);

	foreach ($last_post as $post) 
	{
		$temp = nl2br(htmlspecialchars($post['content']));
		$content = changeLinkToHTML($temp);

		echo '<li class="list-group-item post" id="message-'.$post['id'].'">
				<div class="row no-margin">
		    		<div class="col-lg-2">
						<h4>'.htmlspecialchars($post['nom']).' '.htmlspecialchars($post['prenom']).'</h4>
						<p>'.convertDate($post['creation_date'], $date_format).'</p>
					</div>
					<div class="col-lg-10 post-content">
						<h3>'.$content.'</h3>';
						if(!empty($post['image_link']))
						{
							echo '<img id="image-'.$post['id'].'" src"'.$post['image_link'].'">';
						}
									
						echo '<div class="row">
								<div class="post-footer">';
								if(!empty($post['modification_date']))
								{

								echo '<div class="row no-margin">
										<div class="col-lg-6 block-date">
											<p>Dernière modification le : '.convertDate($post['modification_date'], $date_format).'</p>
										</div>
										<div class="col-lg-6 text-right block-edit">';
											/*
											* A utiliser en prod
											*/
											/*if($post['user_id'] == $user['user_id'])
											{
												echo '<button type="button" class="btn-delete btn-danger delete-link" id="delete-'.$post['id'].'">Supprimer</button>
														<a href="#editPost" class="btn btn-default edit-link" id="post-'.$post['id'].'">Modifier</a>';
											}*/
												echo '<button type="button" class="btn-delete btn-danger delete-link" id="delete-'.$post['id'].'">Supprimer</button>
														<a href="#editPost" class="btn btn-default edit-link" id="post-'.$post['id'].'">Modifier</a>';
											echo '</div>
										</div>';
								}
								else
								{
									echo '<div class="row no-margin">
											<div class="col-lg-offset-6 col-lg-6 text-right block-edit">';
												/*
												* A utiliser en prod
												*/
												/*if($post['user_id'] == $user['user_id'])
												{
													echo '<button type="button" class="btn-delete btn-danger delete-link" id="delete-'.$post['id'].'">Supprimer</button>
															<a href="#editPost" class="btn btn-default edit-link" id="post-'.$post['id'].'">Modifier</a>';
												}*/
													echo '<button type="button" class="btn-delete btn-danger delete-link" id="delete-'.$post['id'].'">Supprimer</button>
															<a href="#editPost" class="btn btn-default edit-link" id="post-'.$post['id'].'">Modifier</a>';
											echo '</div>
										</div>';
								}

						echo '</div>
						</div>
					</div>
				</div>
			</li>';
	}

?>
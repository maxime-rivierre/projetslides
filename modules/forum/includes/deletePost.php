<?php
	ini_set('display_errors',1); 
	error_reporting(E_ALL);
	require_once 'main.php';
	$path = "../../../";
	include_once('../../../ajax/ajax-header.php');

	$id = $_POST['id'];
	try 
	{
		deletePost($id);
		echo "Message supprimé avec succès.";
		
	} 
	catch (Exception $e) 
	{
		echo $e->getMessage();
	}
?>
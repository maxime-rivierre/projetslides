<?php
	ini_set('display_errors',1); 
	error_reporting(E_ALL);
	require_once 'main.php';
	$path = "../../../";
	include_once('../../../ajax/ajax-header.php');

	$topic_id = $_POST['postTopic'];
	$post = htmlspecialchars($_POST['topicPost']);
	$get_notifs = 0;

	if(isset($_POST['getNotifPost']))
	{
		$get_notifs = 1;
	}

	/*
	* A utiliser en prod
	*/
	//$user_id = $user['user_id'];
	$user_id = 1;

	date_default_timezone_set("Europe/Paris");
	$creation_date = gmdate('Y-m-d H:i:s');

	if(!empty($_POST['imgUploaded']))
	{
	    $split = explode(',',$_POST['imgUploaded']);
	    $data = $split[1];
	    $data = base64_decode($data);
	    $split = explode(';',$split[0])[0];
	    $extension = explode('/',$split)[1];
	    $directory = '../images/';
	    $chemin = 'modules/suiviClient/images/';
	    $token = createToken(25);
	    $filename = date('d-m-Y').$token.'.'.$extension;
	    $chemin_image = $chemin.$filename;
	    file_put_contents($directory.$filename, $data);

	    createPost($creation_date, $chemin_image, $post, $user_id, $topic_id, $get_notifs, 1);
	}
	
	else
	{
		$chemin_image = "";
		createPost($creation_date, $chemin_image, $post, $user_id, $topic_id, $get_notifs, 1);
	}

	$post_id = getLastPostId();
	updateTopic($topic_id, $post_id);

	$res = getPost($post_id);
	foreach ($res as $post) 
	{
		$content = nl2br(htmlspecialchars($post['content']));

		echo '<li class="list-group-item post" id="message-'.$post['id'].'">
				<div class="row no-margin">
		    		<div class="col-lg-2">
						<h4>'.htmlspecialchars($post['nom']).' '.htmlspecialchars($post['prenom']).'</h4>
						<p>'.convertDate($post['creation_date'], $date_format).'</p>
					</div>
					<div class="col-lg-10 post-content">
						<h3>'.$content.'</h3>';
						if(!empty($post['image_link']))
						{
							echo '<img id="image-'.$post['id'].'" src="'.$post['image_link'].'">';
						}
									
						echo '<div class="row">
								<div class="post-footer">';
								if(!empty($post['modification_date']))
								{

									echo '<div class="row no-margin">
											<div class="col-lg-6 block-date">
												<p>Dernière modification le : '.convertDate($post['modification_date'], $date_format).'</p>
											</div>
											<div class="col-lg-6 text-right block-edit">';
											/*
											* A utiliser en prod
											*/
											/*if($post['user_id'] == $user['user_id'])
											{
												echo '<button type="button" class="btn-delete btn-danger delete-link" id="delete-'.$post['id'].'">Supprimer</button>
														<a href="#editPost" class="btn btn-default edit-link" id="post-'.$post['id'].'">Modifier</a>';
											}*/
												echo '<button type="button" class="btn-delete btn-danger delete-link" id="delete-'.$post['id'].'">Supprimer</button>
														<a href="#editPost" class="btn btn-default edit-link" id="post-'.$post['id'].'">Modifier</a>';
											echo '</div>
										</div>';
								}
								else
								{
									echo '<div class="row no-margin">
											<div class="col-lg-offset-6 col-lg-6 text-right block-edit">';
												/*
												* A utiliser en prod
												*/
												/*if($post['user_id'] == $user['user_id'])
												{
													echo '<button type="button" class="btn-delete btn-danger delete-link" id="delete-'.$post['id'].'">Supprimer</button>
															<a href="#editPost" class="btn btn-default edit-link" id="post-'.$post['id'].'">Modifier</a>';
												}*/
													echo '<button type="button" class="btn-delete btn-danger delete-link" id="delete-'.$post['id'].'">Supprimer</button>
															<a href="#editPost" class="btn btn-default edit-link" id="post-'.$post['id'].'">Modifier</a>';
										echo '</div>
										 </div>';
								}

						 echo '</div>
						  	</div>
						</div>
					</div>
				</li>';

		$id_createur = getTopicCreateur($topic_id);

		/*
		* A utiliser en prod
		*/
		//$from = "1984.agency@gmail.com";
		//$to = $user['user_email'];

		$from = "1984.agency@gmail.com";
		$to = "maxime.rivierre@1984.agency";

		$notif_user = checkIfUserWantTopicNotif($topic_id, $user_id);
		$topic_name = getTopicName($post['id']);

		if($notif_user[0]['get_notif_after_post'] == 1)
		{
			$content = $user['user_prenom'].' '.$user['user_nom'].' a posté un message sur '.$topic_name.'.';
			$subject = "Nouveau message sur le sujet : ".$topic_name;
			sendMail($from, $to, $content, $subject);
		}

		if($user_id == $id_createur)
		{
			return;
		}
		else
		{
			$res = getNotifTopicCreateur($topic_id, $id_createur);
			if(!empty($res))
			{
				if($res == 1)
				{
					$content = $user['user_prenom'].' '.$user['user_nom'].' a posté un message sur votre sujet.';
					$name = getTopicName($post['id']);
					$subject = "Nouveau message sur le sujet : ".$name;

					sendMail($from, $to, $content, $subject);
				}
			}	
		}
	}
?>
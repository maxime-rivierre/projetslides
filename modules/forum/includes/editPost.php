<?php
	ini_set('display_errors',1); 
	error_reporting(E_ALL);
	require_once 'main.php';
	$path = "../../../";
	include_once('../../../ajax/ajax-header.php');

	$post = getPost($_POST['id']);

	/*
	* Utilisateur a récupéré une fois mis en prod
	*/
	//$user_id = $user['user_id'];

	/*
	* Utilisateur temporaire dans la table forum_user
	*/

	$user_id = 1;
	$etat = "";
	$content = "";
	$image_link = "";

	if($post[0]['user_id'] == $user_id)
	{
		$content =  $post[0]['content'];
		if(!empty($post[0]['image_link']))
		{
			$image_link = $post[0]['image_link'];
		}
		$etat = "editable";
		$id = $post[0]['id'];
		$tab =  array($etat, $content, $id, $image_link);
		echo json_encode($tab);
	}

	else
	{
		$etat = "not_user";
		$tab = array($etat);
		echo json_encode($tab);
	}
?>
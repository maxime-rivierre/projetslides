<?php 
	// Affiche les erreurs PHP
    require_once $_SERVER['DOCUMENT_ROOT'].'/projet_slides/conf.php';
	ini_set('display_errors',1); 
	error_reporting(E_ALL);
	$date_format = 'l j F Y \à H:i:s';

	function dbConnect()
	{
        $server = "mysql:host=localhost;dbname=".$GLOBALS['server'];
        $user = $GLOBALS['user'];
        $pwd = $GLOBALS['pwd'];
		/*$user = "projet";
		$pwd = "NeQnTTYtEzzT";*/

		$options = array(
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
			PDO::ATTR_CASE => PDO::CASE_LOWER,
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
		);

		try 
		{
			$link = new PDO($server, $user, $pwd, $options);

			return $link;
		}
		catch (Exception $e)
		{
			echo "Connection à MySql impossible : ", $e->getMessage();
			die();
		}
	}

	function queryDb($query)
	{
		$dbh = dbConnect();
		try
		{
			$locale = "set lc_time_names = 'fr_FR'";
			$res = $dbh->query($query);
			$res = $res->fetchAll(PDO::FETCH_ASSOC);
			return $res;
		}
		catch(PDOException $exception)
		{
			return $exception;
		}		
	}

	function getCategories()
	{
		return queryDb('SELECT forum_category.id, forum_category.name, forum_category.creation_date, forum_user.first_name as prenom, forum_user.last_name as nom, forum_user_type.name as type FROM forum_category, forum_user, forum_user_type WHERE forum_category.user_id = forum_user.id AND forum_user_type.id = forum_user.id_status');
	}

	function getLastTopicNumber($id_category, $nb_topic)
	{
		$query = "SELECT forum_topic.id, forum_topic.texte as titre, forum_topic.creation_date, forum_user.first_name as prenom, forum_user.last_name as nom
						FROM forum_topic, forum_category, forum_user
						WHERE forum_topic.user_id = forum_user.id
						AND forum_topic.status = 1
						AND forum_topic.category_id = forum_category.id
						AND forum_topic.category_id = ".$id_category." ORDER by forum_topic.creation_date DESC LIMIT ".$nb_topic;
		return queryDb($query);
	}

	function getTopicName($post_id)
	{
		$res = queryDb('SELECT forum_topic.texte FROM forum_topic, forum_post WHERE forum_topic.last_post = forum_post.id AND forum_post.id ='.$post_id);
		return $res[0]['texte'];
	}

	function getCategoryName($topic_id)
	{
		$res = queryDb('SELECT name FROM forum_category WHERE id = '.$topic_id);
		return $res[0]['name'];
	}

	function getAllCategoryTopic($id_category, $nb_topic)
	{
		$dbh = dbConnect();
		$get_topics = $dbh->prepare('SELECT ft.id as id, ft.texte as titre, ft.creation_date as creation_topic, fu.first_name as prenom_auteur, fu.last_name as nom_auteur, 
									fp.creation_date as creation_post, fp.modification_date as modif_post, fp.user_id as id_user_last_post, fu_last.first_name as prenom_last, fu_last.last_name as nom_last
									FROM forum_topic ft, forum_category fc, forum_user fu, forum_user fu_last, forum_post fp
									WHERE ft.user_id = fu.id
									AND ft.status = 1
									AND ft.category_id = fc.id
									AND ft.category_id = :id_category
									AND ft.last_post = fp.id
									AND fu_last.id = fp.user_id
									AND fp.status = 1
									ORDER BY COALESCE (fp.modification_date, fp.creation_date, ft.creation_date) DESC
									LIMIT :nb_topic');
		$get_topics->bindParam(':id_category', $id_category);
		$get_topics->bindParam(':nb_topic', $nb_topic, PDO::PARAM_INT);
		$get_topics->execute();
		$res = $get_topics->fetchAll();
		return $res;
	}

	function lastTopicPost($id_topic)
	{
		return queryDb('SELECT forum_post.*, forum_user.first_name as prenom, forum_user.last_name as nom
						FROM forum_post, forum_topic, forum_user
						WHERE forum_post.id = forum_topic.last_post
						AND forum_post.user_id = forum_user.id
						AND forum_post.status = 1
						AND forum_topic.id ='.$id_topic);
	}

	function getNbPost($id_topic)
	{
		return queryDb('SELECT forum_topic.*, COUNT(forum_post.id) as nb_post
						FROM forum_topic, forum_post
						WHERE forum_topic.id = forum_post.topic_id
						AND forum_post.status = 1
						AND forum_topic.id = '.$id_topic.'
						GROUP BY forum_topic.id');	
	}

	function getTopicPost($id_topic)
	{
		return queryDb('SELECT forum_topic.texte, forum_post.id, forum_post.image_link, forum_post.modification_date, forum_post.creation_date, forum_post.content, 
						forum_user.first_name as prenom, forum_user.last_name as nom, forum_user.id as user_id
						FROM forum_post, forum_topic, forum_user
						WHERE forum_post.user_id = forum_user.id
						AND forum_post.topic_id = forum_topic.id
						AND forum_post.status = 1
						AND forum_topic.id = '.$id_topic.'
						ORDER BY COALESCE(forum_post.creation_date) ASC');	
	}

	function convertDate($date, $format) 
	{
	    $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
	    $french_days = array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
	    $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	    $french_months = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
	    return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date))));
	}

	function getLastTopicId()
	{
		$res = queryDb('SELECT id FROM forum_topic ORDER by id DESC LIMIT 1');
		return $res[0]['id'];
	}

	function getLastPostId()
	{
		$res = queryDb('SELECT id FROM forum_post WHERE status = 1 ORDER by id DESC LIMIT 1');
		return $res[0]['id'];
	}

	function createCategory($name, $creation_date, $user_id)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare("INSERT INTO forum_category (name, creation_date, user_id) VALUES (:name, :creation_date, :user_id)");
		$query->bindParam(':name', $name);
		$query->bindParam(':creation_date', $creation_date);
		$query->bindParam(':user_id', $user_id);
		$query->execute();
	}


	function createTopic($titre, $creation_date, $user_id, $category_id, $get_notif, $status)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare("INSERT INTO forum_topic (texte, creation_date, user_id, category_id, get_creation_notif, status) VALUES (:titre, :creation_date, :user_id, :category_id, :get_notif, :status)");
		$query->bindParam(':titre', $titre);
		$query->bindParam(':creation_date', $creation_date);
		$query->bindParam(':user_id', $user_id);
		$query->bindParam(':category_id', $category_id);
		$query->bindParam(':get_notif', $get_notif);
		$query->bindParam(':status', $status);
		$query->execute();
	}

	function updateTopic($topic_id, $post_id)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare('UPDATE forum_topic SET last_post = :post_id WHERE id =:topic_id');
		$query->bindParam(':post_id', $post_id);
		$query->bindParam(':topic_id', $topic_id);
		$query->execute();
	}

	function createPost($creation_date, $image_link, $content, $user_id, $topic_id, $get_notif_after_post, $status)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare("INSERT INTO forum_post (creation_date, image_link, content, user_id, topic_id, get_notif_after_post, status) VALUES (:creation_date, :image_link, :content, :user_id, :topic_id, :get_notif_after_post, :status)");
		$query->bindParam(':creation_date', $creation_date);
		$query->bindParam(':image_link', $image_link);
		$query->bindParam(':content', $content);
		$query->bindParam(':user_id', $user_id);
		$query->bindParam(':topic_id', $topic_id);
		$query->bindParam(':get_notif_after_post', $get_notif_after_post);
		$query->bindParam(':status', $status);
		$query->execute();
	}

	function updatePost($post_id, $content, $modification_date)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare('UPDATE forum_post SET modification_date = :modification_date, content = :content WHERE id = :post_id');
		$query->bindParam(':post_id', $post_id, PDO::PARAM_INT);
		$query->bindParam(':content', $content);
		$query->bindParam(':modification_date', $modification_date);
		$query->execute();
	}

	function updatePostImage($post_id, $content, $modification_date, $image)
	{
		$dbh = dbConnect();
		$query = $dbh->prepare('UPDATE forum_post SET modification_date = :modification_date, content = :content, image_link = :image WHERE id = :post_id');
		$query->bindParam(':post_id', $post_id, PDO::PARAM_INT);
		$query->bindParam(':content', $content);
		$query->bindParam(':modification_date', $modification_date);
		$query->bindParam(':image', $image);
		$query->execute();
	}

	function getPost($post_id)
	{
		return queryDb('SELECT forum_post.id, forum_post.image_link, forum_post.modification_date, forum_post.creation_date, forum_post.content, forum_user.first_name as prenom,
						forum_user.last_name as nom, forum_user.id as user_id
						FROM forum_post, forum_user
						WHERE forum_post.user_id = forum_user.id
						AND forum_post.status = 1
						AND forum_post.id ='.$post_id);
	}

	function checkTopicCreateur($topic_id, $user_id)
	{
		$res = queryDb('SELECT user_id FROM forum_post WHERE id ='.$topic_id);
		$user = $res[0]['user_id'];

		if($user == $user_id)
		{
			return true;
		}

		else
		{
			return false;
		}
	}

	function getTopicCreateur($topic_id)
	{
		$res = queryDb('SELECT user_id FROM forum_post WHERE id ='.$topic_id);
		return $res[0]['user_id'];
	}

	function deletePost($post_id)
	{
		$dbh = dbConnect();
		$res = $dbh->prepare('UPDATE forum_post SET status = 0 WHERE id = :post_id');
		$res->bindParam(':post_id', $post_id, PDO::PARAM_INT);
		$res->execute();
	}

	function getNotifTopicCreateur($topic_id, $user_id)
	{
		$dbh = dbConnect();
		$res = $dbh->prepare('SELECT get_creation_notif FROM forum_topic WHERE id = :topic_id AND user_id = :user_id AND status = 1');
		$res->bindParam(':topic_id', $topic_id, PDO::PARAM_INT);
		$res->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$res->execute();
		$tab = $res->fetchAll();
		if(count($tab) > 0)
		{
			return $tab[0]['get_creation_notif'];
		}
	}

	function updateNotifCreateur($topic_id, $user_id, $checked)
	{
		$user = queryDb('SELECT user_id FROM forum_topic WHERE id ='.$topic_id);
		$user_id_topic = $user[0]['user_id'];

		if($user_id_topic == $user_id)
		{
			$dbh = dbConnect();
			$res = $dbh->prepare('UPDATE forum_topic SET get_creation_notif = :checked WHERE id = :topic_id');
			$res->bindParam(':checked', $checked, PDO::PARAM_INT);
			$res->bindParam(':topic_id', $topic_id, PDO::PARAM_INT);
			$res->execute();
		}

		else
		{
			echo "Vous n'êtes pas l'auteur de ce sujet.";
		}
	}

	function setNotifNewTopic($user_id, $checked)
	{
		$res = queryDb('SELECT get_notif_new_topic FROM forum_notification WHERE user_id ='.$user_id);
		if(count($res) > 0)
		{
			$current = $res[0]['get_notif_new_topic'];
			if($current != $checked)
			{
				$dbh = dbConnect();
				$upd = $dbh->prepare('UPDATE forum_notification SET get_notif_new_topic = :get_notif_new_topic WHERE user_id = :user_id');
				$upd->bindParam(':get_notif_new_topic', $checked, PDO::PARAM_INT);
				$upd->bindParam(':user_id', $user_id, PDO::PARAM_INT);
				$upd->execute();
			}
		}
		else
		{
			$dbh = dbConnect();
			$insert = $dbh->prepare('INSERT INTO forum_notification (user_id, get_notif_new_topic) VALUES (:user_id, :checked)');
			$insert->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$insert->bindParam(':get_notif_new_topic', $checked, PDO::PARAM_INT);
			$insert->execute();
		}
		
	}

	function getNotifNewTopic($user_id)
	{
		$res = queryDb('SELECT get_notif_new_topic FROM forum_notification WHERE user_id ='.$user_id);
		if(count($res) > 0)
		{
			return $res[0]['get_notif_new_topic'];
		}
	}

	function getUserToNotif()
	{
		return queryDb('SELECT forum_user.id, forum_user.mail FROM forum_user, forum_notification WHERE forum_notification.user_id = forum_user.id 
						AND forum_notification.get_notif_new_topic = 1');
	}

	function checkIfUserWantTopicNotif($topic_id, $user_id)
	{
		$dbh = dbConnect();
		$res = $dbh->prepare('SELECT forum_post.* FROM forum_post, forum_topic 
							  WHERE forum_post.topic_id = forum_topic.id 
							  AND forum_post.user_id = :user_id 
							  AND forum_post.status = 1 
							  AND forum_post.topic_id = :topic_id 
							  AND forum_post.get_notif_after_post = 1');
		$res->bindParam(':topic_id', $topic_id, PDO::PARAM_INT);
		$res->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$res->execute();
		$tab = $res->fetchAll();
		if(count($tab) > 0)
		{
			return $tab;
		}
	}

	function createToken($longueur)
	{
		$random = random_bytes($longueur);
		$nb = bin2hex($random);
		return $nb;
	}

	function deleteFile($filename)
	{
		if(file_exists($filename))
		{
			unlink($filename);
		}
	}
?>
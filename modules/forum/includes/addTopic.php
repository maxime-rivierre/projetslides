<?php
	ini_set('display_errors',1); 
	error_reporting(E_ALL);
	require_once 'main.php';
	$path = "../../../";
	include_once('../../../ajax/ajax-header.php');

	$titre = $_POST['topicTitle'];
	$post = htmlspecialchars($_POST['topicFirstPost']);
	$image_link = "";
	$id_category = $_POST['cat'];
	$get_notifs = 0;

	if(isset($_POST['getNotif']))
	{
		$get_notifs = 1;
	}

	/*
	* Utilisateur a récupéré une fois mis en prod
	*/
	//$user_id = $user['user_id'];

	/*
	* Utilisateur temporaire dans la table forum_user
	*/

	$user_id = 1;
	$status = 1;
	date_default_timezone_set("Europe/Paris");
	$creation_date = gmdate('Y-m-d H:i:s');

	createTopic($titre, $creation_date, $user_id, $id_category, $get_notifs, $status);
	$topic_id = getLastTopicId();
	createPost($creation_date, $image_link, $post, $user_id, $topic_id, $get_notifs, $status);
	$post_id = getLastPostId();
	updateTopic($topic_id, $post_id);

	$added_topic = getAllCategoryTopic($id_category, 1);
	foreach ($added_topic as $topic) 
	{
		$nb_post = getNbPost($topic_id);
						
		echo '<li class="list-group-item topic" id="topic-'.$topic['id'].'">
				<div class="row">
		    		<div class="col-lg-4">
						<h3><a href="https://app.1984.agency/suiviClient?topic='.$topic['id'].'&cat='.$id_category.'">'.htmlspecialchars($topic['titre']).'</a></h3>
						<h4>'.htmlspecialchars($topic['nom_auteur']).' '.htmlspecialchars($topic['prenom_auteur']).'</h4>
						<p>'.convertDate($topic['creation_topic'], $date_format).'</p>
					</div>
					<div class="col-lg-offset-4 col-lg-2 text-right">
						<h3>Messages : '.$nb_post[0]['nb_post'].'</h3>
					</div>
					<div class="col-lg-2 text-right">
						<h3>'.htmlspecialchars($topic['nom_last']).' '.htmlspecialchars($topic['prenom_last']).'</h3>';
						if(!empty($topic['modif_post']))
						{
							echo '<p>'.convertDate($topic['modif_post'], $date_format).'</p>';
						}
						else
						{
							echo '<p>'.convertDate($topic['creation_post'], $date_format).'</p>';
						}
									
			  echo '</div>
				</div>
			</li>';

		$users = getUserToNotif();
		$name = getCategoryName($id_category);

		$from = "1984.agency@gmail.com";
		$content = "Un nouveau sujet a été créé par ".$user['user_prenom'].' '.$user['user_nom'].' dans la catégorie '.$name.'.';
		$subject = "Création d'un sujet";
		
		foreach ($users as $user_to_notif) 
		{
			if(!empty($user_to_notif['mail']))
			{
				$to = $user_to_notif['mail'];
				sendMail($from, $to, $content, $subject);
			}
		}
	}
?>
<?php
	ini_set('display_errors',1); 
	error_reporting(E_ALL);
	require_once 'main.php';
	$path = "../../../";
	include_once('../../../ajax/ajax-header.php');

	$titre = $_POST['categoryTitle'];
	/*
	* Utilisateur a récupéré une fois mis en prod
	*/
	//$user_id = $user['user_id'];

	/*
	* Utilisateur temporaire dans la table forum_user
	*/

	$user_id = 1;
	date_default_timezone_set("Europe/Paris");
	$creation_date = gmdate('Y-m-d H:i:s');

	createCategory($titre, $creation_date, $user_id);
?>
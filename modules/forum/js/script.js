$(document).ready(function($) 
{
	$('#getNotif').prop('checked', true);
	$('#getNotifPost').prop('checked', true);

	$('form').on('click', '.upload-result', function(event) {
		event.preventDefault();
		$('.upload-result').html('<i class="material-icons">done</i>');
		$('.upload-result').css('line-height', '0px');
		$('.upload-result').attr('disabled');
		$('.upload-result').css('background-color', '#00e600');
		$('.cr-slider').css('display', 'none');
	});

	$('form').on('click', '.upload-cancel', function() {
		$('.upload-result').removeAttr('disabled');
		$('.upload-result').html('Valider');
		$('.upload-result').css('line-height', '40px');
		$('.upload-result').css('background-color', '#00ac00');
		$('.imgInput').removeAttr('value');
		$('.cr-slider').css('display', 'block');
	});

	$('#categoryForm').submit(function(event) {
		event.preventDefault();
		var to_send = $(this).serialize();

		if($.trim($("#categoryTitle").val()) === "")
		{
			alert("Vous devez saisir un nom de catégorie.");
			return false;
		}

		$.ajax({
			url: "modules/suiviClient/includes/addCategory.php",
			type: 'POST',
			data: to_send,
			success: function(data){
				$('#addCategory').modal('toggle');
				location.reload();
			}
		})
	});

	$('#topicForm').submit(function(event) {
		event.preventDefault();
		var to_send = $(this).serialize();

		if ($.trim($("#topicTitle").val()) === "" || $.trim($("#topicFirstPost").val()) === "") 
		{
		    alert('Vous devez remplir tous les champs.');
		    return false;
		}

		$.ajax({
			url: "modules/suiviClient/includes/addTopic.php",
			type: 'POST',
			data: to_send,
			success: function(data){
				$('#addTopic').modal('toggle');
				$('.topic-list').prepend(data);
				$('#topicTitle').val('');
				$('#topicFirstPost').val('');
				$('#getNotif').prop('checked', true);
			}
		})
	});

	$('#postForm').submit(function(event) {
		event.preventDefault();
		var to_send = $(this).serialize();

		$('.form-group').css('display', 'none');
	    $('.loading').css('display', 'block');

		$.ajax({
			url: "modules/suiviClient/includes/addPost.php",
			type: 'POST',
			data: to_send,
			success: function(data){
				$('#addPost').modal('toggle');
				$('.post-list').append(data);
				$('.upload-cancel').click();
			  	$('.upload-result').html('');
			  	$('.upload-result').removeAttr('disabled');
			  	$('.upload-result').html('Valider');
				$('.upload-result').css('line-height', '40px');
				$('.upload-result').css('background-color', '#00ac00');

				$('#topicPost').val('');
				$('#getNotifPost').prop('checked', true);

				$('.imgInput').removeAttr('value');
				$('.loading').css('display', 'none');
				$('.cr-slider').css('display', 'block');
				$('.form-group').css('display', 'block');
			}
		})
	});

	$('.post').on('click', '.edit-link', function(event) {
		event.preventDefault();
		var temp = $(this).attr('id');
		var data = temp.split('-');
		var id = data[1];

		var image = $('#image-'+id).attr('src');
		if (typeof image !== typeof undefined && image !== false)
		{
  			if(image.length)
			{
				$('#btnDelete').removeClass('hidden');
			}
		}

		$.ajax({
			url: "modules/suiviClient/includes/editPost.php",
			type: 'POST',
			data: {id : id },
			success: function(data){
				var json = JSON.parse(data);
				var etat = json[0];
				if(etat == "editable")
				{
					$('#blockEditPost').removeClass('hidden');
					$('.add_post').addClass('hidden');
					$('.ta-edit-post').attr('id', 'post-'+json[2]);
					$('#editPost').text(json[1]);
					$('#editPostImage').attr('src', json[3]);
					$('html, body').animate({
				        scrollTop: $("#editPost").offset().top
				    }, 1500);
				}
				else 
				{
					alert('Vous n\'avez pas le droit de modifier ce message.');
					return false;
				}
			}
		})
	});

	$('.edit-post').on('click', '#editedPost', function(event) {
		event.preventDefault();
		var temp = $('.ta-edit-post').attr('id');
		var data = temp.split('-');
		var id = data[1];
		var content = $('#editPost').val();
		var image = $(this).attr('data-img');

		$.ajax({
			url: "modules/suiviClient/includes/updatePost.php",
			type: 'POST',
			data: {id : id, content : content, image : image},
			success: function(data){
				$('#editedPost').attr('data-img', "true");
				$('#editPostImage').removeClass('hidden');
				$('#editImageTemp').addClass('hidden');
				$('.delete-image').css('display', 'block');
				$('#btnDelete').addClass('hidden');
				location.reload(true);
			}
		})
	});

	$('.edit-post').on('click', '.delete-image', function(event) {
		event.preventDefault();
		var c = confirm("Voulez vous vraiment supprimer l'image ?");
		if(c == true)
		{
			$('#editedPost').attr('data-img', "true");
			$('#editPostImage').addClass('hidden');
			$('#editImageTemp').removeClass('hidden');
			$('.delete-image').css('display', 'none');
		}

		else 
		{
			$('#editedPost').attr('data-img', "false");
			$('#editPostImage').removeClass('hidden');
			$('#editImageTemp').addClass('hidden');
			$('.delete-image').css('display', 'block');
		}
	});

	$('.cb-topic-createur').change(function(event) {
		var checked = "";
		var temp = $('.cb-topic-createur').attr('id');
		var data = temp.split('-');
		var id = data[1];

		if($('.cb-topic-createur').is(':checked'))
		{
			checked = 1;
		}
		else 
		{
			checked = 0;
		}
		
		$.ajax({
			url: 'modules/suiviClient/includes/updateNotifCreateur.php',
			type: 'POST',
			data: {checked: checked, id: id},
			success: function(data){
				//console.log(data);
			}
		})				
	});

	$('.cb-new-topic').change(function(event) {
		var checked = "";

		if($('.cb-new-topic').is(':checked'))
		{
			checked = 1;
		}
		else 
		{
			checked = 0;
		}
		
		$.ajax({
			url: 'modules/suiviClient/includes/setNotifNewTopic.php',
			type: 'POST',
			data: {checked: checked },
			success: function(data){
				//console.log(data);
			}
		})	
	});

	$(document).on('click', '.delete-link', function(event) {
		event.preventDefault();
		var c = confirm("Voulez vous vraiment supprimer le message ?");
		if(c == true)
		{
			var temp = $(this).attr('id');
			var data = temp.split('-');
			var id = data[1];

			$.ajax({
				url: 'modules/suiviClient/includes/deletePost.php',
				type: 'POST',
				data: {id: id},
				success: function(data){
					$('#message-'+id).fadeOut(300, function()
					{ 
						$(this).remove();
					});
				}
			})
		}
		else 
		{
			return false;
		}
	});
})
<?php
	ini_set('display_errors',1); 
	error_reporting(E_ALL);
	require_once 'includes/main.php';
	$date_format = 'l j F Y \à H:i:s';

	/*
	* A utiliser en prod
	*/
	//$user_id = $user['user_id'];
	$user_id = 1;
	$notif_new = getNotifNewTopic($user_id);

	echo '<div class="row row-new-topic">
			<div class="checkbox">';
	if($notif_new == 1)
	{
		echo '<label><input class="custom-cb cb-new-topic" checked type="checkbox" id="getNotifNewTopic" name="getNotifNewTopic">Recevoir des notifications pour les nouveaux sujets</label>';
	}

	else
	{
		echo '<label><input class="custom-cb cb-new-topic" type="checkbox" id="getNotifNewTopic" name="getNotifNewTopic">Recevoir des notifications pour les nouveaux sujets</label>';
	}

	echo '</div>
		</div>';
?>

<div>
	<?php
		/*
		* Page d'accueil
		*/
		if(empty($_GET['category']) && empty($_GET['topic']))
		{
			/*
			* A afficher selon le type d'utilisateur
			*/

			echo '<div class="col-lg-offset-10 col-lg-2 text-right">
					<a href="#addCategory" class="addround add_category" data-toggle="modal" data-target="#addCategory">+</a>
				</div>';

			echo '<ol class="list-group">';

			/*
		   	* Récupère toutes les catégories
		   	*/

		   	$list_categorie = getCategories();
		    foreach ($list_categorie as $category) 
		    {
		    	echo '<li class="list-group-item category" id="category-'.$category['id'].'">
		    			<div class="row">
		    				<div class="col-lg-4">
		    					<h3><a href="index.php?page=forum&category='.$category['id'].'">'.htmlspecialchars($category['name']).'</a></h3>
							</div>
							<div class="col-lg-8 text-right">
								<h4>'.htmlspecialchars($category['nom']).' '.htmlspecialchars($category['prenom']).' - '.htmlspecialchars($category['type']).'</h4>
								<p>'.convertDate($category['creation_date'], $date_format).'</p>
							</div>
							<div class="col-lg-12">
								<ol class="list-group">';
								/*
								* Récupère les 5 derniers topics
								*/
								$all_category_topics = getAllCategoryTopic($category['id'], 5);
								foreach ($all_category_topics as $topic_category) 
								{
									$nb_post = getNbPost($topic_category['id']);
										
									echo '<li class="list-group-item topic" id="topic-'.$topic_category['id'].'">
											<div class="row">
						    					<div class="col-lg-4">
													<h3><a href="index.php?page=forum&topic='.$topic_category['id'].'&cat='.$category['id'].'">'.htmlspecialchars($topic_category['titre']).'</a></h3>
													<h4>'.htmlspecialchars($topic_category['nom_auteur']).' '.htmlspecialchars($topic_category['prenom_auteur']).'</h4>
												</div>
												<div class="col-lg-offset-6 col-lg-2 text-right">
													<h3>'.htmlspecialchars($topic_category['nom_last']).' '.htmlspecialchars($topic_category['prenom_last']).'</h3>';
													if(!empty($topic_category['modif_post']))
													{
														echo '<p>'.convertDate($topic_category['modif_post'], $date_format).'</p>';
													}
													else
													{
														echo '<p>'.convertDate($topic_category['creation_post'], $date_format).'</p>';
													}
													
										echo '</div>
											</div>
										</li>';
								}

						echo '</ol>
							</div>
						</div>
					</li>
				</ol>';

				}
			}
			/*
			* Si clic sur une catégorie
			*/
			elseif (isset($_GET['category'])) 
			{
				echo '<div class="row">
						<div class="col-lg-offset-10 col-lg-2 text-right">
							<a href="#addTopic" class="addround add_topic" data-toggle="modal" data-id="cat-'.$_GET['category'].'" data-target="#addTopic">+</a>
			    		</div>
			    	</div>
			    	<div class="row">
						<div class="col-lg-12">
							<ul class="pager">
							  	<li class="previous"><a href="index.php?page=forum">Accueil</a></li>
							  </ul>
						</div>
					  </div>
						<ol class="list-group">
							<li class="topic-header">
								<div class="row">
					    			<div class="col-lg-4">
										<h3>Titre du sujet</h3>
									</div>
									<div class="col-lg-offset-4 col-lg-2 text-right">
										<h3>Nombre de messages</h3>
									</div>
									<div class="col-lg-2 text-right">
										<h3>Dernier message</h3>
									</div>
								</div>
							</li>
						</ol>
						<ol class="list-group topic-list">';

				$all_category_topics = getAllCategoryTopic($_GET['category'], 500);

				foreach ($all_category_topics as $topic_category) 
				{
					$nb_post = getNbPost($topic_category['id']);
						
					echo '<li class="list-group-item topic" id="topic-'.$topic_category['id'].'">
							<div class="row">
		    					<div class="col-lg-4">
									<h3><a href="index.php?page=forum&topic='.$topic_category['id'].'&cat='.$_GET['category'].'">'.htmlspecialchars($topic_category['titre']).'</a></h3>
									<h4>'.htmlspecialchars($topic_category['nom_auteur']).' '.htmlspecialchars($topic_category['prenom_auteur']).'</h4>
									<p>'.convertDate($topic_category['creation_topic'], $date_format).'</p>
								</div>
								<div class="col-lg-offset-4 col-lg-2 text-right">
									<h3>Messages : '.$nb_post[0]['nb_post'].'</h3>
								</div>
								<div class="col-lg-2 text-right">
									<h3>'.htmlspecialchars($topic_category['nom_last']).' '.htmlspecialchars($topic_category['prenom_last']).'</h3>';
									if(!empty($topic_category['modif_post']))
									{
										echo '<p>'.convertDate($topic_category['modif_post'], $date_format).'</p>';
									}
									else
									{
										echo '<p>'.convertDate($topic_category['creation_post'], $date_format).'</p>';
									}
									
						echo '</div>
							</div>
						</li>
					</ol>';
				}
			}
			// Si clic sur un topic
			elseif (isset($_GET['topic'])) 
			{
				echo '<div class="row">
						<div class="col-lg-12">
							<ul class="pager">';
							if(isset($_GET['cat']))
							{
								echo '<li class="previous"><a href="index.php?page=forum&category='.$_GET['cat'].'">Retour aux sujets</a></li>';
							}
							else
							{
								echo '<li class="previous"><a href="index.php?page=forum">Retour a l\'accueil</a></li>';
							}

					echo '</ul>
						</div>
					</div>';

					/*
					* A utiliser en prod
					*/
					//$user_id = $user['user_id'];
					$user_id = 1;

					$res = checkTopicCreateur($_GET['topic'], $user_id);
					
					if($res == true)
					{
						$want_notif = getNotifTopicCreateur($_GET['topic'], $user_id);
						echo '<div class="row">
								<div class="checkbox">';
								if($want_notif == 1)
								{
									echo '<label><input class="custom-cb cb-topic-createur" checked type="checkbox" id="getNotifTopic-'.$_GET['topic'].'" name="getNotif">Recevoir des notifications pour les nouveaux messages</label>';
								}
								else
								{
									echo '<label><input class="custom-cb cb-topic-createur" type="checkbox" id="getNotifTopic-'.$_GET['topic'].'" name="getNotif">Recevoir des notifications pour les nouveaux messages</label>';
								}
							
						echo '</div>
							</div>';
					}

				$all_posts = getTopicPost($_GET['topic']);

				echo '<div class="row">
						<div class="col-lg-12">
							<h1>'.htmlspecialchars($all_posts[0]['texte']).'</h1>
						</div>
					 </div>';
						
				echo '<ol class="list-group post-list">';

				foreach ($all_posts as $post) 
				{
					$temp = nl2br(htmlspecialchars($post['content']));
					$content = changeLinkToHTML($temp);

					echo '<li class="list-group-item post" id="message-'.$post['id'].'">
							<div class="row no-margin">
		    					<div class="col-lg-2">
									<h4>'.htmlspecialchars($post['nom']).' '.htmlspecialchars($post['prenom']).'</h4>
									<p>'.convertDate($post['creation_date'], $date_format).'</p>
								</div>
								<div class="col-lg-10 post-content">
									<h3>'.$content.'</h3>';
									if(!empty($post['image_link']))
									{
										echo '<img id="image-'.$post['id'].'" src="'.$post['image_link'].'">';
									}
									
								echo '<div class="row">
										<div class="post-footer">';
											if(!empty($post['modification_date']))
											{

											   echo '<div class="row no-margin">
											   			<div class="col-lg-6 block-date">
															<p>Dernière modification le : '.convertDate($post['modification_date'], $date_format).'</p>
														</div>
														<div class="col-lg-6 text-right block-edit">';
														/*
														* A utiliser en prod
														*/
														/*if($post['user_id'] == $user['user_id'])
														{
															echo '<button type="button" class="btn-delete btn-danger delete-link" id="delete-'.$post['id'].'">Supprimer</button>
																	<a href="#editPost" class="btn btn-default edit-link" id="post-'.$post['id'].'">Modifier</a>';
														}*/
															echo '<button type="button" class="btn-delete btn-danger delete-link" id="delete-'.$post['id'].'">Supprimer</button>
																	<a href="#editPost" class="btn btn-default edit-link" id="post-'.$post['id'].'">Modifier</a>';
														echo '</div>
													</div>';
											}
											else
											{
												echo '<div class="row no-margin">
														<div class="col-lg-offset-6 col-lg-6 text-right block-edit">';
														/*
														* A utiliser en prod
														*/
														/*if($post['user_id'] == $user['user_id'])
														{
															echo '<button type="button" class="btn-delete btn-danger delete-link" id="delete-'.$post['id'].'">Supprimer</button>
																	<a href="#editPost" class="btn btn-default edit-link" id="post-'.$post['id'].'">Modifier</a>';
														}*/
															echo '<button type="button" class="btn-delete btn-danger delete-link" id="delete-'.$post['id'].'">Supprimer</button>
																	<a href="#editPost" class="btn btn-default edit-link" id="post-'.$post['id'].'">Modifier</a>';
													  	echo '</div>
													  </div>';
											}

						  	  	  echo '</div>
						  	  		</div>
						  		</div>
							</div>
						</li>';
				}

				echo '<li class="list-group-item col-lg-offset-1 col-lg-10 edit-post hidden" id="blockEditPost">
						<div class="row edit-post-content">
							<div class="col-lg-12">
								<h2>Modifier votre message</h2>
							</div>
		    				<div class="col-lg-12 ta-edit-post">
		    					<textarea class="form-control" id="editPost" name="editPost"></textarea>
		    				</div>
		    				<div class="col-lg-12">
		    					<div class="ta-edit-link">
		    						<img src="" id="editPostImage" name="editPostImage">
		    						<p class="hidden" id="editImageTemp">L\'image sera supprimée définitivement.</p>
		    						<button type="button" class="btn-delete btn-danger delete-image hidden" id="btnDelete"><i class="material-icons">delete_forever</i>
		    					</div>
		    				</div>
		    				<div class="col-lg-12 text-right">
		    					<a href="#" class="btn btn-default" data-img="false" id="editedPost" name="editedPost">Enregistrer</a>
		    				</div>
		    			</div>
		    		  </li>
		    		</ol>
					<div class="row">
						<div class="col-lg-offset-10 col-lg-2 text-right">
							<a href="#addPost" class="btn btn-default add_post" data-toggle="modal" data-target="#addPost">Répondre</a>
						</div>
					</div>';
			}

			else
			{
				echo '<h1>404 Page inexistante</h1>';
			}
	?>
	</ol>
</div>

<!--<div class="col-lg-offset-10 col-lg-2 text-right">
	<a href="#addPost" class="addround add_post" data-toggle="modal" data-target="#addPost">+</a>
</div>-->

<div class="modal fade" id="addTopic" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
		    <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Créer un sujet</h4>
		    </div>	         	    
			<form class="form-horizontal" id="topicForm" method="post" autocomplete="off">
			    <div class="modal-body">
					<div class="form-group">
						<label class="control-label col-sm-2" for="topicTitle">Titre :</label>
						<div class="col-sm-10">          
							<input class="form-control" id="topicTitle" placeholder="Titre du sujet" name="topicTitle">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="topicFirstPost">Message :</label>
						<div class="col-sm-10">          
							<textarea class="form-control ta-topic" id="topicFirstPost" placeholder="Message" name="topicFirstPost"></textarea>
						</div>
					</div>
					<div class="form-group">        
						<div class="col-sm-offset-2 col-sm-10">
						    <div class="checkbox">
						        <label><input type="checkbox" id="getNotif" name="getNotif">Recevoir des notifications</label>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" name="cat" id="cat" value="<?php echo $_GET['category']; ?>">
				<div class="modal-footer">
			        <button type="submit" class="btn btn-default">Enregistrer</button>
			    </div>
			</form>
		</div>
	</div>     
</div>

<div class="modal fade" id="addCategory" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
		    <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Créer une catégorie</h4>
		    </div>	         	    
			<form class="form-horizontal" id="categoryForm" method="post" autocomplete="off">
			    <div class="modal-body">
					<div class="form-group">
						<label class="control-label col-sm-2" for="categoryTitle">Nom :</label>
						<div class="col-sm-10">          
							<input class="form-control" id="categoryTitle" placeholder="Nom de catégorie" name="categoryTitle">
						</div>
					</div>
				</div>
				<div class="modal-footer">
			        <button type="submit" class="btn btn-default">Enregistrer</button>
			    </div>
			</form>
		</div>
	</div>     
</div>

<div class="modal fade" id="addPost" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
		    <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Répondre au sujet</h4>
		    </div>	         	    
			<form class="form-horizontal" id="postForm" method="post" autocomplete="off">
			    <div class="modal-body">
			    	<div class='form-group'>
						<label class="control-label col-sm-2" for="postImg">Image :</label>
						<div class='uploadBlock col-sm-10' id="postImg">
						    <div class="col-sm-12 upload-input">
							    <?php include('modules/upload/upload.php'); ?>
							    <div class='param'>
							        <input type='hidden' class='width' value='1920' />
							        <input type='hidden' class='height' value='1080' />
							        <input type="hidden" id="resizable" value="false" >
							         <input type="hidden" id="ajax" value="false" >
							    </div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="topicPost">Message :</label>
						<div class="col-sm-10">          
							<textarea class="form-control ta-post" id="topicPost" placeholder="Message" name="topicPost"></textarea>
						</div>
					</div>
					<div class="form-group i">        
						<div class="col-sm-offset-2 col-sm-10">
						    <div class="checkbox">
						        <label><input type="checkbox" id="getNotifPost" name="getNotifPost">Recevoir des notifications</label>
							</div>
						</div>
					</div>
				</div>
				<div class="loading">
					<img class ="img-loading" src="modules/suiviClient/images/loading.gif"> 
				</div>
				<input type="hidden" name="postTopic" id="postTopic" value="<?php echo $_GET['topic']; ?>">
				<div class="modal-footer">
			        <button type="submit" class="btn btn-default">Enregistrer</button>
			    </div>
			</form>
		</div>
	</div>     
</div>
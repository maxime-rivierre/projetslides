<?php
    //session_start();
	ini_set('display_errors',1); 
	error_reporting(E_ALL);
	require_once 'conf.php';

	/*
	*	PHP Mailer - a modifier si déja présent sur site
	*/
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	function connectApi($clientId, $clientSecret, $link_token)
	{
        $fp = fopen(dirname(__FILE__).'/errorlog.txt', 'w');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $link_token);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$clientSecret);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_STDERR, $fp);

        $result = curl_exec($ch);

		if(empty($result))
		{
			die("Erreur : pas de réponse");
		}
		else
		{
		    $json = json_decode($result);
		    return $json;
		}
		curl_close($ch);
	}

	function applyTax($p_prix, $p_taxe)
	{
		$multiplier = 1 + ($p_taxe / 100);
		return $p_prix * $multiplier;
	}

	function calculPrix($p_sous_total, $p_transport, $p_frais_gestion, $p_remise_transport, $p_assurance)
	{
		$final = $p_sous_total + $p_transport + $p_frais_gestion - $p_remise_transport + $p_assurance;
		return $final;
	}

	function setPayment($token, $link_payment, $var)
	{
		$authorization = "Authorization: Bearer ".$token;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $link_payment);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $var);

		$result = curl_exec($ch);
		return json_decode($result);

		curl_close($ch);
	} 

	function executePayment($token, $link, $payerId)
	{
		$past = '{ "payer_id": "'.$payerId.'"}';
		$authorization = "Authorization: Bearer ".$token;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $link);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $past);

		$result = curl_exec($ch);
		return json_decode($result);

		curl_close($ch);
	}

	function getRefund($token, $link)
	{
		$header = Array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$token,
        );

        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, '{}');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
       	
       	$result = curl_exec($ch);
        return json_decode($result);

        curl_close($ch);
	}

	function sendMail($mail_to_buyer, $name_to_buyer, $content, $debug, $host_smtp, $port, $protocole, $mail_username_smtp, $mail_password_smtp, $mail_sender, $name_sender, $mail_reply, $name_reply, $mail_subject)
    {
		$mail = new PHPMailer;
		$mail->CharSet = 'UTF-8';
		$mail->Encoding = 'base64';
		$mail->isSMTP();
		// 0 = off
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = $debug;
		$mail->Debugoutput = 'html';
		$mail->Host = $host_smtp;
		$mail->Port = $port;
		$mail->SMTPSecure = $protocole;
		$mail->SMTPAuth = true;
		$mail->Username = $mail_username_smtp;
	    $mail->Password = $mail_password_smtp;
		// Envoyeur
		$mail->setFrom($mail_sender, $name_sender);
		// Adresse réponse
		$mail->addReplyTo($mail_reply, $name_reply);
		// Destinataires
		$mail->addAddress($mail_to_buyer, $name_to_buyer);
		//$mail->addAddress('francois.petit@1984.agency', 'François Petit');
		$mail->Subject = $mail_subject;
		$mail->Body = $content;
		$mail->AltBody = "Une erreur s'est produite pendant l'envoi du mail";

		if (!$mail->send()) 
		{
			echo '<div class="container"
					<p>Erreur :"'.$mail->ErrorInfo.'"<p>
				</div>';
		} 
	}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Paiement</title>
</head>
<body>
	<?php
        $connect = connectApi($client_id, $client_secret, $link_token);

		// Token d'itentification nécessaire pour créer un paiement
		$token = $connect->access_token;
		$mail = "";

		// Si un PayerID est retourné, le client a bien validé le paiement
		if(isset($_GET['PayerID']))
		{
			// Numéro de paiement
			$paymentId = $_GET['paymentId'];

			// Identifiant du client pour la transaction
			$payerId = $_GET['PayerID'];


			$link_execute = $link_base.$paymentId."/execute";

			$details = executePayment($token, $link_execute, $payerId);
			//var_dump($details);
			
			$state = $details->state;

			// Si l'état est approved, le paiement a bien été validé et effectué
			if($state == "approved")
			{
				$num_paiement = $details->id;
				$num_commande = $details->cart;

				$state = $details->state;
				$infos = $details->payer;
				$client = $infos->payer_info;

				$email = $client->email;
				$prenom = $client->first_name;
				$nom = $client->last_name;
				$id_client = $client->payer_id;

				$livraison = $client->shipping_address;
				$receveur = $livraison->recipient_name;
				$adresse1 = $livraison->line1;
				//$adresse2 = $livraison->line2;
				$ville = $livraison->city;
				$code_postal = $livraison->postal_code;
				$pays = $livraison->country_code;

				$transaction = $details->transactions;
				$commande = $transaction[0];
				$infos_prix = $commande->amount;
				$total = $infos_prix->total;
				$monnaie = $infos_prix->currency;
				$details_infos = $infos_prix->details;
				$sous_total = $details_infos->subtotal;
				$taxe = $details_infos->tax;
				$transport = $details_infos->shipping;
				$frais_gestion = $details_infos->handling_fee;
				$remise_transport = $details_infos->shipping_discount;
				$assurance = $details_infos->insurance;

				$items_list = $commande->item_list;
				$item = $items_list->items;
				$infos_item = $item[0];
				$produit = $infos_item->name;
				$description_produit = $infos_item->description;
				$quantite = $infos_item->quantity;
				$prix = $infos_item->price;
				$sous_total = $prix * $quantite;

				$ressources = $commande->related_resources;
				$vente = $ressources[0]->sale;
				$id_vente = $vente->id;

				$links = $vente->links;
				$refund = $links[1]->href;
				$encoded = urlencode($refund);

				echo 
				'<div class="container">
					<h1>Paiement effectué avec succès</h1>
					<p>Numéro de paiement : '.$num_paiement.'</p>
					<p>Numéro de commande : '.$num_commande.'</p>
					<p>Numéro de la vente : '.$id_vente.'</p>
					<p>Infos du compte :</p>
					<ul>
						<li>Numéro client : '.$id_client.'</li>
						<li>Prénom : '.$prenom.'</li>
						<li>Nom : '.$nom.'</li>
						<li>Mail du compte : '.$email.'</li>
					</ul>
					<p>Infos de livraison :</p>
					<ul>
						<li>Nom : '.$receveur.'</li>
						<li>Mail : '.$_SESSION['mail'].'</li>
						<li>Adresse : '.$adresse1.'</li>
						<li>Ville : '.$ville.'</li>
						<li>Code postal : '.$code_postal.'</li>
						<li>Pays : '.$pays.'</li>
					</ul>
					<p>Informations de la commande<p>
						<ul>
							<li>Nom du produit : '.$produit.'</li>
							<li>Description : '.$description_produit.'</li>
							<li>Quantité : '.$quantite.'</li>
							<li>Prix unitaire : '.$prix.' '.$monnaie.'</li>
							<li>Total hors frais : '.$sous_total.' '.$monnaie.'</li>
							<p>Détails des frais</p>
							<ul>
								<li>Taxe : '.$taxe.' '.$monnaie.'</li>
								<li>Transport : '.$transport.' '.$monnaie.'</li>
								<li>Frais de gestion : '.$frais_gestion.' '.$monnaie.'</li>
								<li>Remise transport : - '.$remise_transport.' '.$monnaie.'</li>
								<li>Assurance : '.$assurance.' '.$monnaie.'</li>
							</ul>
							<li>Total : '.$total.' '.$monnaie.'</li>
						</ul>
					<a href ="index.php" class="btn btn-info">Accueil</a>';
					if($link_refund == true)
					{
						echo '<a href ="index.php?refund='.$encoded.'" class="btn btn-info">Demander un remboursement</a>';
					}
			echo '<a href ="index.php?mail=send" target="_blank" class="btn btn-info">Envoyer un mail récapitulatif</a>
				</div>';

				$recap = '<div class="container">
							<h1>Récapitulatif de la commande</h1>
							<p>Numéro de paiement : '.$num_paiement.'</p>
							<p>Numéro de commande : '.$num_commande.'</p>
							<p>Numéro de la vente : '.$id_vente.'</p>
							<p>Infos du compte :</p>
							<ul>
								<li>Numéro client : '.$id_client.'</li>
								<li>Prénom : '.$prenom.'</li>
								<li>Nom : '.$nom.'</li>
								<li>Mail du compte : '.$email.'</li>
							</ul>
							<p>Infos de livraison :</p>
							<ul>
								<li>Nom : '.$receveur.'</li>
								<li>Mail : '.$_SESSION['mail'].'</li>
								<li>Adresse : '.$adresse1.'</li>
								<li>Ville : '.$ville.'</li>
								<li>Code postal : '.$code_postal.'</li>
								<li>Pays : '.$pays.'</li>
								<li>Numéro de téléphone : '.$_SESSION['tel'].'</li>
							</ul>
							<p>Informations de la commande<p>
							<ul>
								<li>Nom du produit : '.$produit.'</li>
								<li>Description : '.$description_produit.'</li>
								<li>Quantité : '.$quantite.'</li>
								<li>Prix unitaire : '.$prix.' '.$monnaie.'</li>
								<li>Total hors frais : '.$sous_total.' '.$monnaie.'</li>
								<p>Détails des frais</p>
								<ul>
									<li>Taxe : '.$taxe.' '.$monnaie.'</li>
									<li>Transport : '.$transport.' '.$monnaie.'</li>
									<li>Frais de gestion : '.$frais_gestion.' '.$monnaie.'</li>
									<li>Remise transport : - '.$remise_transport.' '.$monnaie.'</li>
									<li>Assurance : '.$assurance.' '.$monnaie.'</li>
								</ul>
								<li>Total : '.$total.' '.$monnaie.'</li>
							</ul>
						</div>';

				$_SESSION['infos'] = $recap;
			}

			// Si l'éta est a failed, le paiement a été approuvé mais n'a pas été executé
			elseif($state == "failed") 
			{
				echo 
				"<div class='container'>
					<h1>Echec lors du paiement</h1>
					<p>Une erreur est survenue pendant l'authentification du paiement</p>
					<a href ='index.php' class='btn btn-info'>Accueil</a>
				</div>";
			}

			else
			{
				echo "Une erreur inconnue s'est produite";
			}
			
		}

		else
		{
			// Si le formulaire est envoyé
			if(isset($_POST['submit']))
			{
				$name = $_POST['name'];
				$mail = $_POST['mail'];
				$_SESSION['mail'] = $mail;
				$_SESSION['name'] = $name;
				$adresse = $_POST['adresse'];
				$ville = $_POST['ville'];
				$pays = "FR";
				$code = $_POST['code'];
				$tel = $_POST['tel'];
				$_SESSION['tel'] = $tel;

				$temp = str_replace(',', '.', $_POST['quantite']);
				$temp_array = explode('.', $temp);
				$p_quantite = $temp_array[0];
				$p_prix = str_replace(',', '.', $_POST['prix']);
				
				$p_sous_total = $p_quantite * $p_prix;
				$prix_taxe = applyTax($p_sous_total, $p_taxe);
				$p_taxe_en_euro = $prix_taxe - $p_sous_total;

				$p_total = calculPrix($prix_taxe, $p_transport, $p_frais_gestion, $p_remise_transport, $p_assurance);

				$note = "Contacter nous pour en savoir plus";

				$var = '{
					  "intent": "sale",
					  "payer": {
					    "payment_method": "paypal"
					  },
					  "transactions": [{
					    "amount": {
					      "total": "'.$p_total.'",
					      "currency": "EUR",
					      "details": {
					        "subtotal": "'.$p_sous_total.'",
					        "tax": "'.$p_taxe_en_euro.'",
					        "shipping": "'.$p_transport.'",
					        "handling_fee": "'.$p_frais_gestion.'",
					        "shipping_discount": "'.$p_remise_transport.'",
					        "insurance": "'.$p_assurance.'"
					      }
					    },
					    "payment_options": {
					      "allowed_payment_method": "INSTANT_FUNDING_SOURCE"
					    },
					    "item_list": {
					      "items": [{
					        "name": "'.$p_produit.'",
					        "description": "'.$p_description_produit.'",
					        "quantity": "'.$p_quantite.'",
					        "price": "'.$p_prix.'",
					        "tax": "0.00",
					        "sku": "100",
					        "currency": "'.$p_monnaie.'"
					      }],
					      "shipping_address": {
					        "recipient_name": "'.$name.'",
					        "line1": "'.$adresse.'",
					        "city": "'.$ville.'",
					        "country_code": "'.$pays.'",
					        "postal_code": "'.$code.'",
					        "phone": "'.$tel.'"
					      }
					    }
					  }],
					  "note_to_payer": "'.$note.'",
					  "redirect_urls": {
					    "return_url": "https://app.1984.agency/projet/paypal/index.php",
					    "cancel_url": "https://app.1984.agency/projet/paypal/index.php"
					  }
					}';
				$expires = $connect->expires_in;
				$payment = setPayment($token, $link_payment, $var);
				$link_get = $payment->links[0];
				$link_redirect = $payment->links[1];
				$link_post = $payment->links[2];
				$id_payment = $payment->id;
				$state = $payment->state;

				echo '<div class="container">
					<h1>Récapitulatif de la commande</h1>
					<p>Informations du client :</p>
					<ul>
						<li>Nom : '.$name.'</li>
						<li>Mail : '.$mail.'</li>
						<li>Adresse : '.$adresse.'</li>
						<li>Ville : '.$ville.'</li>
						<li>Code postal : '.$code.'</li>
						<li>Pays : '.$pays.'</li>
						<li>Numéro de téléphone : '.$tel.'</li>
					</ul>
					<p>Informations de la commande<p>
					<ul>
						<li>Nom du produit : '.$p_produit.'</li>
						<li>Description : '.$p_description_produit.'</li>
						<li>Quantité : '.$p_quantite.'</li>
						<li>Prix unitaire : '.$p_prix.' '.$p_monnaie.'</li>
						<li>Total hors frais : '.$p_sous_total.' '.$p_monnaie.'</li>
						<p>Détails des frais</p>
						<ul>
							<li>Taxe : '.$p_taxe_en_euro.' '.$p_monnaie.'</li>
							<li>Transport : '.$p_transport.' '.$p_monnaie.'</li>
							<li>Frais de gestion : '.$p_frais_gestion.' '.$p_monnaie.'</li>
							<li>Remise transport : - '.$p_remise_transport.' '.$p_monnaie.'</li>
							<li>Assurance : '.$p_assurance.' '.$p_monnaie.'</li>
						</ul>
						<li>Total : '.$p_total.' '.$p_monnaie.'</li>
					</ul>
					<h2>Voulez vous vraiment valider le paiement ?</h2>
					<a href="index.php" class="btn btn-secondary">Annuler</a>
					<a href="'.$link_redirect->href.'" class="btn btn-primary">Valider</a>
				</div>';
			}

			// Si il n'y a aucun paramètre GET, alors on affiche le formulaire
			elseif(isset($_GET['page']))
			{
				?>
	<div class="container">
		<form method="post" action="index.php" id="commande">
		  	<div class="form-group row">
		    	<label for="name" class="col-sm-2 col-form-label">Nom : </label>
		    	<div class="col-sm-10">
		      		<input type="text" class="form-control" id="name" name="name" placeholder="Nom">
		    	</div>
		  	</div>
		  	<div class="form-group row">
		    	<label for="mail" class="col-sm-2 col-form-label">Mail : </label>
		    	<div class="col-sm-10">
		      		<input type="mail" class="form-control" id="mail" name="mail" placeholder="Adresse mail">
		    	</div>
		  	</div>
		  	<div class="form-group row">
		    	<label for="adresse" class="col-sm-2 col-form-label">Adresse :</label>
		    	<div class="col-sm-10">
		      		<input type="text" class="form-control" id="adresse" name="adresse" placeholder="Adresse N° et Rue">
		    	</div>
		  	</div>
		  	<div class="form-group row">
		    	<label for="ville" class="col-sm-2 col-form-label">Ville :</label>
		    	<div class="col-sm-10">
		      		<input type="text" class="form-control" id="ville" name="ville" placeholder="Ville">
		    	</div>
		  	</div>
		  	<div class="form-group row">
		    	<label for="code" class="col-sm-2 col-form-label">Code postal :</label>
		    	<div class="col-sm-10">
		      		<input type="text" class="form-control" id="code" name="code" placeholder="Code postal">
		    	</div>
		  	</div>
		  	<div class="form-group row">
		    	<label for="tel" class="col-sm-2 col-form-label">Téléphone :</label>
		    	<div class="col-sm-10">
		      		<input type="tel" class="form-control" id="tel" name="tel" placeholder="Numéro de téléphone">
		    	</div>
		  	</div>
		  	<div class="form-group row">
		    	<label for="quantite" class="col-sm-2 col-form-label">Quantité : </label>
		    	<div class="col-sm-10">
		      		<input type="text" class="form-control" id="quantite" name="quantite" placeholder="Quantité">
		    	</div>
		    </div>
		  	<div class="form-group row">
		    	<label for="prix" class="col-sm-2 col-form-label">Prix : </label>
		    	<div class="col-sm-10">
		      		<input type="text" class="form-control" id="prix" name="prix" placeholder="Prix">
		    	</div>
		  	</div>
		  	<div class="form-group row">
		    	<div class="col-sm-12">
		      		<button type="submit" class="btn btn-primary float-right" name="submit">Valider</button>
		    	</div>
		  	</div>
		</form>
	</div>
		<?php	}

			// Si le lien de remboursement a été choisi
			elseif(isset($_GET['refund']))
			{
				$lien_refund = $_GET['refund'];
				$res = getRefund($token, $lien_refund);
				//var_dump($res);

				$num_remboursement = $res->id;
				$num_vente = $res->sale_id;
				$etat = $res->state;
				$montant = $res->amount;
				$total = $montant->total;
				$monnaie = $montant->currency;

				$frais = $res->refund_from_transaction_fee;
				$frais_vendeur = $frais->value;
				$monnaie_vendeur = $frais->currency;

				$name = $res->name;

				// Si l'état est completed, le remboursement a bien eu lieu
				if($etat == "completed")
				{
					echo '<div class="container">
							<h1>Le remboursement a bien été effectué, veuillez vérifier votre solde PayPal</h1>
							<p>Numéro de remboursement : '.$num_remboursement.'</p>
							<p>Numéro de vente : '.$num_vente.'</p>
							<p>Montant remboursé : '.$total.$monnaie.'</p>
							<p>Frais a charge du vendeur : '.$frais_vendeur.$monnaie_vendeur.'</p>
							<a href ="index.php" class="btn btn-info">Accueil</a>
						</div>';
				}

				// Sinon,
				else
				{	
					// Le remboursement a déja eu lieu
					if($name == "TRANSACTION_ALREADY_REFUNDED")
					{
						echo '<div class="container">
						  		<h1>Le remboursement a déja été effectué, veuillez vérifier votre solde PayPal</h1>
						  		<a href ="index.php" class="btn btn-info">Accueil</a>
						  	</div>';
					}
				}
			}

			// Si l'utilisateur choisi l'envoi de mail
			elseif(isset($_GET['mail'])) 
			{
				$content = "<p>Test envoi<p>";
				$mail_to = $_SESSION['mail'];
				$name_to = $_SESSION['name'];
				$infos = $_SESSION['infos'];
				sendMail($mail_to, $name_to, $infos, $debug, $host_smtp, $port, $protocole, $mail_username_smtp, $mail_password_smtp, $mail_sender, $name_sender, $mail_reply, $name_reply, $mail_subject);

				echo '<div class="container">
						<h1>Le mail a bien été envoyé</h1>
						<a href ="index.php" class="btn btn-info">Accueil</a>
					</div>';
			}
		}
	?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
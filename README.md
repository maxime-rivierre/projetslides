# Installation du projet

- ## Installation de l'environnement
  - Télecharger WAMP et l'installer sur le poste : https://sourceforge.net/projects/wampserver/files/WampServer%203/WampServer%203.0.0/wampserver3.1.7_x64.exe/download
  - Configurer WAMP en PHP 7.1.9 et MySQL 5.7.19
 
- ## Récupération des sources

    - Créer un dossier projet_slides dans le dossier www de WAMP
    
    - ### Installation manuelle
    
        - Récuperer l'archive contenant les sources : 
        - Extraire le contenu dans le dossier projet_slides
        
    - ### Installation avec Git
    
        - Lancer Git Bash dans le dossier projet_slides
        - Cloner le dossier : ```git clone https://gitlab.com/maxime-rivierre/projetslides.git```
        
    - ### Mise en place de la base de données
        
        - Se connecter a phpmyadmin
        - Créer une nouvelle base de données
        - Importer et éxécuter script.sql dans cette base
        
- ## Configuration du projet
    
    - Ouvrir le fichier conf.php a la racine du dossier projet_slides
    - Modifier les trois variables avec les infos de connexion a phpmyadmin
    
    | **Variable** | **Valeurs par défaut**|
    |----------|------|
    |base de données | projet|
    |utilisateur | root|
    |mot de passe | |

- ## Accès au site

    - Le site est accessible par défaut à l'adresse http://localhost/projet_slides/index.php ou http://127.0.0.1/projet_slides/index.php
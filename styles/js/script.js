$(document).ready(function () {

    //affiche le shadow au scroll
    $(window).scroll(function () {
        if ($(window).scrollTop() > 0) {
            $('#head-nav').addClass('shadow');
        } else {
            $('#head-nav').removeClass('shadow');
        }

    });
    //Vincent MARY
    $("#selectCompany").change(function(){
        var agenceSelected = $('#selectCompany option:selected').attr("value");
        //console.log(agenceSelected.attr("value"));
        $('.cl-mcont').load("pages/creation-article.php?agence="+ agenceSelected);
    });
    //open input search agency
    $('.openSearch').on('click', function () {
        if ($(window).width() < 768) {
            $(this).parent().addClass('showSearch');
            $(this).parent().find('input').focus();
        }
    });

    $('.searchBar').focusout(function () {
        $('.iconInput').removeClass('showSearch');
        $(this).val('');
    })
    /* if(('#search-agence').focusout() && $('.iconInput').hasClass('showSearch')){
          $('.iconInput').removeClass('showSearch');
    } */

    if ($('#message').length > 0) {

        $('#message-content').html($('#message').val());

        if ($('#message').hasClass('error')) {
            $('#message-icone').removeClass('fa-check');
            $('#message-affichage').removeClass('alert-success');

            $('#message-icone').addClass('fa-times-circle');
            $('#message-affichage').addClass('alert-danger');
        }

        $('#message-affichage').fadeIn();
        $('#message-affichage').delay(6000).fadeOut();

    }
    $('#open-description[data-toggle="popoverYoutube"]').popover({
        html: true,
        trigger: 'hover',
        content: function () { return '<img src="' + $(this).data('img') + '" />'; }
    });

    $('.datepicker').datetimepicker({
        format: "dd/mm/yyyy",
        locale: 'fr',
        language: 'fr',
        minView: 2
    });

    $('.timepicker').datetimepicker({
        format: "hh:ii:ss",
        locale: 'fr',
        language: 'fr'
    });

    $('.open-nav-tab').click(function () {
        $('.nav-tabs').slideToggle();
    });

    $('.notif-check').click(function () {
        $.ajax({
            url: "/ajax/ajax-notif.php",
            // Passage des donnÃ©es au fichier ajax.php
            data: { notif_check: 'true' },
            cache: false,
            error: function (request, error) { // Info Debuggage si erreur
                alert("Erreur : responseText: " + request.responseText);
            },
            success: function (data) {
                $('.notif-check .bubble').fadeOut();
            }
        });
    });

    $('.openBlockSwitchUsers').click(function () {
        $('.blockSwitchUsers').toggleClass('open');
    });
    if ($('.blockSwitchUsers').length > 0) {
        $(".blockSwitchUsers select").select2({
            width: '100%'
        });
    }

    // Ouverture du nav
    $(document).on('click', '.openNav', function () {
        $(this).toggleClass('toggle');
        $('.navRight').toggleClass('open');
    });

    // Nav qui se ferme avec click en dehors
    $(document).on('click', function (event) {
        if ($('.navRight.open').length > 0 && !$(event.target).hasClass('open') && !$(event.target).parent().hasClass('openNav') && $(event.target).parents(".navRight.open").length < 1) {
            $('.openNav').toggleClass('toggle');
            $('.navRight').toggleClass('open');
        } else {
            return true;
        }
    });

    $(document).on('click', '.stepLink', function () {
        var valid = true;
        if ($(this).hasClass('checkInput')) {
            var parent = $(this).parent();
            var inputs = parent.find('input,select,textarea');
            inputs.each(function () {
                if (!$(this).hasClass('select2-input')) {
                    if ($(this).val() === null || $(this).val().length < 1) {
                        valid = false;
                        if (parent.find('.warning').length < 1) parent.append('<span class="warning">Champ(s) invalide(s)</span>');
                        $(this).addClass('error');
                    }
                }
            });
        }
        if (valid) {
            var step = $(this).attr('step-target');
            $('.stepBlock.active').removeClass('active');
            $('.stepBlock[step=' + step + ']').toggleClass('active');
        }
    });

    $(document).on('keyup', 'input.error', function () {
        $(this).removeClass('error');
        $('.warning').fadeOut(function () {
            $(this).remove();
        });
    });

    //afficher au chargement de la page le nb de char restant
    $('.count_char').each(function () {
        var maxlength = $(this).attr('maxlength');
        var length = $(this).val().length
        $(this).parent().find('.show_char').text(maxlength - length);
    })

    $('.count_char').keyup(function () {
        var maxlength = $(this).attr('maxlength');
        var length = $(this).val().length;
        length = maxlength - length;
        $('.show_char').text(length);
    });

    //Ouvre le dropdown si le sous menu a la class isactive
    if ($('.sub-menu li a.isactive').length > 0 && $(window).width() > 979 || $(window).width() < 768) {
        var elem = $('.sub-menu li a.isactive');
        elem.parents('.parent').addClass('open');
        elem.parents('.sub-menu').css('display', 'block');
    }

    $(document).on('change','.changeAccount',function(){
        $('#accountSwap .valueSwap').val($(this).val())
        $('#accountSwap').submit();
    });

    //Recpatcha
    var formStart = 0;
    $('#connexion').submit(function (e) {

        formStart++;

        e.preventDefault();

        if (formStart == 1) {
            var widgetId = grecaptcha.render('recaptcha_start', {
                'sitekey': '6LcqXpcUAAAAADuRd-kTG_zeq6qT9XYENK0zoFwZ',
                'callback': onSubmitFormStart,
                'size': "invisible",
                'data-badge': 'bottomleft'
            });
        }

        grecaptcha.reset(widgetId);

        grecaptcha.execute(widgetId);

    });

});

function onSubmitFormStart(token) {
    document.getElementById("connexion").submit();
}

var hamburger = document.querySelector(".hamburger--elastic");
if (hamburger != null) {
    hamburger.addEventListener("click", function () {
        hamburger.classList.toggle("is-active");
    });
}

//autoresize select
(function($, window){
    var marginRight = 5;

    $.fn.resizeSelect = function(settings) {
        return this.each(function() {

            $(this).change(function(){
                var $this = $(this);

                // create test element
                var text = $this.find("option:selected").text();

                var $test = $("<span>").html(text).css({
                    "font-size": $this.css("font-size"), // ensures same size text
                    "visibility": "hidden" 							 // prevents FOUC
                });


                // add to body, get width, and get out
                $test.appendTo($this.parent());
                var width = $test.width();
                $test.remove();

                // set select width
                $this.width(width + marginRight);

                // run on start
            }).change();

        });
    };

    // run by default
    $(".resizeSelect").resizeSelect();

})(jQuery, window);
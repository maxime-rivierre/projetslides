<?php
    session_start();
    require_once 'conf.php';
    if(is_null($_GET['page']) || empty($_GET['page']))
    {
        header('Location: index.php?page=projectPresentation');
    }

    require_once 'modules/PHPMailer/Exception.php';
    require_once 'modules/PHPMailer/OAuth.php';
    require_once 'modules/PHPMailer/PHPMailer.php';
    require_once 'modules/PHPMailer/POP3.php';
    require_once 'modules/PHPMailer/SMTP.php';

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta property="og:url" content="http://app.1984.agency"/>
    <meta property="og:type" content=""/>
    <meta property="og:title" content="Multisite Manager"/>
    <meta property="og:description" content=""/>
    <meta property="og:image" content="https://app.1984.agency"/>
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png">
    <script src="https://use.fontawesome.com/6d1d954c25.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet'
    type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="icon" type="image/png" sizes="192x192" href="styles/ico.png">
    <link href="modules/croppie/croppie.css" rel="stylesheet" type="text/css">
    <?php
    $files_base = scandir('styles/css/');
    $files = scandir('modules/' . $_GET['page'] . '/css/');
    {
        foreach ($files_base as $file_base){
            if(!is_dir($file_base)){
                echo '<link href="styles/css/' . $file_base . '" rel="stylesheet"/>';
            }
        }

        foreach ($files as $file) {
            if (!is_dir($file)) {
                echo '<link href="modules/' . $_GET['page'] . '/css/' . $file . '" rel="stylesheet"/>';
            }
        }
    }
    ?>
    <link rel="stylesheet" href="modules/projectPresentation/css/style.css">
    <title>Application 1984</title>
</head>
<body>
<div id="cl-wrapper">
    <div class="cl-sidebar">
        <div class="cl-toggle hamburger--elastic">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </div>
        <div class="cl-navblock">
            <div class="menu-space">
                <div class="content">
                    <div class="sidebar-logo">
                        <a href="/">
                            <div class="logo">
                                <img alt="logo" src='styles/images/84.jpg'/>
                            </div>
                        </a>
                    </div>

                    <ul class="cl-vnavigation">
                        <li class="">
                            <a href='index.php?page=projectPresentation'>
                                <i class='material-icons'>slideshow</i>
                                <span class='text_menu'>Présentation projet</span>
                            </a>
                        </li>
                        <li class="">
                            <a href='index.php?page=forum'>
                                <i class='material-icons '>forum</i>
                                <span class='text_menu'>Forum</span>
                            </a>
                        </li>
                        <li class="">
                            <a href='index.php?page=checkSites'>
                                <i class='material-icons '>check</i>
                                <span class='text_menu'>Vérification des sites</span>
                            </a>
                        </li>
                        <li class="">
                            <a href='index.php?page=paiement'>
                                <i class='material-icons '>payment</i>
                                <span class='text_menu'>Paiement</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="pcont" class="container-fluid">
        <div id="head-nav" class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-collapse">
                    <ul class="nav navbar-nav not-nav">
                        <?php
                        if (isset($_GET['page'])) {
                            require_once 'modules/' . $_GET['page'] . '/navbar/nav.php';
                        } else {
                            echo "Une erreur est survenue.";
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="cl-mcont ">
            <?php
            if (isset($_GET['page'])) {
                require_once 'modules/' . $_GET['page'] . '/index.php';
            } else {
                echo "Une erreur est survenue.";
            }
            ?>
        </div>
    </div>
</div>
<script src="styles/js/jquery.js"></script>
<script src="styles/js/cookie.js"></script>
<script src="styles/js/PushMenu.js"></script>
<script src="styles/js/nanoscroller.js"></script>
<script src="styles/js/sparkline.js"></script>
<script src="styles/js/jquery-ui.js"></script>
<script src="styles/js/jquery-gritter.js"></script>
<script src="styles/js/core.js"></script>
<script src="styles/js/parsley.js"></script>
<script src="styles/js/bootstrap.js"></script>
<script src="styles/js/codemirror.js"></script>
<script src="styles/js/jvector-min.js"></script>
<script src="styles/js/jvector-world.js"></script>
<script src="styles/js/jquery-tablesorter.js"></script>
<script src="styles/js/bootstrap-datetimepicker.js"></script>
<script src="styles/js/bootstrap-datetimepickerfr.js"></script>
<script src="styles/js/fixed-headertable.js"></script>
<script src="styles/js/bootstrap-switch.js"></script>
<script src="styles/js/select2.js"></script>
<script src="modules/croppie/croppie.js">git </script>
<?php
$files = scandir('modules/' . $_GET['page'] . '/js/');
{
    foreach ($files as $file) {
        if (!is_dir($file)) {
            echo '<script src="modules/' . $_GET['page'] . '/js/' . $file . '"></script>';
        }
    }
}
?>
<script src="styles/js/touch-punch.js"></script>
<script src="styles/js/script.js"></script>
<script>
    $(document).ready(function() {
        function getParam(param) {
            let vars = {};
            window.location.href.replace(location.hash, '').replace(
                /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
                function (m, key, value) { // callback
                    vars[key] = value !== undefined ? value : '';
                }
            );

            if (param) {
                return vars[param] ? vars[param] : null;
            }
            return vars;
        }

        let param = getParam(), projet = param['page'];
    });
</script>
</body>
</html>
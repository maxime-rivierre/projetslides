<?php
include_once '../modules/projectPresentation/includes/main.php';
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="assets/css/paral.css"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/images/icon.ico">
    <style>html, body {
            font-family: 'Montserrat', sans-serif;
        }</style>
    <title><?php
        $url = $_GET["parallax"];
        $data = queryDb('SELECT nom FROM projet WHERE url ="' . $url . '"');
        $nom = $data[0]['nom'];
        echo $nom; ?></title>
</head>

<body>
<div style="display:block" id="hideAll">
    <div id="screnn">
        <img id="logo" src="assets/images/logo-1984-hd.jpg"/>
        <div class='progress' id="progress_div">
            <div class='bar' id='bar1'>
            </div>
            <div class='percent' id='percent1'></div>
            <input type="hidden" id="progress_width" value="0">
        </div>
    </div>
</div>

<?php
$nom_projet = $_GET['parallax'];
$id_projet = getProjectId($nom_projet);

$data_intro = queryDb('SELECT * FROM slide,type_slide WHERE slide.type_slide = type_slide.id AND type_slide.nom = "Introduction" AND slide.num_projet =' . $id_projet);

foreach ($data_intro as $row_intro) {
    $num_intro = $row_intro["numero"];
    $lien_image_intro = $row_intro["lien_image"];
    $texte_intro = $row_intro["titre"];
    $date = $row_intro["date_creation"];
    $font = $row_intro["couleur_texte"];
    $gras = $row_intro["gras"];

    $val = explode('-', $date);
    $date_intro = $val[2] . "-" . $val[1] . '-' . $val[0];

    if (!empty($lien_image_intro)) {
        echo '<div class="block" id=' . $num_intro . '>
							<img src="https://app.1984.agency/modules/projectPresentation/' . $lien_image_intro . '" data-speed="0.75" class="img-parallax">
							<div class="block-logo">
								<img class="img-logo" src="https://app.1984.agency/modules/projectPresentation/images/logo-transparent.png">
							</div>
							<div class="contenu">
								<div class="title">';
        if ($gras == 1) {
            echo '<h3 style="color:' . $font . ';font-size:3vw;"><strong>' . $texte_intro . '</strong></h3>';
        } else {
            echo '<h3 style="color:' . $font . ';font-size:3vw;">' . $texte_intro . '</h3>';
        }

        echo '</div>
							</div>	
							<div class="contenu-intro">
								<div class="texte">
									<h3 style="color:' . $font . '">' . $date_intro . '</h3>
								</div>
							</div>
						</div>';
    } else {
        echo '<div style="background-color:black" class="block" id=' . $num_intro . '>
							<div class="block-logo">
								<img class="img-logo" src="https://app.1984.agency/modules/projectPresentation/images/logo-transparent.png" data-speed="0.75">
							</div>
							<div class="contenu">
								<div class="title">';
        if ($gras == 1) {
            echo '<h3 style="color:' . $font . ';font-size:3vw;"><strong>' . $texte_intro . '</strong></h3>';
        } else {
            echo '<h3 style="color:' . $font . ';font-size:3vw;">' . $texte_intro . '</h3>';
        }

        echo '</div>	
							</div>
							<div class="contenu-intro">		
								<div class="texte">
									<h3 style="color:' . $font . '">' . $date_intro . '</h3>
								</div>
							</div>
						</div>';
    }
}

$data = queryDb('SELECT type_slide.nom as type, slide.*
								FROM type_slide, slide 
								WHERE type_slide.id = slide.type_slide 
								AND num_projet = ' . $id_projet . ' 
								AND slide.type_slide NOT IN (
								    SELECT type_slide.id 
								    FROM type_slide 
								    WHERE type_slide.nom = "Introduction" 
								    OR type_slide.nom = "Remerciements" )
								ORDER BY ordre ASC');

foreach ($data as $row) {
    $numero = $row["numero"];
    $type_slide = $row["type"];
    $lien_image = $row["lien_image"];
    $couleur = $row["couleur"];
    $font = $row["couleur_texte"];
    $titre = $row["titre"];
    $texte = $row["texte"];
    $gras = $row["gras"];
    $lien_video = $row["lien_video"];
    $externe = $row["lien_externe"];
    $ordre = $row["ordre"];

    if (empty($font)) {
        $font = "#ffffff";
    }

    if ($type_slide == "Présentation") {
        // Affichage si image
        if (!empty($lien_image)) {
            if (!empty($titre) && $gras == 1) {
                if (!empty($texte)) {
                    // Image + titre gras + texte
                    echo '<div class="block" id=' . $numero . '>
										<img src="https://app.1984.agency/modules/projectPresentation/' . $lien_image . '" data-speed="0.75" class="img-parallax">
										<div class="contenu">
											<div class="title">
												<h3 style="color:' . $font . '"><strong>' . $titre . '</strong></h3>
											</div>			
											<div class="texte">
												<h3 style="color:' . $font . '">' . $texte . '</h3>
											</div>	
										</div>';
                    if (!empty($externe)) {
                        echo '<div class="externe">
													<a class="lien_externe" href="' . $externe . '" target="_blank"><i class="material-icons">add</i></a>
												</div>';
                    }
                    echo '</div>';
                } else {
                    // Image + titre gras
                    echo '<div class="block" id=' . $numero . '>
										<img src="https://app.1984.agency/modules/projectPresentation/' . $lien_image . '" data-speed="0.75" class="img-parallax">
										<div class="contenu">
											<div class="title">
												<h3 style="color:' . $font . '"><strong>' . $titre . '</strong></h3>
											</div>							
										</div>';
                    if (!empty($externe)) {
                        echo '<div class="externe">
													<a class="lien_externe" href="' . $externe . '" target="_blank"><i class="material-icons">add</i></a>
												</div>';
                    }
                    echo '</div>';
                }
            } elseif (!empty($titre) && $gras == 0) {
                if (!empty($texte)) {
                    // Image + titre + texte
                    echo '<div class="block" id=' . $numero . '>
										<img src="https://app.1984.agency/modules/projectPresentation/' . $lien_image . '" data-speed="0.75" class="img-parallax">
										<div class="contenu">
											<div class="title">
												<h3 style="color:' . $font . '">' . $titre . '</h3>
											</div>			
											<div class="texte">
												<h3 style="color:' . $font . '">' . $texte . '</h3>
											</div>
										</div>';
                    if (!empty($externe)) {
                        echo '<div class="externe">
													<a class="lien_externe" href="' . $externe . '" target="_blank"><i class="material-icons">add</i></a>
												</div>';
                    }
                    echo '</div>';
                } else {
                    // Image + titre
                    echo '<div class="block" id=' . $numero . '>
										<img src="https://app.1984.agency/modules/projectPresentation/' . $lien_image . '" data-speed="0.75" class="img-parallax">
										<div class="contenu">
											<div class="title">
												<h3 style="color:' . $font . '">' . $titre . '</h3>
											</div>
										</div>';
                    if (!empty($externe)) {
                        echo '<div class="externe">
													<a class="lien_externe" href="' . $externe . '" target="_blank"><i class="material-icons">add</i></a>
												</div>';
                    }
                    echo '</div>';
                }
            } else {
                // Image
                echo '<div class="block" id=' . $numero . '>
										<img src="https://app.1984.agency/modules/projectPresentation/' . $lien_image . '" data-speed="0.75" class="img-parallax">';
                if (!empty($externe)) {
                    echo '<div class="externe">
											<a class="lien_externe" href="' . $externe . '" target="_blank"><i class="material-icons">add</i></a>
										</div>';
                }
                echo '</div>';
            }
        } // Affichage sans image
        else {
            if (!empty($titre) && $gras == 1) {
                if (!empty($texte)) {
                    // Couleur + Titre gras + texte
                    echo '<div class="block" id=' . $numero . ' style="background-color:' . $couleur . '">
										<div class="contenu">
											<div class="title">
												<h3 style="color:' . $font . '"><strong>' . $titre . '</strong></h3>
											</div>			
											<div class="texte">
												<h3 style="color:' . $font . '">' . $texte . '</h3>
											</div>
										</div>';
                    if (!empty($externe)) {
                        echo '<div class="externe">
													<a class="lien_externe" href="' . $externe . '" target="_blank"><i class="material-icons">add</i></a>
												</div>';
                    }
                    echo '</div>';

                } else {
                    // Couleur + Titre gras
                    echo '<div class="block" id=' . $numero . ' style="background-color:' . $couleur . '">
										<div class="contenu">
											<div class="title">
												<h3 style="color:' . $font . '"><strong>' . $titre . '</strong></h3>
											</div>
										</div>';
                    if (!empty($externe)) {
                        echo '<div class="externe">
													<a class="lien_externe" href="' . $externe . '" target="_blank"><i class="material-icons">add</i></a>
												</div>';
                    }
                    echo '</div>';
                }
            } elseif (!empty($titre) && $gras == 0) {
                if (!empty($texte)) {
                    // Couleur + Titre + texte
                    echo '<div class="block" id=' . $numero . ' style="background-color:' . $couleur . '">
										<div class="contenu">
											<div class="title">
												<h3 style="color:' . $font . '">' . $titre . '</h3>
											</div>			
											<div class="texte">
												<h3 style="color:' . $font . '">' . $texte . '</h3>
											</div>
										</div>';
                    if (!empty($externe)) {
                        echo '<div class="externe">
													<a class="lien_externe" href="' . $externe . '" target="_blank"><i class="material-icons">add</i></a>
												</div>';
                    }
                    echo '</div>';
                } else {
                    // Couleur + Titre
                    echo '<div class="block" id=' . $numero . ' style="background-color:' . $couleur . '">
										<div class="contenu">
											<div class="title">
												<h3 style="color:' . $font . '">' . $titre . '</h3>
											</div>
										</div>';
                    if (!empty($externe)) {
                        echo '<div class="externe">
													<a class="lien_externe" href="' . $externe . '" target="_blank"><i class="material-icons">add</i></a>
												</div>';
                    }
                    echo '</div>';
                }
            } else {
                // Couleur
                echo '<div class="block" id=' . $numero . ' style="background-color:' . $couleur . '">';
                if (!empty($externe)) {
                    echo '<div class="externe">
										<a class="lien_externe" href="' . $externe . '" target="_blank"><i class="material-icons">add</i></a>
									</div>';
                }
                echo '</div>';
            }
        }
    } // Affichage si vidéo
    else {
        echo '<div class="block" id=' . $numero . ' style="background-color:#000">
							<iframe class = "slide-video" data-speed="0.75" width="100%" height="100%" src="' . $lien_video . '" frameBorder="0" allowfullscreen></iframe>';
        if (!empty($externe)) {
            echo '<div class="externe">
										<a class="lien_externe" href="' . $externe . '" target="_blank"><i class="material-icons">add</i></a>
									</div>';
        }
        echo '</div>';
    }
}

$data_ending = queryDb('SELECT * FROM slide,type_slide WHERE slide.type_slide = type_slide.id AND type_slide.nom = "Remerciements" AND slide.num_projet =' . $id_projet);

foreach ($data_ending as $row_ending) {
    $pnum_ending = $row_ending["numero"];
    $ptexte_ending = $row_ending["titre"];
    $pparticipants = $row_ending["texte"];
    $font = $row_ending["couleur_texte"];
    $gras = $row_ending["gras"];

    $list = explode(",", $pparticipants);

    echo '<div class="block" id=' . $pnum_ending . ' style="background-color: #000">
							<div class="contenu">
								<div class="title">';
    if ($gras == 1) {
        echo '<h3 style="color:' . $font . '"><strong>' . $ptexte_ending . '</strong></h3>';
    } else {
        echo '<h3 style="color:' . $font . '">' . $ptexte_ending . '</h3>';
    }

    echo '</div>
							</div>
							<div class="noms">
								<div class="texte">';
    foreach ($list as $participants) {
        echo '<h5 style="color:' . $font . '">' . $participants . '</h5>';
    }
    echo '</div>
							</div>
						</div>';
}

?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="assets/js/paral.js"></script>
<script type="text/javascript">
    $('.block').last().append('<div class="row-top"><a href="#" class="lien-top" title="Top"><i class="material-icons">keyboard_arrow_up</i></a></div>');

    $('.lien-top').click(function (event) {
        event.preventDefault();
        $("html, body").animate({scrollTop: 0}, 1500);
    });
</script>
</body>
</html>

if (document.images)(new Image()).src="https://app.1984.agency/projet/assets/images/logo-1984-hd.jpg";

$('.img-parallax').each(function(){
  var img = $(this);
  var blockParent = $(this).parent();

  function parallaxImg () {
    var speed = img.data('speed');
    var blockY = blockParent.offset().top;
    var winY = $(this).scrollTop();
    var winH = $(this).height();
    var parentH = blockParent.innerHeight();


    // The next pixel to show on screen      
    var winBottom = winY + winH;

    // If block is shown on screen
    if (winBottom > blockY && winY < blockY + parentH) {
      // Number of pixels shown after block appear
      var imgBottom = ((winBottom - blockY) * speed);
      // Max number of pixels until block disappear
      var imgTop = winH + parentH;
      // Porcentage between start showing until disappearing
      var blockPercent = ((imgBottom / imgTop) * 100) + (50 - (speed * 50));
    }
    img.css({
      top: blockPercent + '%',
      transform: 'translate(-50%, -' + blockPercent + '%)'
    });
  }
  $(document).on({
    scroll: function () {
      parallaxImg();
    }, ready: function () {
      parallaxImg();
    }
  });
});

// Scroller 100%
$(document).ready(function () {

    var divs = $('.block');
    var dir = 'up';
    var div = 0;
    $(document.body).on('DOMMouseScroll mousewheel', function (e) {
        if (e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) {
            dir = 'down';
        } else {
            dir = 'up';
        }
        div = -1;
        divs.each(function(i){
            if (div<0 && ($(this).offset().top >= $(window).scrollTop())) {
                div = i;
            }
        });
        if (dir == 'up' && div > 0) {
            div--;
        }
        if (dir == 'down' && div < divs.length) {
            div++;
        }
        $('html,body').stop().animate({
            scrollTop: divs.eq(div).offset().top
        }, 1000);
        return false;
    });
    $(window).resize(function () {
        $('html,body').scrollTop(divs.eq(div).offset().top);
    });
	
});


// loader
document.onreadystatechange = function(e) {
  if(document.readyState=="interactive") {
    var all = document.getElementsByTagName("*");
    for (var i=0, max=all.length; i < max; i++) {
      set_ele(all[i]);
    }
  } else {
    $(".progress").fadeOut("1000");
    $("#hideAll").fadeOut('1000');
  }
}

function check_element(ele) {
  var all = document.getElementsByTagName("*");
  var totalele=all.length;
  var per_inc=100 / all.length;

  if($(ele).on()) {
    var prog_width=per_inc+Number(document.getElementById("progress_width").value);
    document.getElementById("progress_width").value=prog_width;
    $("#bar1").animate({width:prog_width+"%"},10,function(){
      if(document.getElementById("bar1").style.width=="100%") {
       $(".progress").fadeOut("1000");
       $("#hideAll").fadeOut('1000');
      }
    });
  } else {
    set_ele(ele);
  }
}

function set_ele(set_element) {
  check_element(set_element);
}